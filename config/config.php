<?php

return [
    'storage' => [
        \Smorken\Import\Contracts\Storage\ImportResult::class => \Smorken\Import\Storage\Eloquent\ImportResult::class,
    ],
    'models' => [
        \Smorken\Import\Contracts\Models\ImportResult::class => \Smorken\Import\Models\Eloquent\ImportResult::class,
    ],
    'email_to' => env('IMPORT_EMAIL_TO', env('ERROR_EMAIL')),
    'migrations' => env('IMPORT_MIGRATIONS', false),
    'import_map' => [
        'impl' => \Smorken\Import\Storage\Eloquent\ImportMap::class,
        'model' => \Smorken\Import\Models\Eloquent\ImportMap::class,
    ],
];
