<?php

namespace Tests\Smorken\Import\Unit;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Import\Contracts\Enums\IdentifierTypes;
use Smorken\Import\Contracts\Enums\PartsFactoryParts;
use Smorken\Import\Contracts\ImportMapper;
use Smorken\Import\Contracts\PartsFactory;
use Smorken\Import\Contracts\Storage\ImportMap;
use Smorken\Import\Lookups;
use Smorken\Import\MapId;
use Tests\Smorken\Import\Unit\Stubs\MultiColDataModel;
use Tests\Smorken\Import\Unit\Stubs\SingleColDataModel;
use Tests\Smorken\Import\Unit\Stubs\StubImporter;

class ImportMapperTest extends TestCase
{
    public function testSetMapForIdentifiersWithSourcesNoMatches(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sut->getProvider()->shouldReceive('getByImporterAndIdentifiers')
            ->once()
            ->with(StubImporter::class, IdentifierTypes::SOURCE, [1, 2, 3, 5])
            ->andReturn(new Collection());
        $sut->setMapIdsForIdentifiers(StubImporter::class, IdentifierTypes::SOURCE, [1, 2, 3, 5]);
        $map = $sut->getFactory()->getLookups()->get(StubImporter::class);
        $this->assertTrue($map->isEmpty());
    }

    public function testSetMapForIdentifiersWithSourcesWithMatches(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $mapped = new Collection([
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => StubImporter::class, 'source_id' => 1, 'target_id' => '100',
            ]),
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => StubImporter::class, 'source_id' => 2, 'target_id' => '200',
            ]),
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => StubImporter::class, 'source_id' => 5, 'target_id' => '500',
            ]),
        ]);
        $sut->getProvider()->shouldReceive('getByImporterAndIdentifiers')
            ->once()
            ->with(StubImporter::class, IdentifierTypes::SOURCE, [1, 2, 3, 5])
            ->andReturn($mapped);
        $sut->setMapIdsForIdentifiers(StubImporter::class, IdentifierTypes::SOURCE, [1, 2, 3, 5]);
        $map = $sut->getFactory()->getLookups()->get(StubImporter::class);
        $this->assertFalse($map->isEmpty());
        $this->assertEquals('100', $map->get(1));
        $this->assertEquals('200', $map->get(2));
        $this->assertNull($map->get(3));
        $this->assertEquals('500', $map->get(5));
    }

    public function testSetMapForIdentifiersWithSourcesWithMatchesWithPreload(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $mapped = new Collection([
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => StubImporter::class, 'source_id' => 1, 'target_id' => '100',
            ]),
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => StubImporter::class, 'source_id' => 2, 'target_id' => '200',
            ]),
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => StubImporter::class, 'source_id' => 5, 'target_id' => '500',
            ]),
        ]);
        $sut->getProvider()->shouldReceive('getByImporter')
            ->once()
            ->with(StubImporter::class)
            ->andReturn($mapped);
        $sut->getProvider()->shouldReceive('getByImporterAndIdentifiers')
            ->once()
            ->with(StubImporter::class, IdentifierTypes::SOURCE, [3])
            ->andReturn(new Collection());
        $sut->preload(StubImporter::class);
        $sut->setMapIdsForIdentifiers(StubImporter::class, IdentifierTypes::SOURCE, [1, 2, 3, 5]);
        $map = $sut->getFactory()->getLookups()->get(StubImporter::class);
        $this->assertFalse($map->isEmpty());
        $this->assertEquals('100', $map->get(1));
        $this->assertEquals('200', $map->get(2));
        $this->assertNull($map->get(3));
        $this->assertEquals('500', $map->get(5));
    }

    public function testSetMapForIdentifiersWithTargetsNoMatches(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sut->getProvider()->shouldReceive('getByImporterAndIdentifiers')
            ->once()
            ->with(StubImporter::class, IdentifierTypes::TARGET, [1, 2, 3, 5])
            ->andReturn(new Collection());
        $sut->setMapIdsForIdentifiers(StubImporter::class, IdentifierTypes::TARGET, [1, 2, 3, 5]);
        $map = $sut->getFactory()->getLookups()->get(StubImporter::class);
        $this->assertTrue($map->isEmpty());
    }

    public function testSetMapForIdentifiersWithTargetsWithMatches(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $mapped = new Collection([
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => StubImporter::class, 'source_id' => 1, 'target_id' => '100',
            ]),
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => StubImporter::class, 'source_id' => 2, 'target_id' => '200',
            ]),
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => StubImporter::class, 'source_id' => 5, 'target_id' => '500',
            ]),
        ]);
        $sut->getProvider()->shouldReceive('getByImporterAndIdentifiers')
            ->once()
            ->with(StubImporter::class, IdentifierTypes::TARGET, ['100', '200', '300', '500'])
            ->andReturn($mapped);
        $sut->setMapIdsForIdentifiers(StubImporter::class, IdentifierTypes::TARGET, ['100', '200', '300', '500']);
        $map = $sut->getFactory()->getLookups()->get(StubImporter::class);
        $this->assertFalse($map->isEmpty());
        $this->assertEquals(1, $map->getSourceIdentifier('100'));
        $this->assertEquals(2, $map->getSourceIdentifier('200'));
        $this->assertNull($map->getSourceIdentifier('300'));
        $this->assertEquals(5, $map->getSourceIdentifier('500'));
    }

    public function testSetMapForModelsWithColumnNameWithSourcesNoMatches(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sut->getProvider()->shouldReceive('getByImporterAndIdentifiers')
            ->once()
            ->with(StubImporter::class, IdentifierTypes::SOURCE, [1, 2, 3, 5])
            ->andReturn(new Collection());
        $dataModels = new Collection([
            new MultiColDataModel(['external_id' => 1]),
            new MultiColDataModel(['external_id' => 2]),
            new MultiColDataModel(['external_id' => 3]),
            new MultiColDataModel(['external_id' => 5]),
        ]);
        $sut->setMapIdsForModels(StubImporter::class, IdentifierTypes::SOURCE, $dataModels, 'external_id');
        $map = $sut->getFactory()->getLookups()->get(StubImporter::class);
        $this->assertTrue($map->isEmpty());
    }

    public function testSetMapForModelsWithSourcesNoMatches(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sut->getProvider()->shouldReceive('getByImporterAndIdentifiers')
            ->once()
            ->with(StubImporter::class, IdentifierTypes::SOURCE, [1, 2, 3, 5])
            ->andReturn(new Collection());
        $dataModels = new Collection([
            new SingleColDataModel(['external_id' => 1]),
            new SingleColDataModel(['external_id' => 2]),
            new SingleColDataModel(['external_id' => 3]),
            new SingleColDataModel(['external_id' => 5]),
        ]);
        $sut->setMapIdsForModels(StubImporter::class, IdentifierTypes::SOURCE, $dataModels);
        $map = $sut->getFactory()->getLookups()->get(StubImporter::class);
        $this->assertTrue($map->isEmpty());
    }

    public function testSetMapForModelsWithSourcesWithMatches(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $dataModels = new Collection([
            new SingleColDataModel(['external_id' => 1]),
            new SingleColDataModel(['external_id' => 2]),
            new SingleColDataModel(['external_id' => 3]),
            new SingleColDataModel(['external_id' => 5]),
        ]);
        $mapped = new Collection([
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => StubImporter::class, 'source_id' => 1, 'target_id' => '100',
            ]),
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => StubImporter::class, 'source_id' => 2, 'target_id' => '200',
            ]),
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => StubImporter::class, 'source_id' => 5, 'target_id' => '500',
            ]),
        ]);
        $sut->getProvider()->shouldReceive('getByImporterAndIdentifiers')
            ->once()
            ->with(StubImporter::class, IdentifierTypes::SOURCE, [1, 2, 3, 5])
            ->andReturn($mapped);
        $sut->setMapIdsForModels(StubImporter::class, IdentifierTypes::SOURCE, $dataModels);
        $map = $sut->getFactory()->getLookups()->get(StubImporter::class);
        $this->assertFalse($map->isEmpty());
        $this->assertEquals('100', $map->get(1));
        $this->assertEquals('200', $map->get(2));
        $this->assertNull($map->get(3));
        $this->assertEquals('500', $map->get(5));
    }

    public function testSetMapForModelsWithTargetsAndColumnWithMatches(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $dataModels = new Collection([
            new SingleColDataModel(['external_id' => '100']),
            new SingleColDataModel(['external_id' => '200']),
            new SingleColDataModel(['external_id' => '300']),
            new SingleColDataModel(['external_id' => '500']),
        ]);
        $mapped = new Collection([
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => StubImporter::class, 'source_id' => 1, 'target_id' => '100',
            ]),
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => StubImporter::class, 'source_id' => 2, 'target_id' => '200',
            ]),
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => StubImporter::class, 'source_id' => 5, 'target_id' => '500',
            ]),
        ]);
        $sut->getProvider()->shouldReceive('getByImporterAndIdentifiers')
            ->once()
            ->with(StubImporter::class, IdentifierTypes::TARGET, ['100', '200', '300', '500'])
            ->andReturn($mapped);
        $sut->setMapIdsForModels(StubImporter::class, IdentifierTypes::TARGET, $dataModels, 'external_id');
        $map = $sut->getFactory()->getLookups()->get(StubImporter::class);
        $this->assertFalse($map->isEmpty());
        $this->assertEquals(1, $map->getSourceIdentifier('100'));
        $this->assertEquals(2, $map->getSourceIdentifier('200'));
        $this->assertNull($map->getSourceIdentifier('300'));
        $this->assertEquals(5, $map->getSourceIdentifier('500'));
    }

    public function testSetMapForModelsWithTargetsNoMatches(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sut->getProvider()->shouldReceive('getByImporter')
            ->never();
        $dataModels = new Collection([
            new SingleColDataModel(['external_id' => 1]),
            new SingleColDataModel(['external_id' => 2]),
            new SingleColDataModel(['external_id' => 3]),
            new SingleColDataModel(['external_id' => 5]),
        ]);
        $sut->setMapIdsForModels(StubImporter::class, IdentifierTypes::TARGET, $dataModels);
        $map = $sut->getFactory()->getLookups()->get(StubImporter::class);
        $this->assertTrue($map->isEmpty());
    }

    public function testStoreWithMatchStoresMissing(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sut->getFactory()->getLookups()->get(StubImporter::class)->set(1, 100);
        $sut->getFactory()->getLookups()->get(StubImporter::class)->set(2, null);
        $sut->getFactory()->getLookups()->get(StubImporter::class)->set(3, 300);
        $mapped = new Collection([
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => StubImporter::class, 'source_id' => 3, 'target_id' => 300,
            ]),
        ]);
        $sut->getProvider()->shouldReceive('getByImporter')
            ->once()
            ->with(StubImporter::class)
            ->andReturn($mapped);
        $sut->getProvider()->shouldReceive('saveManyImporterAndArray')
            ->once()
            ->with(StubImporter::class, [['source_id' => 1, 'target_id' => 100]])
            ->andReturn(1);
        $this->assertEquals(1, $sut->store(StubImporter::class));
    }

    public function testStoreWithNoMatchStoresAll(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sut->getFactory()->getLookups()->get(StubImporter::class)->set(1, 100);
        $sut->getFactory()->getLookups()->get(StubImporter::class)->set(2, null);
        $sut->getFactory()->getLookups()->get(StubImporter::class)->set(3, 300);
        $mapped = new Collection();
        $sut->getProvider()->shouldReceive('getByImporter')
            ->once()
            ->with(StubImporter::class)
            ->andReturn($mapped);
        $sut->getProvider()->shouldReceive('saveManyImporterAndArray')
            ->once()
            ->with(StubImporter::class,
                [['source_id' => 1, 'target_id' => 100], ['source_id' => 3, 'target_id' => 300]])
            ->andReturn(2);
        $this->assertEquals(2, $sut->store(StubImporter::class));
    }

    protected function getPartsFactory(array $overrides = []): PartsFactory
    {
        $defaults = [
            PartsFactoryParts::LOOKUPS => new Lookups(['default' => new MapId()]),
        ];

        return new \Smorken\Import\PartsFactory(array_merge($defaults, $overrides));
    }

    protected function getSut(PartsFactory $partsFactory): ImportMapper
    {
        $p = m::mock(ImportMap::class);
        $p->shouldReceive('getModel')
            ->andReturn(new \Smorken\Import\Models\Eloquent\ImportMap());
        App::shouldReceive('make')
            ->with(ImportMap::class)
            ->andReturn($p);

        return new \Smorken\Import\ImportMapper($partsFactory);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
