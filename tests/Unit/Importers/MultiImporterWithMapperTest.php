<?php

namespace Tests\Smorken\Import\Unit\Importers;

use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Import\Contracts\Enums\IdentifierTypes;
use Smorken\Import\Contracts\Enums\PartsFactoryParts;
use Smorken\Import\Contracts\PartsFactory;
use Smorken\Import\Contracts\Storage\HasSource;
use Smorken\Import\Contracts\Storage\HasTarget;
use Smorken\Import\Contracts\Storage\ImportMap;
use Smorken\Import\ImportMapper;
use Smorken\Import\Lookups;
use Smorken\Import\MapId;
use Smorken\Import\Results;
use Tests\Smorken\Import\Unit\Stubs\MultiImportMapperModeller\DataModel;
use Tests\Smorken\Import\Unit\Stubs\MultiImportMapperModeller\Modeller;
use Tests\Smorken\Import\Unit\Stubs\MultiImportMapperModeller\MultiImporter;
use Tests\Smorken\Import\Unit\Stubs\MultiImportMapperModeller\OtherImporter;
use Tests\Smorken\Import\Unit\Stubs\MultiImportMapperModeller\SourceModel;
use Tests\Smorken\Import\Unit\Stubs\MultiImportMapperModeller\TargetModel;

class MultiImporterWithMapperTest extends TestCase
{
    protected ?ImportMap $importMapStorage = null;

    protected ?HasSource $sourceStorage = null;

    protected ?HasTarget $targetStorage = null;

    public function testMapLimitsSavedImportMap(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sourceModels = new Collection([
            new SourceModel(['id' => 10, 'descr' => 'SM 10', 'some_other_id' => 11]),
            new SourceModel(['id' => 20, 'descr' => 'SM 20', 'some_other_id' => 22]),
            new SourceModel(['id' => 30, 'descr' => 'SM 30', 'some_other_id' => 33]),
            new SourceModel(['id' => 50, 'descr' => 'SM 50', 'some_other_id' => 55]),
        ]);
        $this->getImportMapStorage()->shouldReceive('getByImporter')
            ->once()
            ->with(MultiImporter::class)
            ->andReturn(new Collection(new Collection(
                [
                    new \Smorken\Import\Models\Eloquent\ImportMap([
                        'importer' => MultiImporter::class, 'source_id' => 50, 'target_id' => 500,
                    ]),
                ]
            )));
        $this->getImportMapStorage()->shouldReceive('getByImporterAndIdentifiers')
            ->twice()
            ->with(MultiImporter::class, IdentifierTypes::SOURCE, ['10', '20', '30'])
            ->andReturn(new Collection());
        $this->getImportMapStorage()->shouldReceive('getByImporter')
            ->once()
            ->with(OtherImporter::class)
            ->andReturn(new Collection([
                new \Smorken\Import\Models\Eloquent\ImportMap([
                    'importer' => OtherImporter::class, 'source_id' => 11, 'target_id' => 111,
                ]),
                new \Smorken\Import\Models\Eloquent\ImportMap([
                    'importer' => OtherImporter::class, 'source_id' => 22, 'target_id' => 222,
                ]),
                new \Smorken\Import\Models\Eloquent\ImportMap([
                    'importer' => OtherImporter::class, 'source_id' => 33, 'target_id' => 333,
                ]),
                new \Smorken\Import\Models\Eloquent\ImportMap([
                    'importer' => OtherImporter::class, 'source_id' => 55, 'target_id' => 555,
                ]),
            ]));
        $this->getTargetProvider()->shouldReceive('getByTargetIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = [500];
                $this->assertEquals($expected, array_keys($identifiers));

                return count($identifiers) === count($expected);
            }))
            ->andReturn(new Collection([
                (new TargetModel())->forceFill(['id' => 500, 'name' => 'SM Change me', 'other_id' => 555]),
            ]));
        $this->getTargetProvider()->shouldReceive('createMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $others = [111, 222, 333];
                foreach ($models as $model) {
                    $this->assertTrue(in_array($model->other_id, $others));
                    $model->id = $model->source_id * 10;
                }
                $this->assertCount(3, $models);

                return $models;
            });
        $this->getTargetProvider()->shouldReceive('updateMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $this->assertCount(1, $models);

                return 1;
            });
        $this->getTargetProvider()->shouldReceive('touchMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $this->assertCount(0, $models);

                return 0;
            });
        $this->getImportMapStorage()->shouldReceive('saveManyImporterAndArray')
            ->once()
            ->with('Tests\Smorken\Import\Unit\Stubs\MultiImportMapperModeller\MultiImporter', [
                ['source_id' => 10, 'target_id' => 100],
                ['source_id' => 20, 'target_id' => 200],
                ['source_id' => 30, 'target_id' => 300],
            ])
            ->andReturn(3);
        $sut->handle($sourceModels);
        $expected = [
            'MultiImporter::total' => 4,
            'MultiImporter::validated' => 4,
            'MultiImporter::existing' => 1,
            'MultiImporter::created' => 3,
            'MultiImporter::touched' => 0,
            'MultiImporter::updated' => 1,
        ];
        $this->assertEquals($expected, $sut->getFactory()->getResults()->getCounters());
    }

    public function testNoMatchesCreatesAll(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sourceModels = new Collection([
            new SourceModel(['id' => 10, 'descr' => 'SM 10', 'some_other_id' => 11]),
            new SourceModel(['id' => 20, 'descr' => 'SM 20', 'some_other_id' => 22]),
            new SourceModel(['id' => 30, 'descr' => 'SM 30', 'some_other_id' => 33]),
            new SourceModel(['id' => 50, 'descr' => 'SM 50', 'some_other_id' => 55]),
        ]);
        $this->getImportMapStorage()->shouldReceive('getByImporter')
            ->once()
            ->with(MultiImporter::class)
            ->andReturn(new Collection());
        $this->getImportMapStorage()->shouldReceive('getByImporterAndIdentifiers')
            ->twice()
            ->with(MultiImporter::class, IdentifierTypes::SOURCE, ['10', '20', '30', '50'])
            ->andReturn(new Collection());
        $this->getImportMapStorage()->shouldReceive('getByImporter')
            ->once()
            ->with(OtherImporter::class)
            ->andReturn(new Collection([
                new \Smorken\Import\Models\Eloquent\ImportMap([
                    'importer' => OtherImporter::class, 'source_id' => 11, 'target_id' => 111,
                ]),
                new \Smorken\Import\Models\Eloquent\ImportMap([
                    'importer' => OtherImporter::class, 'source_id' => 22, 'target_id' => 222,
                ]),
                new \Smorken\Import\Models\Eloquent\ImportMap([
                    'importer' => OtherImporter::class, 'source_id' => 33, 'target_id' => 333,
                ]),
                new \Smorken\Import\Models\Eloquent\ImportMap([
                    'importer' => OtherImporter::class, 'source_id' => 55, 'target_id' => 555,
                ]),
            ]));
        $this->getTargetProvider()->shouldReceive('getByTargetIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = [];
                $this->assertEquals($expected, array_keys($identifiers));

                return count($identifiers) === count($expected);
            }))
            ->andReturn(new Collection());
        $this->getTargetProvider()->shouldReceive('createMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $others = [111, 222, 333, 555];
                foreach ($models as $model) {
                    $this->assertTrue(in_array($model->other_id, $others));
                    $model->id = $model->source_id * 10;
                }
                $this->assertCount(4, $models);

                return $models;
            });
        $this->getTargetProvider()->shouldReceive('updateMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $this->assertCount(0, $models);

                return 0;
            });
        $this->getTargetProvider()->shouldReceive('touchMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $this->assertCount(0, $models);

                return 0;
            });
        $this->getImportMapStorage()->shouldReceive('saveManyImporterAndArray')
            ->once()
            ->with('Tests\Smorken\Import\Unit\Stubs\MultiImportMapperModeller\MultiImporter', [
                ['source_id' => 10, 'target_id' => 100],
                ['source_id' => 20, 'target_id' => 200],
                ['source_id' => 30, 'target_id' => 300],
                ['source_id' => 50, 'target_id' => 500],
            ])
            ->andReturn(4);
        $sut->handle($sourceModels);
        $expected = [
            'MultiImporter::total' => 4,
            'MultiImporter::validated' => 4,
            'MultiImporter::existing' => 0,
            'MultiImporter::created' => 4,
            'MultiImporter::touched' => 0,
            'MultiImporter::updated' => 0,
        ];
        $this->assertEquals($expected, $sut->getFactory()->getResults()->getCounters());
    }

    public function testUpdateAndTouchFromMap(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sourceModels = new Collection([
            new SourceModel(['id' => 10, 'descr' => 'SM 10', 'some_other_id' => 11]),
            new SourceModel(['id' => 20, 'descr' => 'SM 20', 'some_other_id' => 22]),
            new SourceModel(['id' => 30, 'descr' => 'SM 30', 'some_other_id' => 33]),
            new SourceModel(['id' => 50, 'descr' => 'SM 50', 'some_other_id' => 55]),
        ]);
        $importMapCollection = new Collection([
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => MultiImporter::class, 'source_id' => '10', 'target_id' => '100',
            ]),
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => MultiImporter::class, 'source_id' => '30', 'target_id' => '300',
            ]),
        ]);
        $this->getImportMapStorage()->shouldReceive('getByImporter')
            ->once()
            ->with(MultiImporter::class)
            ->andReturn($importMapCollection);
        $this->getImportMapStorage()->shouldReceive('getByImporterAndIdentifiers')
            ->twice()
            ->with(MultiImporter::class, IdentifierTypes::SOURCE, ['20', '50'])
            ->andReturn(new Collection());
        $this->getImportMapStorage()->shouldReceive('getByImporter')
            ->once()
            ->with(OtherImporter::class)
            ->andReturn(new Collection([
                new \Smorken\Import\Models\Eloquent\ImportMap([
                    'importer' => OtherImporter::class, 'source_id' => 11, 'target_id' => 111,
                ]),
                new \Smorken\Import\Models\Eloquent\ImportMap([
                    'importer' => OtherImporter::class, 'source_id' => 22, 'target_id' => 222,
                ]),
                new \Smorken\Import\Models\Eloquent\ImportMap([
                    'importer' => OtherImporter::class, 'source_id' => 33, 'target_id' => 333,
                ]),
                new \Smorken\Import\Models\Eloquent\ImportMap([
                    'importer' => OtherImporter::class, 'source_id' => 55, 'target_id' => 555,
                ]),
            ]));
        $this->getTargetProvider()->shouldReceive('getByTargetIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['100', '300'];
                $this->assertEquals($expected, array_keys($identifiers));

                return count($identifiers) === count($expected);
            }))
            ->andReturn(new Collection([
                (new TargetModel())->forceFill(['id' => 100, 'name' => 'SM Change me', 'other_id' => 111]),
                (new TargetModel())->forceFill(['id' => 300, 'name' => 'SM 30', 'other_id' => 333]),
            ]));
        $this->getTargetProvider()->shouldReceive('createMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $others = [222, 555];
                foreach ($models as $model) {
                    $this->assertTrue(in_array($model->other_id, $others));
                    $model->id = $model->source_id * 10;
                }
                $this->assertCount(2, $models);

                return $models;
            });
        $this->getTargetProvider()->shouldReceive('updateMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $first = $models->first();
                $this->assertEquals('SM 10', $first->name);
                $this->assertEquals(111, $first->other_id);
                $this->assertCount(1, $models);

                return 1;
            });
        $this->getTargetProvider()->shouldReceive('touchMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $first = $models->first();
                $this->assertEquals('SM 30', $first->name);
                $this->assertEquals(333, $first->other_id);
                $this->assertCount(1, $models);

                return 1;
            });
        $this->getImportMapStorage()->shouldReceive('saveManyImporterAndArray')
            ->once()
            ->with(MultiImporter::class, [
                ['source_id' => 20, 'target_id' => 200],
                ['source_id' => 50, 'target_id' => 500],
            ])
            ->andReturn(2);
        $sut->handle($sourceModels);
        $expected = [
            'MultiImporter::total' => 4,
            'MultiImporter::validated' => 4,
            'MultiImporter::existing' => 2,
            'MultiImporter::created' => 2,
            'MultiImporter::touched' => 1,
            'MultiImporter::updated' => 1,
        ];
        $this->assertEquals($expected, $sut->getFactory()->getResults()->getCounters());
    }

    protected function getImportMapStorage(): ImportMap
    {
        if (is_null($this->importMapStorage)) {
            $this->importMapStorage = m::mock(ImportMap::class);
            $this->importMapStorage->shouldReceive('getModel')
                ->andReturn(new \Smorken\Import\Models\Eloquent\ImportMap());
        }

        return $this->importMapStorage;
    }

    protected function getPartsFactory(array $overrides = []): PartsFactory
    {
        $defaults = [
            PartsFactoryParts::SOURCE_PROVIDER => $this->getSourceProvider(),
            PartsFactoryParts::LOOKUPS => new Lookups(['default' => new MapId()]),
            PartsFactoryParts::RESULTS => new Results(),
            PartsFactoryParts::DATA_MODEL => new DataModel(),
            PartsFactoryParts::TARGET_PROVIDER => $this->getTargetProvider(),
            PartsFactoryParts::DATA_TARGET_MODELLER => new Modeller(),
        ];

        return new \Smorken\Import\PartsFactory(array_merge($defaults, $overrides));
    }

    protected function getSourceProvider(): HasSource
    {
        if (is_null($this->sourceStorage)) {
            $this->sourceStorage = m::mock(HasSource::class);
            $this->sourceStorage->shouldReceive('getSource')
                ->andReturn(new SourceModel());
        }

        return $this->sourceStorage;
    }

    protected function getSut(PartsFactory $partsFactory): MultiImporter
    {
        ImportMapper::$importMapProvider = $this->getImportMapStorage();

        return new MultiImporter($partsFactory);
    }

    protected function getTargetProvider(): HasTarget
    {
        if (is_null($this->targetStorage)) {
            $this->targetStorage = m::mock(HasTarget::class);
            $this->targetStorage->shouldReceive('getTarget')
                ->andReturn(new TargetModel());
        }

        return $this->targetStorage;
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
