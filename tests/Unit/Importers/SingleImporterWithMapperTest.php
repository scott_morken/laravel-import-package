<?php

namespace Tests\Smorken\Import\Unit\Importers;

use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Import\Contracts\Enums\IdentifierTypes;
use Smorken\Import\Contracts\Enums\PartsFactoryParts;
use Smorken\Import\Contracts\PartsFactory;
use Smorken\Import\Contracts\Storage\ImportMap;
use Smorken\Import\ImportMapper;
use Smorken\Import\Lookups;
use Smorken\Import\MapId;
use Smorken\Import\Results;
use Tests\Smorken\Import\Unit\Stubs\SingIeImportMapperModeller\DataModel;
use Tests\Smorken\Import\Unit\Stubs\SingIeImportMapperModeller\Modeller;
use Tests\Smorken\Import\Unit\Stubs\SingIeImportMapperModeller\SingleImporter;
use Tests\Smorken\Import\Unit\Stubs\SingIeImportMapperModeller\SourceModel;
use Tests\Smorken\Import\Unit\Stubs\SingIeImportMapperModeller\SourceStorage;
use Tests\Smorken\Import\Unit\Stubs\SingIeImportMapperModeller\TargetModel;
use Tests\Smorken\Import\Unit\Stubs\SingIeImportMapperModeller\TargetStorage;

class SingleImporterWithMapperTest extends TestCase
{
    protected ?ImportMap $importMapStorage = null;

    protected ?SourceStorage $sourceStorage = null;

    protected ?TargetStorage $targetStorage = null;

    public function testAlreadyHandledSkips(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sourceModels = new Collection([
            new SourceModel(['id' => 10, 'descr' => 'SM 10']),
            new SourceModel(['id' => 20, 'descr' => 'SM 20']),
            new SourceModel(['id' => 30, 'descr' => 'SM 30']),
            new SourceModel(['id' => 50, 'descr' => 'SM 50']),
        ]);
        $sut->getFactory()->getLookups()->get()->set(10, 100);
        $this->getImportMapStorage()->shouldReceive('getByImporter')
            ->once()
            ->with(SingleImporter::class)
            ->andReturn(new Collection());
        $this->getImportMapStorage()->shouldReceive('getByImporterAndIdentifiers')
            ->twice()
            ->with(SingleImporter::class, IdentifierTypes::SOURCE, ['20', '30', '50'])
            ->andReturn(new Collection());
        $this->getTargetProvider()->shouldReceive('getByTargetIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = [];
                $this->assertEquals($expected, array_keys($identifiers));

                return count($identifiers) === count($expected);
            }))
            ->andReturn(new Collection());
        $this->getTargetProvider()->shouldReceive('createMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $this->assertCount(3, $models);
                foreach ($models as $model) {
                    $sourceId = (int) $model->getIdentifiers()->asString(IdentifierTypes::SOURCE);
                    $model->id = $sourceId * 10;
                }

                return $models;
            });
        $this->getTargetProvider()->shouldReceive('updateMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $this->assertCount(0, $models);

                return 0;
            });
        $this->getTargetProvider()->shouldReceive('touchMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $this->assertCount(0, $models);

                return 0;
            });
        $this->getImportMapStorage()->shouldReceive('saveManyImporterAndArray')
            ->once()
            ->with('Tests\Smorken\Import\Unit\Stubs\SingIeImportMapperModeller\SingleImporter', [
                ['source_id' => 20, 'target_id' => '200'],
                ['source_id' => 30, 'target_id' => '300'],
                ['source_id' => 50, 'target_id' => '500'],
            ])
            ->andReturn(3);
        $sut->handle($sourceModels);
        $expected = [
            'SingleImporter::total' => 4,
            'SingleImporter::validated' => 3,
            'SingleImporter::existing' => 0,
            'SingleImporter::created' => 3,
            'SingleImporter::touched' => 0,
            'SingleImporter::updated' => 0,
        ];
        $this->assertEquals($expected, $sut->getFactory()->getResults()->getCounters());
    }

    public function testCanSetExistingTargetIdFromMap(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sourceModels = new Collection([
            new SourceModel(['id' => 10, 'descr' => 'SM 10']),
            new SourceModel(['id' => 20, 'descr' => 'SM 20']),
            new SourceModel(['id' => 30, 'descr' => 'SM 30']),
            new SourceModel(['id' => 50, 'descr' => 'SM 50']),
        ]);
        $importMapCollection = new Collection([
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => SingleImporter::class, 'source_id' => '10', 'target_id' => '100',
            ]),
        ]);
        $this->getImportMapStorage()->shouldReceive('getByImporter')
            ->once()
            ->with(SingleImporter::class)
            ->andReturn($importMapCollection);
        $this->getImportMapStorage()->shouldReceive('getByImporterAndIdentifiers')
            ->twice()
            ->with(SingleImporter::class, IdentifierTypes::SOURCE, ['20', '30', '50'])
            ->andReturn(new Collection());
        $this->getTargetProvider()->shouldReceive('getByTargetIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['100'];
                $this->assertEquals($expected, array_keys($identifiers));

                return count($identifiers) === count($expected);
            }))
            ->andReturn(new Collection([
                (new TargetModel())->forceFill(['id' => 100, 'name' => 'SM Change me']),
            ]));
        $this->getTargetProvider()->shouldReceive('createMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $this->assertCount(3, $models);
                foreach ($models as $model) {
                    $sourceId = (int) $model->getIdentifiers()->asString(IdentifierTypes::SOURCE);
                    $model->id = $sourceId * 10;
                }

                return $models;
            });
        $this->getTargetProvider()->shouldReceive('updateMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $first = $models->first();
                $this->assertEquals('SM 10', $first->name);
                $this->assertCount(1, $models);

                return 1;
            });
        $this->getTargetProvider()->shouldReceive('touchMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $this->assertCount(0, $models);

                return 0;
            });
        $this->getImportMapStorage()->shouldReceive('saveManyImporterAndArray')
            ->once()
            ->with('Tests\Smorken\Import\Unit\Stubs\SingIeImportMapperModeller\SingleImporter', [
                ['source_id' => 20, 'target_id' => '200'],
                ['source_id' => 30, 'target_id' => '300'],
                ['source_id' => 50, 'target_id' => '500'],
            ])
            ->andReturn(3);
        $sut->handle($sourceModels);
        $expected = [
            'SingleImporter::total' => 4,
            'SingleImporter::validated' => 4,
            'SingleImporter::existing' => 1,
            'SingleImporter::created' => 3,
            'SingleImporter::touched' => 0,
            'SingleImporter::updated' => 1,
        ];
        $this->assertEquals($expected, $sut->getFactory()->getResults()->getCounters());
    }

    public function testNoMatchesCreatesAll(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sourceModels = new Collection([
            new SourceModel(['id' => 10, 'descr' => 'SM 10']),
            new SourceModel(['id' => 20, 'descr' => 'SM 20']),
            new SourceModel(['id' => 30, 'descr' => 'SM 30']),
            new SourceModel(['id' => 50, 'descr' => 'SM 50']),
        ]);
        $this->getImportMapStorage()->shouldReceive('getByImporter')
            ->once()
            ->with(SingleImporter::class)
            ->andReturn(new Collection());
        $this->getImportMapStorage()->shouldReceive('getByImporterAndIdentifiers')
            ->twice()
            ->with(SingleImporter::class, IdentifierTypes::SOURCE, ['10', '20', '30', '50'])
            ->andReturn(new Collection());
        $this->getTargetProvider()->shouldReceive('getByTargetIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = [];
                $this->assertEquals($expected, array_keys($identifiers));

                return count($identifiers) === count($expected);
            }))
            ->andReturn(new Collection());
        $this->getTargetProvider()->shouldReceive('createMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $this->assertCount(4, $models);
                foreach ($models as $model) {
                    $sourceId = (int) $model->getIdentifiers()->asString(IdentifierTypes::SOURCE);
                    $model->id = $sourceId * 10;
                }

                return $models;
            });
        $this->getTargetProvider()->shouldReceive('updateMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $this->assertCount(0, $models);

                return 0;
            });
        $this->getTargetProvider()->shouldReceive('touchMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $this->assertCount(0, $models);

                return 0;
            });
        $this->getImportMapStorage()->shouldReceive('saveManyImporterAndArray')
            ->once()
            ->with('Tests\Smorken\Import\Unit\Stubs\SingIeImportMapperModeller\SingleImporter', [
                ['source_id' => 10, 'target_id' => '100'],
                ['source_id' => 20, 'target_id' => '200'],
                ['source_id' => 30, 'target_id' => '300'],
                ['source_id' => 50, 'target_id' => '500'],
            ])
            ->andReturn(4);
        $sut->handle($sourceModels);
        $expected = [
            'SingleImporter::total' => 4,
            'SingleImporter::validated' => 4,
            'SingleImporter::existing' => 0,
            'SingleImporter::created' => 4,
            'SingleImporter::touched' => 0,
            'SingleImporter::updated' => 0,
        ];
        $this->assertEquals($expected, $sut->getFactory()->getResults()->getCounters());
    }

    public function testUpdateAndTouchFromMap(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sourceModels = new Collection([
            new SourceModel(['id' => 10, 'descr' => 'SM 10']),
            new SourceModel(['id' => 20, 'descr' => 'SM 20']),
            new SourceModel(['id' => 30, 'descr' => 'SM 30']),
            new SourceModel(['id' => 50, 'descr' => 'SM 50']),
        ]);
        $importMapCollection = new Collection([
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => SingleImporter::class, 'source_id' => '10', 'target_id' => '100',
            ]),
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => SingleImporter::class, 'source_id' => '30', 'target_id' => '300',
            ]),
        ]);
        $this->getImportMapStorage()->shouldReceive('getByImporter')
            ->once()
            ->with(SingleImporter::class)
            ->andReturn($importMapCollection);
        $this->getImportMapStorage()->shouldReceive('getByImporterAndIdentifiers')
            ->twice()
            ->with(SingleImporter::class, IdentifierTypes::SOURCE, ['20', '50'])
            ->andReturn(new Collection());
        $this->getTargetProvider()->shouldReceive('getByTargetIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['100', '300'];
                $this->assertEquals($expected, array_keys($identifiers));

                return count($identifiers) === count($expected);
            }))
            ->andReturn(new Collection([
                (new TargetModel())->forceFill(['id' => 100, 'name' => 'SM Change me']),
                (new TargetModel())->forceFill(['id' => 300, 'name' => 'SM 30']),
            ]));
        $this->getTargetProvider()->shouldReceive('createMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $this->assertCount(2, $models);
                foreach ($models as $model) {
                    $sourceId = (int) $model->getIdentifiers()->asString(IdentifierTypes::SOURCE);
                    $model->id = $sourceId * 10;
                }

                return $models;
            });
        $this->getTargetProvider()->shouldReceive('updateMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $first = $models->first();
                $this->assertEquals('SM 10', $first->name);
                $this->assertCount(1, $models);

                return 1;
            });
        $this->getTargetProvider()->shouldReceive('touchMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $this->assertCount(1, $models);

                return 1;
            });
        $this->getImportMapStorage()->shouldReceive('saveManyImporterAndArray')
            ->once()
            ->with('Tests\Smorken\Import\Unit\Stubs\SingIeImportMapperModeller\SingleImporter', [
                ['source_id' => 20, 'target_id' => '200'],
                ['source_id' => 50, 'target_id' => '500'],
            ])
            ->andReturn(2);
        $sut->handle($sourceModels);
        $expected = [
            'SingleImporter::total' => 4,
            'SingleImporter::validated' => 4,
            'SingleImporter::existing' => 2,
            'SingleImporter::created' => 2,
            'SingleImporter::touched' => 1,
            'SingleImporter::updated' => 1,
        ];
        $this->assertEquals($expected, $sut->getFactory()->getResults()->getCounters());
    }

    protected function getImportMapStorage(): ImportMap
    {
        if (is_null($this->importMapStorage)) {
            $this->importMapStorage = m::mock(ImportMap::class);
            $this->importMapStorage->shouldReceive('getModel')
                ->andReturn(new \Smorken\Import\Models\Eloquent\ImportMap());
        }

        return $this->importMapStorage;
    }

    protected function getPartsFactory(array $overrides = []): PartsFactory
    {
        $defaults = [
            PartsFactoryParts::SOURCE_PROVIDER => $this->getSourceProvider(),
            PartsFactoryParts::LOOKUPS => new Lookups(['default' => new MapId()]),
            PartsFactoryParts::RESULTS => new Results(),
            PartsFactoryParts::DATA_MODEL => new DataModel(),
            PartsFactoryParts::TARGET_PROVIDER => $this->getTargetProvider(),
            PartsFactoryParts::DATA_TARGET_MODELLER => new Modeller(),
        ];

        return new \Smorken\Import\PartsFactory(array_merge($defaults, $overrides));
    }

    protected function getSourceProvider(): SourceStorage
    {
        if (is_null($this->sourceStorage)) {
            $this->sourceStorage = m::mock(SourceStorage::class);
            $this->sourceStorage->shouldReceive('getSource')
                ->andReturn(new SourceModel());
        }

        return $this->sourceStorage;
    }

    protected function getSut(PartsFactory $partsFactory): SingleImporter
    {
        ImportMapper::$importMapProvider = $this->getImportMapStorage();

        return new SingleImporter($partsFactory);
    }

    protected function getTargetProvider(): TargetStorage
    {
        if (is_null($this->targetStorage)) {
            $this->targetStorage = m::mock(TargetStorage::class);
            $this->targetStorage->shouldReceive('getTarget')
                ->andReturn(new TargetModel());
        }

        return $this->targetStorage;
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
