<?php

namespace Tests\Smorken\Import\Unit\Importers;

use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Import\Contracts\Enums\IdentifierTypes;
use Smorken\Import\Contracts\Enums\PartsFactoryParts;
use Smorken\Import\Contracts\PartsFactory;
use Smorken\Import\Contracts\Storage\HasSource;
use Smorken\Import\Contracts\Storage\HasTarget;
use Smorken\Import\Contracts\Storage\ImportMap;
use Smorken\Import\ImportMapper;
use Smorken\Import\Lookups;
use Smorken\Import\MapId;
use Smorken\Import\Results;
use Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller\DataModel;
use Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller\DependencyImporter;
use Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller\DependentImporter;
use Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller\Modeller;
use Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller\SourceModel;
use Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller\TargetModel;

class DependentImportWithMapperTest extends TestCase
{
    protected ?ImportMap $importMapStorage = null;

    protected ?HasSource $sourceStorage = null;

    protected ?HasTarget $targetStorage = null;

    public function testWithDependenciesMappedNoMatchesCreatesAll(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sourceModels = new Collection([
            new \Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller\SourceModel([
                'id' => 1, 'reviewer_id' => 3100, 'student_id' => 3000, 'progress_report_id' => 1000,
            ]),
            new \Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller\SourceModel([
                'id' => 2, 'reviewer_id' => 3100, 'student_id' => 3001, 'progress_report_id' => 1000,
            ]),
            new \Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller\SourceModel([
                'id' => 3, 'reviewer_id' => 3101, 'student_id' => 3002, 'progress_report_id' => 1000,
            ]),
            new \Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller\SourceModel([
                'id' => 4, 'reviewer_id' => 3102, 'student_id' => 3003, 'progress_report_id' => 1000,
            ]),
        ]);
        $this->getImportMapStorage()->shouldReceive('getByImporter')
            ->once()
            ->with(DependentImporter::class)
            ->andReturn(new Collection());
        $this->getImportMapStorage()->shouldReceive('getByImporterAndIdentifiers')
            ->twice()
            ->with(DependentImporter::class, IdentifierTypes::SOURCE,
                ['3100:3000:1000', '3100:3001:1000', '3101:3002:1000', '3102:3003:1000'])
            ->andReturn(new Collection());
        $this->getImportMapStorage()->shouldReceive('getByImporter')
            ->once()
            ->with(DependencyImporter::class)
            ->andReturn(new Collection([
                new \Smorken\Import\Models\Eloquent\ImportMap(['source_id' => '1000', 'target_id' => '1111']),
            ]));
        $this->getTargetProvider()->shouldReceive('getByTargetIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['3100:3000:1111', '3100:3001:1111', '3101:3002:1111', '3102:3003:1111'];
                $this->assertEquals($expected, array_keys($identifiers));

                return count($identifiers) === count($expected);
            }))
            ->andReturn(new Collection());
        $this->getTargetProvider()->shouldReceive('createMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $this->assertCount(4, $models);

                return $models;
            });
        $this->getTargetProvider()->shouldReceive('updateMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $this->assertCount(0, $models);

                return 0;
            });
        $this->getTargetProvider()->shouldReceive('touchMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $this->assertCount(0, $models);

                return 0;
            });
        $this->getImportMapStorage()->shouldReceive('saveManyImporterAndArray')
            ->once()
            ->with('Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller\DependentImporter', [
                ['source_id' => '3100:3000:1000', 'target_id' => '3100:3000:1111'],
                ['source_id' => '3100:3001:1000', 'target_id' => '3100:3001:1111'],
                ['source_id' => '3101:3002:1000', 'target_id' => '3101:3002:1111'],
                ['source_id' => '3102:3003:1000', 'target_id' => '3102:3003:1111'],
            ])
            ->andReturn(4);
        $sut->handle($sourceModels);
        $expected = [
            'DependentImporter::total' => 4,
            'DependentImporter::validated' => 4,
            'DependentImporter::existing' => 0,
            'DependentImporter::created' => 4,
            'DependentImporter::touched' => 0,
            'DependentImporter::updated' => 0,
        ];
        $this->assertEquals($expected, $sut->getFactory()->getResults()->getCounters());
        $this->assertCount(0, $sut->getFactory()->getResults()->getMessages());
    }

    public function testWithDependenciesMappedWithDependentMappedAndMatches(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sourceModels = new Collection([
            new \Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller\SourceModel([
                'id' => 1, 'reviewer_id' => 3100, 'student_id' => 3000, 'progress_report_id' => 1000,
            ]),
            new \Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller\SourceModel([
                'id' => 2, 'reviewer_id' => 3100, 'student_id' => 3001, 'progress_report_id' => 1000,
            ]),
            new \Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller\SourceModel([
                'id' => 3, 'reviewer_id' => 3101, 'student_id' => 3002, 'progress_report_id' => 1000,
            ]),
            new \Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller\SourceModel([
                'id' => 4, 'reviewer_id' => 3102, 'student_id' => 3003, 'progress_report_id' => 1000,
            ]),
        ]);
        $dependentImportMap = new Collection([
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => DependentImporter::class, 'source_id' => '3100:3000:1000',
                'target_id' => '3100:3000:1111',
            ]),
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => DependentImporter::class, 'source_id' => '3100:3001:1000',
                'target_id' => '3100:3001:1111',
            ]),
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => DependentImporter::class, 'source_id' => '3101:3002:1000',
                'target_id' => '3101:3002:1111',
            ]),
        ]);
        $targetModels = new Collection([
            (new TargetModel())->forceFill([
                'id' => 11, 'reviewer_id' => 3100, 'student_id' => 3000, 'request_id' => 1111,
            ]),
            (new TargetModel())->forceFill([
                'id' => 12, 'reviewer_id' => 3100, 'student_id' => 3001, 'request_id' => 1111,
            ]),
            (new TargetModel())->forceFill([
                'id' => 13, 'reviewer_id' => 3101, 'student_id' => 3002, 'request_id' => 1111,
            ]),
        ]);
        $this->getImportMapStorage()->shouldReceive('getByImporter')
            ->once()
            ->with(DependentImporter::class)
            ->andReturn($dependentImportMap);
        $this->getImportMapStorage()->shouldReceive('getByImporterAndIdentifiers')
            ->twice()
            ->with(DependentImporter::class, IdentifierTypes::SOURCE,
                ['3102:3003:1000'])
            ->andReturn(new Collection());
        $this->getImportMapStorage()->shouldReceive('getByImporter')
            ->once()
            ->with(DependencyImporter::class)
            ->andReturn(new Collection([
                new \Smorken\Import\Models\Eloquent\ImportMap(['source_id' => '1000', 'target_id' => '1111']),
            ]));
        $this->getTargetProvider()->shouldReceive('getByTargetIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['3100:3000:1111', '3100:3001:1111', '3101:3002:1111', '3102:3003:1111'];
                $this->assertEquals($expected, array_keys($identifiers));

                return count($identifiers) === count($expected);
            }))
            ->andReturn($targetModels);
        $this->getTargetProvider()->shouldReceive('createMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $this->assertCount(1, $models);

                return $models;
            });
        $this->getTargetProvider()->shouldReceive('updateMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $this->assertCount(0, $models);

                return 0;
            });
        $this->getTargetProvider()->shouldReceive('touchMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $this->assertCount(3, $models);

                return 3;
            });
        $this->getImportMapStorage()->shouldReceive('saveManyImporterAndArray')
            ->once()
            ->with('Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller\DependentImporter', [
                ['source_id' => '3102:3003:1000', 'target_id' => '3102:3003:1111'],
            ])
            ->andReturn(1);
        $sut->handle($sourceModels);
        $expected = [
            'DependentImporter::total' => 4,
            'DependentImporter::validated' => 4,
            'DependentImporter::existing' => 3,
            'DependentImporter::created' => 1,
            'DependentImporter::touched' => 3,
            'DependentImporter::updated' => 0,
        ];
        $this->assertEquals($expected, $sut->getFactory()->getResults()->getCounters());
        $this->assertCount(0, $sut->getFactory()->getResults()->getMessages());
    }

    public function testWithDependenciesMappedWithDependentMappedAndMatchesCanUpdate(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sourceModels = new Collection([
            new \Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller\SourceModel([
                'id' => 1, 'reviewer_id' => 3100, 'student_id' => 3000, 'progress_report_id' => 1000,
                'comments' => 'foo bar',
            ]),
            new \Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller\SourceModel([
                'id' => 2, 'reviewer_id' => 3100, 'student_id' => 3001, 'progress_report_id' => 1000,
            ]),
            new \Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller\SourceModel([
                'id' => 3, 'reviewer_id' => 3101, 'student_id' => 3002, 'progress_report_id' => 1000,
            ]),
            new \Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller\SourceModel([
                'id' => 4, 'reviewer_id' => 3102, 'student_id' => 3003, 'progress_report_id' => 1000,
            ]),
        ]);
        $dependentImportMap = new Collection([
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => DependentImporter::class, 'source_id' => '3100:3000:1000',
                'target_id' => '3100:3000:1111',
            ]),
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => DependentImporter::class, 'source_id' => '3100:3001:1000',
                'target_id' => '3100:3001:1111',
            ]),
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => DependentImporter::class, 'source_id' => '3101:3002:1000',
                'target_id' => '3101:3002:1111',
            ]),
        ]);
        $targetModels = new Collection([
            (new TargetModel())->forceFill([
                'id' => 11, 'reviewer_id' => 3100, 'student_id' => 3000, 'request_id' => 1111,
                'comments' => 'fizz buzz',
            ]),
            (new TargetModel())->forceFill([
                'id' => 12, 'reviewer_id' => 3100, 'student_id' => 3001, 'request_id' => 1111,
            ]),
            (new TargetModel())->forceFill([
                'id' => 13, 'reviewer_id' => 3101, 'student_id' => 3002, 'request_id' => 1111,
            ]),
        ]);
        $this->getImportMapStorage()->shouldReceive('getByImporter')
            ->once()
            ->with(DependentImporter::class)
            ->andReturn($dependentImportMap);
        $this->getImportMapStorage()->shouldReceive('getByImporterAndIdentifiers')
            ->twice()
            ->with(DependentImporter::class, IdentifierTypes::SOURCE,
                ['3102:3003:1000'])
            ->andReturn(new Collection());
        $this->getImportMapStorage()->shouldReceive('getByImporter')
            ->once()
            ->with(DependencyImporter::class)
            ->andReturn(new Collection([
                new \Smorken\Import\Models\Eloquent\ImportMap(['source_id' => '1000', 'target_id' => '1111']),
            ]));
        $this->getTargetProvider()->shouldReceive('getByTargetIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['3100:3000:1111', '3100:3001:1111', '3101:3002:1111', '3102:3003:1111'];
                $this->assertEquals($expected, array_keys($identifiers));

                return count($identifiers) === count($expected);
            }))
            ->andReturn($targetModels);
        $this->getTargetProvider()->shouldReceive('createMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $this->assertCount(1, $models);

                return $models;
            });
        $this->getTargetProvider()->shouldReceive('updateMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $this->assertCount(1, $models);

                return 1;
            });
        $this->getTargetProvider()->shouldReceive('touchMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $this->assertCount(2, $models);

                return 2;
            });
        $this->getImportMapStorage()->shouldReceive('saveManyImporterAndArray')
            ->once()
            ->with('Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller\DependentImporter', [
                ['source_id' => '3102:3003:1000', 'target_id' => '3102:3003:1111'],
            ])
            ->andReturn(1);
        $sut->handle($sourceModels);
        $expected = [
            'DependentImporter::total' => 4,
            'DependentImporter::validated' => 4,
            'DependentImporter::existing' => 3,
            'DependentImporter::created' => 1,
            'DependentImporter::touched' => 2,
            'DependentImporter::updated' => 1,
        ];
        $this->assertEquals($expected, $sut->getFactory()->getResults()->getCounters());
        $this->assertCount(0, $sut->getFactory()->getResults()->getMessages());
    }

    public function testWithNoDependenciesMappedIsValidationErrors(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sourceModels = new Collection([
            new \Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller\SourceModel([
                'id' => 1, 'reviewer_id' => 3100, 'student_id' => 3000, 'progress_report_id' => 1000,
            ]),
            new \Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller\SourceModel([
                'id' => 2, 'reviewer_id' => 3100, 'student_id' => 3001, 'progress_report_id' => 1000,
            ]),
            new \Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller\SourceModel([
                'id' => 3, 'reviewer_id' => 3101, 'student_id' => 3002, 'progress_report_id' => 1000,
            ]),
            new \Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller\SourceModel([
                'id' => 4, 'reviewer_id' => 3102, 'student_id' => 3003, 'progress_report_id' => 1000,
            ]),
        ]);
        $this->getImportMapStorage()->shouldReceive('getByImporter')
            ->once()
            ->with(DependentImporter::class)
            ->andReturn(new Collection());
        $this->getImportMapStorage()->shouldReceive('getByImporterAndIdentifiers')
            ->once()
            ->with(DependentImporter::class, IdentifierTypes::SOURCE,
                ['3100:3000:1000', '3100:3001:1000', '3101:3002:1000', '3102:3003:1000'])
            ->andReturn(new Collection());
        $this->getImportMapStorage()->shouldReceive('getByImporter')
            ->once()
            ->with(DependencyImporter::class)
            ->andReturn(new Collection());
        $this->getImportMapStorage()->shouldReceive('getByImporterAndIdentifiers')
            ->once()
            ->with(DependencyImporter::class, IdentifierTypes::SOURCE, [1000])
            ->andReturn(new Collection());
        $this->getTargetProvider()->shouldReceive('getByTargetIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = [];
                $this->assertEquals($expected, array_keys($identifiers));

                return count($identifiers) === count($expected);
            }))
            ->andReturn(new Collection());
        $this->getTargetProvider()->shouldReceive('createMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $this->assertCount(0, $models);

                return $models;
            });
        $this->getTargetProvider()->shouldReceive('updateMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $this->assertCount(0, $models);

                return 0;
            });
        $this->getTargetProvider()->shouldReceive('touchMany')
            ->once()
            ->with(m::type(Collection::class))
            ->andReturnUsing(function (Collection $models) {
                $this->assertCount(0, $models);

                return 0;
            });
        $this->getImportMapStorage()->shouldReceive('saveManyImporterAndArray')
            ->once()
            ->with('Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller\DependentImporter', [])
            ->andReturn(0);
        $sut->handle($sourceModels);
        $expected = [
            'DependentImporter::total' => 4,
            'DependentImporter::validated' => 0,
            'DependentImporter::existing' => 0,
            'DependentImporter::created' => 0,
            'DependentImporter::touched' => 0,
            'DependentImporter::updated' => 0,
        ];
        $this->assertEquals($expected, $sut->getFactory()->getResults()->getCounters());
        $this->assertCount(4, $sut->getFactory()->getResults()->getMessages());
    }

    protected function getImportMapStorage(): ImportMap
    {
        if (is_null($this->importMapStorage)) {
            $this->importMapStorage = m::mock(ImportMap::class);
            $this->importMapStorage->shouldReceive('getModel')
                ->andReturn(new \Smorken\Import\Models\Eloquent\ImportMap());
        }

        return $this->importMapStorage;
    }

    protected function getPartsFactory(array $overrides = []): PartsFactory
    {
        $defaults = [
            PartsFactoryParts::SOURCE_PROVIDER => $this->getSourceProvider(),
            PartsFactoryParts::LOOKUPS => new Lookups(['default' => new MapId()]),
            PartsFactoryParts::RESULTS => new Results(),
            PartsFactoryParts::DATA_MODEL => new DataModel(),
            PartsFactoryParts::TARGET_PROVIDER => $this->getTargetProvider(),
            PartsFactoryParts::DATA_TARGET_MODELLER => new Modeller(),
        ];

        return new \Smorken\Import\PartsFactory(array_merge($defaults, $overrides));
    }

    protected function getSourceProvider(): HasSource
    {
        if (is_null($this->sourceStorage)) {
            $this->sourceStorage = m::mock(HasSource::class);
            $this->sourceStorage->shouldReceive('getSource')
                ->andReturn(new SourceModel());
        }

        return $this->sourceStorage;
    }

    protected function getSut(PartsFactory $partsFactory): DependentImporter
    {
        ImportMapper::$importMapProvider = $this->getImportMapStorage();

        return new DependentImporter($partsFactory);
    }

    protected function getTargetProvider(): HasTarget
    {
        if (is_null($this->targetStorage)) {
            $this->targetStorage = m::mock(HasTarget::class);
            $this->targetStorage->shouldReceive('getTarget')
                ->andReturn(new TargetModel());
        }

        return $this->targetStorage;
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
