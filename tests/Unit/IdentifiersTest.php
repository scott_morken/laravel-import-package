<?php

namespace Tests\Smorken\Import\Unit;

use PHPUnit\Framework\TestCase;
use Smorken\Import\Contracts\Enums\IdentifierTypes;
use Tests\Smorken\Import\Unit\Stubs\MultiColDataModel;
use Tests\Smorken\Import\Unit\Stubs\MultiColTargetModel;
use Tests\Smorken\Import\Unit\Stubs\MultiColVirtualSourceTargetModel;
use Tests\Smorken\Import\Unit\Stubs\SingleColDataModel;
use Tests\Smorken\Import\Unit\Stubs\SingleColTargetModel;
use Tests\Smorken\Import\Unit\Stubs\SingleColVirtualSourceTargetModel;

class IdentifiersTest extends TestCase
{
    public function testMultiColSourceAsString(): void
    {
        $m = (new MultiColDataModel())->fromModel(['SC_ID' => 'id1', 'SC_ANOTHER' => 'id2']);
        $sut = $m->getIdentifiers();
        $this->assertEquals('id1:id2', $sut->asString(IdentifierTypes::SOURCE));
    }

    public function testMultiColSourceGet(): void
    {
        $m = (new MultiColDataModel())->fromModel(['SC_ID' => 'id1', 'SC_ANOTHER' => 'id2']);
        $sut = $m->getIdentifiers();
        $this->assertEquals([
            'external_id' => 'id1',
            'another_id' => 'id2',
        ], $sut->get(IdentifierTypes::SOURCE));
    }

    public function testMultiColSourceGetSingleAttributeIsException(): void
    {
        $m = new MultiColDataModel(['SC_ID' => 'id1', 'SC_ANOTHER' => 'id2']);
        $sut = $m->getIdentifiers();
        $this->expectException(\OutOfBoundsException::class);
        $this->expectExceptionMessage('source is not a single column attribute.');
        $sut->getSingleAttribute(IdentifierTypes::SOURCE);
    }

    public function testMultiColSourceGetSingleValueIsException(): void
    {
        $m = new MultiColDataModel(['SC_ID' => 'id1', 'SC_ANOTHER' => 'id2']);
        $sut = $m->getIdentifiers();
        $this->expectException(\OutOfBoundsException::class);
        $this->expectExceptionMessage('source is not a single column value.');
        $sut->getSingleValue(IdentifierTypes::SOURCE);
    }

    public function testMultiColSourceIsSingleAttributeIsFalse(): void
    {
        $m = new MultiColDataModel(['SC_ID' => 'id1', 'SC_ANOTHER' => 'id2']);
        $sut = $m->getIdentifiers();
        $this->assertFalse($sut->isSingleAttribute(IdentifierTypes::SOURCE));
    }

    public function testMultiColSourceSetNotKeyedCanSet(): void
    {
        $m = new MultiColDataModel(['SC_ID' => 'id1', 'SC_ANOTHER' => 'id2']);
        $sut = $m->getIdentifiers();
        $sut->set(IdentifierTypes::SOURCE, 'foo:bar', false);
        $this->assertEquals(['external_id' => 'foo', 'another_id' => 'bar'], $sut->get(IdentifierTypes::SOURCE));
    }

    public function testMultiColSourceSetNotMatchingAttributesIsException(): void
    {
        $m = new MultiColDataModel(['SC_ID' => 'id1', 'SC_ANOTHER' => 'id2']);
        $sut = $m->getIdentifiers();
        $this->expectException(\OutOfBoundsException::class);
        $this->expectExceptionMessage('Count of attributes does not match keys in identifier.');
        $sut->set(IdentifierTypes::SOURCE, ['external_id' => 'foo']);
    }

    public function testMultiColTargetAsString(): void
    {
        $m = new MultiColTargetModel(['id_1' => 'id1', 'id_2' => 'id2']);
        $sut = $m->getIdentifiers();
        $this->assertEquals('id1:id2', $sut->asString(IdentifierTypes::TARGET));
    }

    public function testMultiColTargetGet(): void
    {
        $m = new MultiColTargetModel(['id_1' => 'id1', 'id_2' => 'id2']);
        $sut = $m->getIdentifiers();
        $this->assertEquals([
            'id_1' => 'id1',
            'id_2' => 'id2',
        ], $sut->get(IdentifierTypes::TARGET));
    }

    public function testMultiColTargetGetSingleAttributeIsException(): void
    {
        $m = new MultiColTargetModel(['id_1' => 'id1', 'id_2' => 'id2']);
        $sut = $m->getIdentifiers();
        $this->expectException(\OutOfBoundsException::class);
        $this->expectExceptionMessage('target is not a single column attribute.');
        $sut->getSingleAttribute(IdentifierTypes::TARGET);
    }

    public function testMultiColTargetGetSingleValueIsException(): void
    {
        $m = new MultiColTargetModel(['id_1' => 'id1', 'id_2' => 'id2']);
        $sut = $m->getIdentifiers();
        $this->expectException(\OutOfBoundsException::class);
        $this->expectExceptionMessage('target is not a single column value.');
        $sut->getSingleValue(IdentifierTypes::TARGET);
    }

    public function testMultiColTargetTargetAsAttribute(): void
    {
        $m = new MultiColVirtualSourceTargetModel([
            'id_1' => '1', 'id_2' => '2', 'source_identifier' => ['external_id' => 99, 'another_id' => 88],
        ]);
        $this->assertEquals(['id_1' => '1', 'id_2' => '2'], $m->target_identifier);
    }

    public function testMultiColVirtualSourceTargetAsAttribute(): void
    {
        $m = new MultiColVirtualSourceTargetModel([
            'id' => '1', 'source_identifier' => ['external_id' => 99, 'another_id' => 88],
        ]);
        $this->assertEquals(['external_id' => 99, 'another_id' => 88], $m->source_identifier);
    }

    public function testMultiColVirtualSourceTargetAsString(): void
    {
        $m = new MultiColVirtualSourceTargetModel([
            'id' => '1', 'source_identifier' => ['external_id' => 99, 'another_id' => 88],
        ]);
        $sut = $m->getIdentifiers();
        $this->assertEquals('99:88', $sut->asString(IdentifierTypes::SOURCE));
    }

    public function testSingleColSourceAsString(): void
    {
        $m = (new SingleColDataModel())->fromModel(['SC_ID' => 'id1']);
        $sut = $m->getIdentifiers();
        $this->assertEquals('id1', $sut->asString(IdentifierTypes::SOURCE));
    }

    public function testSingleColSourceGet(): void
    {
        $m = (new SingleColDataModel())->fromModel(['SC_ID' => 'id1']);
        $sut = $m->getIdentifiers();
        $this->assertEquals([
            'external_id' => 'id1',
        ], $sut->get(IdentifierTypes::SOURCE));
    }

    public function testSingleColSourceIsSingleAttributeIsTrue(): void
    {
        $m = new SingleColDataModel(['SC_ID' => 'id1']);
        $sut = $m->getIdentifiers();
        $this->assertTrue($sut->isSingleAttribute(IdentifierTypes::SOURCE));
    }

    public function testSingleColTargetAsString(): void
    {
        $m = new SingleColTargetModel(['id' => '1']);
        $sut = $m->getIdentifiers();
        $this->assertEquals('1', $sut->asString(IdentifierTypes::TARGET));
    }

    public function testSingleColTargetGet(): void
    {
        $m = new SingleColTargetModel(['id' => '1']);
        $sut = $m->getIdentifiers();
        $this->assertEquals([
            'id' => '1',
        ], $sut->get(IdentifierTypes::TARGET));
    }

    public function testSingleColTargetIsSingleAttributeIsTrue(): void
    {
        $m = new SingleColTargetModel(['id' => '1']);
        $sut = $m->getIdentifiers();
        $this->assertTrue($sut->isSingleAttribute(IdentifierTypes::TARGET));
    }

    public function testSingleColVirtualSourceTargetAsString(): void
    {
        $m = new SingleColVirtualSourceTargetModel(['id' => '1', 'source_identifier' => ['external_id' => 99]]);
        $sut = $m->getIdentifiers();
        $this->assertEquals('99', $sut->asString(IdentifierTypes::SOURCE));
    }
}
