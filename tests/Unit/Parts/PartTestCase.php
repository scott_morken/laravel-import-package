<?php

namespace Tests\Smorken\Import\Unit\Parts;

use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Import\Contracts\Enums\PartsFactoryParts;
use Smorken\Import\Contracts\Importer;
use Smorken\Import\Contracts\Models\Data;
use Smorken\Import\Contracts\Models\Source;
use Smorken\Import\Contracts\Models\Target;
use Smorken\Import\Contracts\Storage\HasSource;
use Smorken\Import\Contracts\Storage\HasTarget;
use Smorken\Import\DataAndTargetModeller;
use Smorken\Import\Lookups;
use Smorken\Import\MapId;
use Smorken\Import\PartsFactory;
use Smorken\Import\Results;

abstract class PartTestCase extends TestCase
{
    protected string $baseName = '';

    abstract protected function getDataModel(array $attributes = []): Data;

    abstract protected function getSourceModel(array $attributes = []): Source;

    abstract protected function getTargetModel(array $attributes = []): Target;

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    protected function createDataModelsFromSourceModels(Importer $sut, Collection $importModels): Collection
    {
        return $importModels->map(function ($importModel) use ($sut) {
            return $sut->getFactory()->getDataModel()->fromModel($importModel);
        });
    }

    /**
     * @return \Illuminate\Support\Collection<\Smorken\Import\Contracts\Models\Source>
     */
    protected function createSourceModelModels(int $count = 5): Collection
    {
        $coll = new Collection();
        for ($i = 0; $i < $count; $i++) {
            $id = 'ID'.$i;
            $m = $this->getSourceModel(['SC_ID' => $id, 'SC_NAME' => 'Name '.$i]);
            $coll->put($id, $m);
        }

        return $coll;
    }

    protected function createTargetsFromDataModels(Importer $sut, Collection $dataModels): Collection
    {
        $count = 1;

        return $dataModels->map(function ($dataModel) use ($sut, &$count) {
            $m = $sut->getFactory()->getTargetProvider()->getTarget()->fromDataModel($dataModel);
            $m->forceFill(['id' => $count]);
            $count++;

            return $m;
        });
    }

    protected function getPartsFactory(): \Smorken\Import\Contracts\PartsFactory
    {
        $parts = new PartsFactory([
            PartsFactoryParts::RESULTS => new Results($this->baseName),
            PartsFactoryParts::TARGET_PROVIDER => m::mock(HasTarget::class),
            PartsFactoryParts::SOURCE_PROVIDER => m::mock(HasSource::class),
            PartsFactoryParts::DATA_MODEL => $this->getDataModel(),
            PartsFactoryParts::LOOKUPS => new Lookups(['default' => new MapId()]),
            PartsFactoryParts::DATA_TARGET_MODELLER => new DataAndTargetModeller(),
        ]);
        $parts->getTargetProvider()
            ->shouldReceive('getTarget')
            ->andReturn($this->getTargetModel());

        return $parts;
    }

    protected function getSut(\Smorken\Import\Contracts\PartsFactory $partsFactory): Importer
    {
        return new \Smorken\Import\Importer($partsFactory);
    }
}
