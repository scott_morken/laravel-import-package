<?php

namespace Tests\Smorken\Import\Unit\Parts;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Event;
use Mockery as m;
use Smorken\Import\Contracts\Enums\IdentifierTypes;
use Smorken\Import\Contracts\Models\Data;
use Smorken\Import\Contracts\Models\Source;
use Smorken\Import\Contracts\Models\Target;
use Smorken\Import\Events\ImportResults;
use Smorken\Support\Filter;
use Tests\Smorken\Import\Unit\Stubs\SingleColDataModel;
use Tests\Smorken\Import\Unit\Stubs\SingleColSourceModel;
use Tests\Smorken\Import\Unit\Stubs\SingleColTargetModel;

class SingleColsTest extends PartTestCase
{
    protected string $baseName = 'SingleColsImporter';

    public function testHandleWithExistingTargetModelsNoChanges(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $importModels = $this->createSourceModelModels();
        $dataModels = $this->createDataModelsFromSourceModels($sut, $importModels);
        $targetModels = $this->createTargetsFromDataModels($sut, $dataModels);
        $sut->getFactory()
            ->getTargetProvider()
            ->shouldReceive('getBySourceIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['ID0', 'ID1', 'ID2', 'ID3', 'ID4'];
                foreach ($identifiers as $identifier) {
                    $key = $identifier->asString(IdentifierTypes::SOURCE);
                    if (! in_array($key, $expected)) {
                        return false;
                    }
                }

                return count($identifiers) === count($expected);
            }))
            ->andReturn($targetModels);
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('createMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 0;
            }))
            ->andReturn(new Collection());
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('updateMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 0;
            }))
            ->andReturn(0);
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('touchMany')
            ->once()
            ->with(m::on(function (Collection $models) use ($importModels) {
                return $models->count() === $importModels->count();
            }))
            ->andReturn($importModels->count());
        $sut->handle($importModels);
        $expected = [
            'SingleColsImporter::total' => 5,
            'SingleColsImporter::validated' => 5,
            'SingleColsImporter::existing' => 5,
            'SingleColsImporter::created' => 0,
            'SingleColsImporter::touched' => 5,
            'SingleColsImporter::updated' => 0,
        ];
        $this->assertEquals($expected, $sut->getFactory()->getResults()->getCounters());
    }

    public function testHandleWithSomeExistingTargetModelsNoChanges(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $importModels = $this->createSourceModelModels();
        $dataModels = $this->createDataModelsFromSourceModels($sut, $importModels);
        $targetModels = $this->createTargetsFromDataModels($sut, $dataModels);
        $sut->getFactory()
            ->getTargetProvider()
            ->shouldReceive('getBySourceIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['ID0', 'ID1', 'ID2', 'ID3', 'ID4'];
                foreach ($identifiers as $identifier) {
                    $key = $identifier->asString(IdentifierTypes::SOURCE);
                    if (! in_array($key, $expected)) {
                        return false;
                    }
                }

                return count($identifiers) === count($expected);
            }))
            ->andReturn($targetModels->slice(0, 1));
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('createMany')
            ->once()
            ->with(m::on(function (Collection $models) use ($targetModels) {
                $sliced = $targetModels->slice(1);
                $check = ['name', 'external_id'];
                foreach ($models as $k => $m) {
                    $t = $sliced->get($k);
                    foreach ($check as $attr) {
                        if ($t->getAttribute($attr) !== $m->getAttribute($attr)) {
                            return false;
                        }
                    }
                }

                return $models->count() === $sliced->count();
            }))
            ->andReturn($targetModels->slice(1));
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('updateMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 0;
            }))
            ->andReturn(0);
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('touchMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 1;
            }))
            ->andReturn(1);
        $sut->handle($importModels);
        $expected = [
            'SingleColsImporter::total' => 5,
            'SingleColsImporter::validated' => 5,
            'SingleColsImporter::existing' => 1,
            'SingleColsImporter::created' => 4,
            'SingleColsImporter::touched' => 1,
            'SingleColsImporter::updated' => 0,
        ];
        $this->assertEquals($expected, $sut->getFactory()->getResults()->getCounters());
    }

    public function testHandleWithSomeExistingTargetModelsWithChanges(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $importModels = $this->createSourceModelModels();
        $dataModels = $this->createDataModelsFromSourceModels($sut, $importModels);
        $targetModels = $this->createTargetsFromDataModels($sut, $dataModels);
        $sut->getFactory()
            ->getTargetProvider()
            ->shouldReceive('getBySourceIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['ID0', 'ID1', 'ID2', 'ID3', 'ID4'];
                foreach ($identifiers as $identifier) {
                    $key = $identifier->asString(IdentifierTypes::SOURCE);
                    if (! in_array($key, $expected)) {
                        return false;
                    }
                }

                return count($identifiers) === count($expected);
            }))
            ->andReturn($targetModels->slice(0, 2));
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('createMany')
            ->once()
            ->with(m::on(function (Collection $models) use ($targetModels) {
                $sliced = $targetModels->slice(2);
                $check = ['name', 'external_id'];
                foreach ($models as $k => $m) {
                    $t = $sliced->get($k);
                    foreach ($check as $attr) {
                        if ($t->getAttribute($attr) !== $m->getAttribute($attr)) {
                            return false;
                        }
                    }
                }

                return $models->count() === $sliced->count();
            }))
            ->andReturn($targetModels->slice(2));
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('updateMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 1 && $models->first()->name === 'Foo Bar';
            }))
            ->andReturn(1);
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('touchMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 1;
            }))
            ->andReturn(1);
        $importModels->get('ID1')->SC_NAME = 'Foo Bar';
        $sut->handle($importModels);
        $expected = [
            'SingleColsImporter::total' => 5,
            'SingleColsImporter::validated' => 5,
            'SingleColsImporter::existing' => 2,
            'SingleColsImporter::created' => 3,
            'SingleColsImporter::touched' => 1,
            'SingleColsImporter::updated' => 1,
        ];
        $this->assertEquals($expected, $sut->getFactory()->getResults()->getCounters());
    }

    public function testHandleWithoutExistingTargetModels(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $importModels = $this->createSourceModelModels();
        $dataModels = $this->createDataModelsFromSourceModels($sut, $importModels);
        $targetModels = $this->createTargetsFromDataModels($sut, $dataModels);
        $sut->getFactory()
            ->getTargetProvider()
            ->shouldReceive('getBySourceIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['ID0', 'ID1', 'ID2', 'ID3', 'ID4'];
                foreach ($identifiers as $identifier) {
                    $key = $identifier->asString(IdentifierTypes::SOURCE);
                    if (! in_array($key, $expected)) {
                        return false;
                    }
                }

                return count($identifiers) === count($expected);
            }))
            ->andReturn(new Collection());
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('createMany')
            ->once()
            ->with(m::on(function (Collection $models) use ($targetModels) {
                $check = ['name', 'external_id'];
                foreach ($models as $k => $m) {
                    $t = $targetModels->get($k);
                    foreach ($check as $attr) {
                        if ($t->getAttribute($attr) !== $m->getAttribute($attr)) {
                            return false;
                        }
                    }
                }

                return $models->count() === $targetModels->count();
            }))
            ->andReturn($targetModels);
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('updateMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 0;
            }))
            ->andReturn(0);
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('touchMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 0;
            }))
            ->andReturn(0);
        $sut->handle($importModels);
        $expected = [
            'SingleColsImporter::total' => 5,
            'SingleColsImporter::validated' => 5,
            'SingleColsImporter::existing' => 0,
            'SingleColsImporter::created' => 5,
            'SingleColsImporter::touched' => 0,
            'SingleColsImporter::updated' => 0,
        ];
        $this->assertEquals($expected, $sut->getFactory()->getResults()->getCounters());
    }

    public function testImportWithExistingTargetModelsNoChanges(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $importModels = $this->createSourceModelModels();
        $dataModels = $this->createDataModelsFromSourceModels($sut, $importModels);
        $targetModels = $this->createTargetsFromDataModels($sut, $dataModels);
        $sut->getFactory()
            ->getSourceProvider()
            ->shouldReceive('forImport')
            ->once()
            ->andReturnUsing(function (\Smorken\Support\Contracts\Filter $filter, callable $callback, int $perPage) use (
                $importModels
            ) {
                $callback($importModels);

            });
        $sut->getFactory()
            ->getTargetProvider()
            ->shouldReceive('getBySourceIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['ID0', 'ID1', 'ID2', 'ID3', 'ID4'];
                foreach ($identifiers as $identifier) {
                    $key = $identifier->asString(IdentifierTypes::SOURCE);
                    if (! in_array($key, $expected)) {
                        return false;
                    }
                }

                return count($identifiers) === count($expected);
            }))
            ->andReturn($targetModels);
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('createMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 0;
            }))
            ->andReturn(new Collection());
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('updateMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 0;
            }))
            ->andReturn(0);
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('touchMany')
            ->once()
            ->with(m::on(function (Collection $models) use ($importModels) {
                return $models->count() === $importModels->count();
            }))
            ->andReturn($importModels->count());
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('cleanup')
            ->once()
            ->with(m::type(\Smorken\Support\Contracts\Filter::class))
            ->andReturn(0);
        Event::shouldReceive('dispatch')->once()->with(m::type(ImportResults::class));
        $sut->import(new Filter());
        $expected = [
            'SingleColsImporter::total' => 5,
            'SingleColsImporter::validated' => 5,
            'SingleColsImporter::existing' => 5,
            'SingleColsImporter::created' => 0,
            'SingleColsImporter::touched' => 5,
            'SingleColsImporter::updated' => 0,
            'SingleColsImporter::deleted' => 0,
        ];
        $this->assertEquals($expected, $sut->getFactory()->getResults()->getCounters());
    }

    public function testSetsResultsBase(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sut->getFactory()->getResults()->increment('foo');
        $expected = [
            'SingleColsImporter::foo' => 1,
        ];
        $this->assertEquals($expected, $sut->getFactory()->getResults()->getCounters());
    }

    protected function getDataModel(array $attributes = []): Data
    {
        return new SingleColDataModel($attributes);
    }

    protected function getSourceModel(array $attributes = []): Source
    {
        return new SingleColSourceModel($attributes);
    }

    protected function getTargetModel(array $attributes = []): Target
    {
        return new SingleColTargetModel($attributes);
    }
}
