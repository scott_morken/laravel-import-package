<?php

namespace Tests\Smorken\Import\Unit\Parts;

use Illuminate\Support\Collection;
use Mockery as m;
use Smorken\Import\Contracts\Enums\IdentifierTypes;
use Smorken\Import\Contracts\Models\Data;
use Smorken\Import\Contracts\Models\Source;
use Smorken\Import\Contracts\Models\Target;
use Tests\Smorken\Import\Unit\Stubs\MultiColDataModel;
use Tests\Smorken\Import\Unit\Stubs\MultiColSourceModel;
use Tests\Smorken\Import\Unit\Stubs\MultiColTargetModel;

class MultiColsTest extends PartTestCase
{
    protected string $baseName = 'MultiColsImporter';

    public function testHandleWithExistingTargetModelsNoChanges(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $importModels = $this->createSourceModelModels();
        $dataModels = $this->createDataModelsFromSourceModels($sut, $importModels);
        $targetModels = $this->createTargetsFromDataModels($sut, $dataModels);
        $sut->getFactory()
            ->getTargetProvider()
            ->shouldReceive('getBySourceIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['ID0:A-ID0', 'ID1:A-ID1', 'ID2:A-ID2', 'ID3:A-ID3', 'ID4:A-ID4'];
                foreach ($identifiers as $identifier) {
                    $key = $identifier->asString(IdentifierTypes::SOURCE);
                    if (! in_array($key, $expected)) {
                        return false;
                    }
                }

                return count($identifiers) === count($expected);
            }))
            ->andReturn($targetModels);
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('createMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 0;
            }))
            ->andReturn(new Collection());
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('updateMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 0;
            }))
            ->andReturn(0);
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('touchMany')
            ->once()
            ->with(m::on(function (Collection $models) use ($importModels) {
                return $models->count() === $importModels->count();
            }))
            ->andReturn($importModels->count());
        $sut->handle($importModels);
        $expected = [
            'MultiColsImporter::total' => 5,
            'MultiColsImporter::validated' => 5,
            'MultiColsImporter::existing' => 5,
            'MultiColsImporter::created' => 0,
            'MultiColsImporter::touched' => 5,
            'MultiColsImporter::updated' => 0,
        ];
        $this->assertEquals($expected, $sut->getFactory()->getResults()->getCounters());
    }

    public function testHandleWithSomeExistingTargetModelsNoChanges(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $importModels = $this->createSourceModelModels();
        $dataModels = $this->createDataModelsFromSourceModels($sut, $importModels);
        $targetModels = $this->createTargetsFromDataModels($sut, $dataModels);
        $sut->getFactory()
            ->getTargetProvider()
            ->shouldReceive('getBySourceIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['ID0:A-ID0', 'ID1:A-ID1', 'ID2:A-ID2', 'ID3:A-ID3', 'ID4:A-ID4'];
                foreach ($identifiers as $identifier) {
                    $key = $identifier->asString(IdentifierTypes::SOURCE);
                    if (! in_array($key, $expected)) {
                        return false;
                    }
                }

                return count($identifiers) === count($expected);
            }))
            ->andReturn($targetModels->slice(0, 1));
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('createMany')
            ->once()
            ->with(m::on(function (Collection $models) use ($targetModels) {
                $sliced = $targetModels->slice(1);
                $check = ['name', 'external_id', 'another_id'];
                foreach ($models as $k => $m) {
                    $t = $sliced->get($k);
                    foreach ($check as $attr) {
                        if ($t->getAttribute($attr) !== $m->getAttribute($attr)) {
                            return false;
                        }
                    }
                }

                return $models->count() === $sliced->count();
            }))
            ->andReturn($targetModels->slice(1));
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('updateMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 0;
            }))
            ->andReturn(0);
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('touchMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 1;
            }))
            ->andReturn(1);
        $sut->handle($importModels);
        $expected = [
            'MultiColsImporter::total' => 5,
            'MultiColsImporter::validated' => 5,
            'MultiColsImporter::existing' => 1,
            'MultiColsImporter::created' => 4,
            'MultiColsImporter::touched' => 1,
            'MultiColsImporter::updated' => 0,
        ];
        $this->assertEquals($expected, $sut->getFactory()->getResults()->getCounters());
    }

    public function testHandleWithSomeExistingTargetModelsWithChanges(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $importModels = $this->createSourceModelModels();
        $dataModels = $this->createDataModelsFromSourceModels($sut, $importModels);
        $targetModels = $this->createTargetsFromDataModels($sut, $dataModels);
        $sut->getFactory()
            ->getTargetProvider()
            ->shouldReceive('getBySourceIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['ID0:A-ID0', 'ID1:A-ID1', 'ID2:A-ID2', 'ID3:A-ID3', 'ID4:A-ID4'];
                foreach ($identifiers as $identifier) {
                    $key = $identifier->asString(IdentifierTypes::SOURCE);
                    if (! in_array($key, $expected)) {
                        return false;
                    }
                }

                return count($identifiers) === count($expected);
            }))
            ->andReturn($targetModels->slice(0, 2));
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('createMany')
            ->once()
            ->with(m::on(function (Collection $models) use ($targetModels) {
                $sliced = $targetModels->slice(2);
                $check = ['name', 'external_id'];
                foreach ($models as $k => $m) {
                    $t = $sliced->get($k);
                    foreach ($check as $attr) {
                        if ($t->getAttribute($attr) !== $m->getAttribute($attr)) {
                            return false;
                        }
                    }
                }

                return $models->count() === $sliced->count();
            }))
            ->andReturn($targetModels->slice(2));
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('updateMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 1 && $models->first()->name === 'Foo Bar';
            }))
            ->andReturn(1);
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('touchMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 1;
            }))
            ->andReturn(1);
        $importModels->get('ID1:A-ID1')->SC_NAME = 'Foo Bar';
        $sut->handle($importModels);
        $expected = [
            'MultiColsImporter::total' => 5,
            'MultiColsImporter::validated' => 5,
            'MultiColsImporter::existing' => 2,
            'MultiColsImporter::created' => 3,
            'MultiColsImporter::touched' => 1,
            'MultiColsImporter::updated' => 1,
        ];
        $this->assertEquals($expected, $sut->getFactory()->getResults()->getCounters());
    }

    public function testHandleWithoutExistingLocalModels(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $importModels = $this->createSourceModelModels();
        $dataModels = $this->createDataModelsFromSourceModels($sut, $importModels);
        $targetModels = $this->createTargetsFromDataModels($sut, $dataModels);
        $sut->getFactory()
            ->getTargetProvider()
            ->shouldReceive('getBySourceIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['ID0:A-ID0', 'ID1:A-ID1', 'ID2:A-ID2', 'ID3:A-ID3', 'ID4:A-ID4'];
                foreach ($identifiers as $identifier) {
                    $key = $identifier->asString(IdentifierTypes::SOURCE);
                    if (! in_array($key, $expected)) {
                        return false;
                    }
                }

                return count($identifiers) === count($expected);
            }))
            ->andReturn(new Collection());
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('createMany')
            ->once()
            ->with(m::on(function (Collection $models) use ($targetModels) {
                $check = ['name', 'external_id'];
                foreach ($models as $k => $m) {
                    $t = $targetModels->get($k);
                    foreach ($check as $attr) {
                        if ($t->getAttribute($attr) !== $m->getAttribute($attr)) {
                            return false;
                        }
                    }
                }

                return $models->count() === $targetModels->count();
            }))
            ->andReturn($targetModels);
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('updateMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 0;
            }))
            ->andReturn(0);
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('touchMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 0;
            }))
            ->andReturn(0);
        $sut->handle($importModels);
        $expected = [
            'MultiColsImporter::total' => 5,
            'MultiColsImporter::validated' => 5,
            'MultiColsImporter::existing' => 0,
            'MultiColsImporter::created' => 5,
            'MultiColsImporter::touched' => 0,
            'MultiColsImporter::updated' => 0,
        ];
        $this->assertEquals($expected, $sut->getFactory()->getResults()->getCounters());
    }

    public function testSetsResultsBase(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sut->getFactory()->getResults()->increment('foo');
        $expected = [
            'MultiColsImporter::foo' => 1,
        ];
        $this->assertEquals($expected, $sut->getFactory()->getResults()->getCounters());
    }

    /**
     * @return \Illuminate\Support\Collection<\Smorken\Import\Contracts\Models\Source>
     */
    protected function createSourceModelModels(int $count = 5): Collection
    {
        $coll = new Collection();
        for ($i = 0; $i < $count; $i++) {
            $id = 'ID'.$i;
            $key = $id.':A-'.$id;
            $coll->put($key,
                $this->getSourceModel(['SC_ID' => $id, 'SC_ANOTHER' => 'A-'.$id, 'SC_NAME' => 'Name '.$i]));
        }

        return $coll;
    }

    protected function getDataModel(array $attributes = []): Data
    {
        return new MultiColDataModel($attributes);
    }

    protected function getSourceModel(array $attributes = []): Source
    {
        return new MultiColSourceModel($attributes);
    }

    protected function getTargetModel(array $attributes = []): Target
    {
        return new MultiColTargetModel($attributes);
    }
}
