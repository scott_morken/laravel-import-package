<?php

namespace Tests\Smorken\Import\Unit\Modellers;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Import\Contracts\Enums\IdentifierTypes;
use Smorken\Import\Contracts\Enums\PartsFactoryParts;
use Smorken\Import\Contracts\PartsFactory;
use Smorken\Import\Contracts\Storage\ImportMap;
use Smorken\Import\Lookups;
use Smorken\Import\MapId;
use Smorken\Import\Results;
use Tests\Smorken\Import\Unit\Stubs\SingIeImportMapperModeller\DataModel;
use Tests\Smorken\Import\Unit\Stubs\SingIeImportMapperModeller\Modeller;
use Tests\Smorken\Import\Unit\Stubs\SingIeImportMapperModeller\SingleImporter;
use Tests\Smorken\Import\Unit\Stubs\SingIeImportMapperModeller\SourceModel;
use Tests\Smorken\Import\Unit\Stubs\SingIeImportMapperModeller\SourceStorage;
use Tests\Smorken\Import\Unit\Stubs\SingIeImportMapperModeller\TargetModel;
use Tests\Smorken\Import\Unit\Stubs\SingIeImportMapperModeller\TargetStorage;

class ModellerWithSingleImportMapperTest extends TestCase
{
    protected ?SourceStorage $sourceStorage = null;

    protected ?TargetStorage $targetStorage = null;

    public function testNoMatchesInSourceModels(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sourceModels = new Collection([
            new SourceModel(['id' => 10, 'descr' => 'SM 10']),
            new SourceModel(['id' => 20, 'descr' => 'SM 20']),
            new SourceModel(['id' => 30, 'descr' => 'SM 30']),
            new SourceModel(['id' => 50, 'descr' => 'SM 50']),
        ]);
        $sut->getImportMapper()->getProvider()->shouldReceive('getByImporter')
            ->once()
            ->with(SingleImporter::class)
            ->andReturn(new Collection());
        $sut->getImportMapper()->getProvider()->shouldReceive('getByImporterAndIdentifiers')
            ->twice()
            ->with(SingleImporter::class, IdentifierTypes::SOURCE, ['10', '20', '30', '50'])
            ->andReturn(new Collection());
        $this->getTargetProvider()->shouldReceive('getByTargetIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = [];
                $this->assertEquals($expected, array_keys($identifiers));

                return count($identifiers) === count($expected);
            }))
            ->andReturn(new Collection());
        $sut->init();
        $modelled = $sut->fromSourceModels($sourceModels);
        $this->assertCount(4, $modelled->getDataModels());
        $this->assertCount(0, $modelled->getTargetModels());
        $this->assertEquals('SM 10', $modelled->getDataModels()['10']->name);
        $this->assertCount(0, $modelled->getValidationFailures());
    }

    public function testWithMatchesInImportMapperAndTarget(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sourceModels = new Collection([
            new SourceModel(['id' => 10, 'descr' => 'SM 10']),
            new SourceModel(['id' => 20, 'descr' => 'SM 20']),
            new SourceModel(['id' => 30, 'descr' => 'SM 30']),
            new SourceModel(['id' => 50, 'descr' => 'SM 50']),
        ]);
        $mapped = new Collection([
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => SingleImporter::class, 'source_id' => '20', 'target_id' => 200,
            ]),
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => SingleImporter::class, 'source_id' => '50', 'target_id' => 500,
            ]),
        ]);
        $sut->getImportMapper()->getProvider()->shouldReceive('getByImporter')
            ->once()
            ->with(SingleImporter::class)
            ->andReturn(new Collection());
        $sut->getImportMapper()->getProvider()->shouldReceive('getByImporterAndIdentifiers')
            ->once()
            ->with(SingleImporter::class, IdentifierTypes::SOURCE, ['10', '20', '30', '50'])
            ->andReturn($mapped);
        $sut->getImportMapper()->getProvider()->shouldReceive('getByImporterAndIdentifiers')
            ->once()
            ->with(SingleImporter::class, IdentifierTypes::SOURCE, ['10', '30'])
            ->andReturn(new Collection());
        $targetModels = new Collection([
            (new TargetModel())->forceFill(['id' => '200', 'name' => 'SM 20']),
            (new TargetModel())->forceFill(['id' => '500', 'name' => 'SM 50']),
        ]);
        $this->getTargetProvider()->shouldReceive('getByTargetIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['200', '500'];
                $this->assertEquals($expected, array_keys($identifiers));

                return count($identifiers) === count($expected);
            }))
            ->andReturn($targetModels);
        $sut->init();
        $modelled = $sut->fromSourceModels($sourceModels);
        $this->assertCount(4, $modelled->getDataModels());
        $this->assertCount(2, $modelled->getTargetModels());
        $this->assertEquals('SM 10', $modelled->getDataModels()['10']->name);
        $this->assertCount(0, $modelled->getValidationFailures());
    }

    public function testWithMatchesInImportMapperPreload(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sourceModels = new Collection([
            new SourceModel(['id' => 10, 'descr' => 'SM 10']),
            new SourceModel(['id' => 20, 'descr' => 'SM 20']),
            new SourceModel(['id' => 30, 'descr' => 'SM 30']),
            new SourceModel(['id' => 50, 'descr' => 'SM 50']),
        ]);
        $mapped = new Collection([
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => SingleImporter::class, 'source_id' => '20', 'target_id' => 200,
            ]),
            new \Smorken\Import\Models\Eloquent\ImportMap([
                'importer' => SingleImporter::class, 'source_id' => '50', 'target_id' => 500,
            ]),
        ]);
        $sut->getImportMapper()->getProvider()->shouldReceive('getByImporter')
            ->once()
            ->with(SingleImporter::class)
            ->andReturn($mapped);
        $sut->getImportMapper()->getProvider()->shouldReceive('getByImporterAndIdentifiers')
            ->twice()
            ->with(SingleImporter::class, IdentifierTypes::SOURCE, ['10', '30'])
            ->andReturn(new Collection());
        $targetModels = new Collection([
            (new TargetModel())->forceFill(['id' => '200', 'name' => 'SM 20']),
            (new TargetModel())->forceFill(['id' => '500', 'name' => 'SM 50']),
        ]);
        $this->getTargetProvider()->shouldReceive('getByTargetIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['200', '500'];
                $this->assertEquals($expected, array_keys($identifiers));

                return count($identifiers) === count($expected);
            }))
            ->andReturn($targetModels);
        $sut->init();
        $modelled = $sut->fromSourceModels($sourceModels);
        $this->assertCount(4, $modelled->getDataModels());
        $this->assertCount(2, $modelled->getTargetModels());
        $this->assertEquals('SM 10', $modelled->getDataModels()['10']->name);
        $this->assertCount(0, $modelled->getValidationFailures());
    }

    protected function getPartsFactory(array $overrides = []): PartsFactory
    {
        $defaults = [
            PartsFactoryParts::SOURCE_PROVIDER => $this->getSourceProvider(),
            PartsFactoryParts::LOOKUPS => new Lookups(['default' => new MapId()]),
            PartsFactoryParts::RESULTS => new Results(),
            PartsFactoryParts::DATA_MODEL => new DataModel(),
            PartsFactoryParts::TARGET_PROVIDER => $this->getTargetProvider(),
        ];

        return new \Smorken\Import\PartsFactory(array_merge($defaults, $overrides));
    }

    protected function getSourceProvider(): SourceStorage
    {
        if (is_null($this->sourceStorage)) {
            $this->sourceStorage = m::mock(SourceStorage::class);
            $this->sourceStorage->shouldReceive('getSource')
                ->andReturn(new SourceModel());
        }

        return $this->sourceStorage;
    }

    protected function getSut(PartsFactory $partsFactory): Modeller
    {
        App::shouldReceive('make')
            ->with(ImportMap::class)
            ->andReturn(m::mock(ImportMap::class));
        $m = new Modeller();
        $partsFactory->set(PartsFactoryParts::DATA_TARGET_MODELLER, $m);
        $m->setFactory($partsFactory);

        return $m;
    }

    protected function getTargetProvider(): TargetStorage
    {
        if (is_null($this->targetStorage)) {
            $this->targetStorage = m::mock(TargetStorage::class);
            $this->targetStorage->shouldReceive('getTarget')
                ->andReturn(new TargetModel());
        }

        return $this->targetStorage;
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
