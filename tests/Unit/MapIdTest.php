<?php

namespace Tests\Smorken\Import\Unit;

use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;
use Smorken\Import\Contracts\Enums\MapIdTypes;
use Tests\Smorken\Import\Unit\Stubs\MapId;
use Tests\Smorken\Import\Unit\Stubs\SingleColDataModel;
use Tests\Smorken\Import\Unit\Stubs\SingleColTargetModel;

class MapIdTest extends TestCase
{
    public function testFromDataCollection(): void
    {
        $coll = new Collection([
            (new SingleColDataModel())->fromModel(['SC_ID' => 'L1', 'SC_NAME' => 'Location 1']),
            (new SingleColDataModel())->fromModel(['SC_ID' => 'L2', 'SC_NAME' => 'Location 2']),
            (new SingleColDataModel())->fromModel(['SC_ID' => 'L3', 'SC_NAME' => 'Location 3']),
        ]);
        $sut = new MapId();
        $sut->fromDataCollection($coll);
        $this->assertEquals(['L1', 'L2', 'L3'], $sut->getIdArray(MapIdTypes::SOURCE));
        $this->assertEquals([], $sut->getIdArray(MapIdTypes::TARGET));
    }

    public function testFromTargetCollection(): void
    {
        $coll = new Collection([
            new SingleColTargetModel(['id' => 1, 'external_id' => 'D1']),
            new SingleColTargetModel(['id' => 2, 'external_id' => 'D2']),
            new SingleColTargetModel(['id' => 3, 'external_id' => 'D3']),
        ]);
        $sut = new MapId();
        $sut->fromTargetCollection($coll);
        $this->assertEquals(['D1', 'D2', 'D3'], $sut->getIdArray(MapIdTypes::SOURCE));
        $this->assertEquals([1, 2, 3], $sut->getIdArray(MapIdTypes::TARGET));
    }

    public function testGetIdArrayForSourceIds(): void
    {
        $set = [
            '1111' => 1,
            '2222' => 2,
            '3333' => 3,
            '4444' => 4,
        ];
        $sut = new MapId($set);
        $this->assertEquals(array_keys($set), $sut->getIdArray(MapIdTypes::SOURCE));
    }

    public function testGetIdArrayForTargetIds(): void
    {
        $set = [
            '1111' => 1,
            '2222' => 2,
            '3333' => 3,
            '4444' => 4,
            '5555' => null,
        ];
        $sut = new MapId($set);
        $this->assertEquals([1, 2, 3, 4], $sut->getIdArray(MapIdTypes::TARGET));
    }

    public function testGetSourceIdentifier(): void
    {
        $set = [
            '1111' => 1,
            '2222' => 2,
            '3333' => 3,
            '4444' => 4,
        ];
        $sut = new MapId($set);
        $this->assertEquals('3333', $sut->getSourceIdentifier('3'));
        $this->assertEquals('3333', $sut->getSourceIdentifier(3));
        $this->assertNull($sut->getSourceIdentifier(5));
    }

    public function testIsEmptyIsFalseWhenNotEmpty(): void
    {
        $set = [
            '1111' => 1,
            '2222' => 2,
            '3333' => 3,
            '4444' => 4,
        ];
        $sut = new MapId($set);
        $this->assertFalse($sut->isEmpty());
    }

    public function testIsEmptyIsTrueWhenEmpty(): void
    {
        $sut = new MapId();
        $this->assertTrue($sut->isEmpty());
    }

    public function testIteratorYieldsItems(): void
    {
        $set = [
            '1111' => 1,
            '2222' => 2,
            '3333' => 3,
            '4444' => 4,
        ];
        $sut = new MapId($set);
        foreach ($sut as $key => $value) {
            $this->assertEquals($value, $set[$key]);
        }
    }

    public function testNewInstanceIsDifferentObject(): void
    {
        $sut = new MapId();
        $this->assertNotSame($sut, $sut->newInstance());
    }

    public function testSetNullId(): void
    {
        $sut = new MapId();
        $sut->set('1111', null);
        $this->assertNull($sut['1111']);
        $this->assertFalse($sut->has('1111'));
    }

    public function testSetNullIdHasCanBeTrueWhenIgnoreNullIsFalse(): void
    {
        $sut = new MapId();
        $sut->set('1111', null);
        $this->assertNull($sut['1111']);
        $this->assertTrue($sut->has('1111', false));
    }

    public function testSetWithNullTargetIdDoesNotOverwriteExistingTarget(): void
    {
        $sut = new MapId();
        $sut->set('abc', '123');
        $sut->set('abc', null);
        $this->assertEquals('123', $sut->get('abc'));
    }

    public function testSetWithNullTargetIdDoesOverwrite(): void
    {
        $sut = new MapId();
        $sut->set('abc', null);
        $sut->set('abc', '444');
        $this->assertEquals('444', $sut->get('abc'));
    }

    public function testSetWithTargetIdDoesOverwriteExistingTarget(): void
    {
        $sut = new MapId();
        $sut->set('abc', '123');
        $sut->set('abc', '456');
        $this->assertEquals('456', $sut->get('abc'));
    }
}
