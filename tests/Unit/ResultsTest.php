<?php

namespace Tests\Smorken\Import\Unit;

use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use Smorken\Import\Results;

class ResultsTest extends TestCase
{
    public function testAddMessageWithBaseAndNotPassedAsKey(): void
    {
        $sut = new Results('Base');
        $sut->addMessage('foo', 'Good day');
        $expected = [
            [
                'Base::foo' => 'Good day',
            ],
        ];
        $this->assertEquals($expected, $sut->getMessages());
    }

    public function testAddMessageWithBaseAndPassedAsKey(): void
    {
        $sut = new Results('Base');
        $sut->addMessage('Something::foo', 'Good day');
        $expected = [
            [
                'Something::foo' => 'Good day',
            ],
        ];
        $this->assertEquals($expected, $sut->getMessages());
    }

    public function testAddMessageWithoutBaseAndNotPassedAsKey(): void
    {
        $sut = new Results();
        $sut->addMessage('foo', 'Good day');
        $expected = [
            [
                'foo' => 'Good day',
            ],
        ];
        $this->assertEquals($expected, $sut->getMessages());
    }

    public function testDecrementWithBaseAndCount(): void
    {
        $sut = new Results('Base');
        $sut->setCounter('counter', 50);
        $sut->decrement('counter', 10);
        $this->assertEquals(40, $sut->getCounter('counter'));
    }

    public function testDecrementWithBaseAndNoCount(): void
    {
        $sut = new Results('Base');
        $sut->decrement('counter');
        $this->assertEquals(-1, $sut->getCounter('counter'));
    }

    public function testDecrementWithoutBaseAndNoCount(): void
    {
        $sut = new Results();
        $sut->decrement('counter');
        $this->assertEquals(-1, $sut->getCounter('counter'));
    }

    public function testIncrementWithBaseAndCount(): void
    {
        $sut = new Results('Base');
        $sut->setCounter('counter', 50);
        $sut->increment('counter', 10);
        $this->assertEquals(60, $sut->getCounter('counter'));
    }

    public function testIncrementWithBaseAndNoCount(): void
    {
        $sut = new Results('Base');
        $sut->increment('counter');
        $this->assertEquals(1, $sut->getCounter('counter'));
    }

    public function testIncrementWithoutBaseAndNoCount(): void
    {
        $sut = new Results();
        $sut->increment('counter');
        $this->assertEquals(1, $sut->getCounter('counter'));
    }

    public function testInitWithBase(): void
    {
        $sut = new Results('Base');
        $sut->init(['counter1', 'counter2']);
        $expected = [
            'Base::counter1' => 0,
            'Base::counter2' => 0,
            'messages' => [],
        ];
        $actual = $sut->toArray();
        foreach ($expected as $k => $v) {
            $this->assertEquals($v, $actual[$k]);
        }
        $this->assertInstanceOf(Carbon::class, $actual['start']);
        $this->assertInstanceOf(Carbon::class, $actual['end']);
        $this->assertGreaterThan(0.0, $actual['memory']);
    }

    public function testInitWithoutBase(): void
    {
        $sut = new Results();
        $sut->init(['counter1', 'counter2']);
        $expected = [
            'counter1' => 0,
            'counter2' => 0,
            'messages' => [],
        ];
        $actual = $sut->toArray();
        foreach ($expected as $k => $v) {
            $this->assertEquals($v, $actual[$k]);
        }
        $this->assertInstanceOf(Carbon::class, $actual['start']);
        $this->assertInstanceOf(Carbon::class, $actual['end']);
        $this->assertGreaterThan(0.0, $actual['memory']);
    }

    public function testStopStopsOnce(): void
    {
        $sut = new Results();
        $sut->init();
        $first = $sut->toArray();
        $sut->stop();
        $second = $sut->toArray();
        $this->assertEquals($first, $second);
    }
}
