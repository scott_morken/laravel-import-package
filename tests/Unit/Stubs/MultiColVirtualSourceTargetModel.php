<?php

namespace Tests\Smorken\Import\Unit\Stubs;

use Smorken\Import\Models\Target\Eloquent;

class MultiColVirtualSourceTargetModel extends Eloquent
{
    protected $fillable = ['id_1', 'id_2', 'name'];

    protected bool $sourceIdentifierIsVirtual = true;

    protected array $sourceIdentifierAttributes = ['external_id', 'another_id'];

    protected array $targetIdentifierAttributes = ['id_1', 'id_2'];
}
