<?php

namespace Tests\Smorken\Import\Unit\Stubs;

use Smorken\Import\Models\Source\Eloquent;

class MultiColSourceModel extends Eloquent
{
    protected $fillable = ['SC_ID', 'SC_ANOTHER', 'SC_NAME'];
}
