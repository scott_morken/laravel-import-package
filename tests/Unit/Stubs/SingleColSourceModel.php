<?php

namespace Tests\Smorken\Import\Unit\Stubs;

use Smorken\Import\Models\Source\Eloquent;

class SingleColSourceModel extends Eloquent
{
    protected $fillable = ['SC_ID', 'SC_NAME'];
}
