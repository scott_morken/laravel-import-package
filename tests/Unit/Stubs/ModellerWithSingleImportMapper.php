<?php

namespace Tests\Smorken\Import\Unit\Stubs;

use Smorken\Import\Modellers\ModellerWithImportMapper;

class ModellerWithSingleImportMapper extends ModellerWithImportMapper
{
    protected ?string $importMapperKey = StubImporter::class;
}
