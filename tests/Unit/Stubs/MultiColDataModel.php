<?php

namespace Tests\Smorken\Import\Unit\Stubs;

use Smorken\Import\Models\Data;

class MultiColDataModel extends Data
{
    protected array $sourceIdentifierAttributes = ['external_id', 'another_id'];

    public function conversions(): array
    {
        return [
            'default' => [
                'external_id' => 'SC_ID',
                'another_id' => 'SC_ANOTHER',
                'name' => 'SC_NAME',
            ],
        ];
    }

    public function isValid(): bool
    {
        return parent::isValid() && $this->validateNameFails();
    }

    protected function validateNameFails(): bool
    {
        return $this->name !== 'fail';
    }
}
