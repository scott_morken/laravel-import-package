<?php

namespace Tests\Smorken\Import\Unit\Stubs;

use Smorken\Import\Models\Target\Eloquent;

class SingleColVirtualSourceTargetModel extends Eloquent
{
    protected bool $sourceIdentifierIsVirtual = true;

    protected $fillable = ['id', 'name'];
}
