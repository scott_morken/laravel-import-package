<?php

namespace Tests\Smorken\Import\Unit\Stubs;

use Illuminate\Support\Collection;
use Smorken\Import\DataAndTargetModeller;

class StubModeller extends DataAndTargetModeller
{
    protected function afterCreateDataModels(Collection $dataModels): void
    {
        $dataModels->first()->name = $dataModels->first()->name.' after';
    }

    protected function afterCreateTargetModels(Collection $targetModels): void
    {
        if ($targetModels->count()) {
            $targetModels->first()->name = $targetModels->first()->name.' AFTER';
        }
    }

    protected function beforeCreateDataModels(Collection $sourceModels): void
    {
        $sourceModels->first()->SC_NAME = 'before';
    }
}
