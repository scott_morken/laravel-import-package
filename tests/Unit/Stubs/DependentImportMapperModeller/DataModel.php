<?php

namespace Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller;

use Smorken\Import\Models\Data;

class DataModel extends Data
{
    protected array $sourceIdentifierAttributes = ['reviewer_id', 'student_id', 'progress_report_id'];

    protected array $targetIdentifierAttributes = ['reviewer_id', 'student_id', 'request_id'];

    public function conversions(): array
    {
        return [
            'default' => [
                'reviewer_id' => 'reviewer_id',
                'student_id' => 'student_id',
                'progress_report_id' => 'progress_report_id',
                'request_id' => 'request_id',
                'comments' => 'comments',
            ],
        ];
    }
}
