<?php

namespace Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller;

use Smorken\Import\Contracts\Enums\IdentifierTypes;
use Smorken\Import\Modellers\ModellerWithImportMapper;

class Modeller extends ModellerWithImportMapper
{
    protected ?string $importMapperKey = DependentImporter::class;

    protected array $importMapperLookups = [
        DependencyImporter::class => [
            IdentifierTypes::SOURCE => 'progress_report_id',
            IdentifierTypes::TARGET => 'request_id',
        ],
    ];
}
