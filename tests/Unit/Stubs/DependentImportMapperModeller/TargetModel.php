<?php

namespace Tests\Smorken\Import\Unit\Stubs\DependentImportMapperModeller;

use Smorken\Import\Models\Target\Eloquent;

class TargetModel extends Eloquent
{
    protected $fillable = ['reviewer_id', 'student_id', 'request_id', 'comments'];

    protected array $sourceIdentifierAttributes = ['reviewer_id', 'student_id', 'progress_report_id'];

    protected bool $sourceIdentifierIsVirtual = true;

    protected array $targetIdentifierAttributes = ['reviewer_id', 'student_id', 'request_id'];
}
