<?php

namespace Tests\Smorken\Import\Unit\Stubs;

use Smorken\Import\Models\Target\Eloquent;

class SingleColTargetModel extends Eloquent
{
    protected $fillable = ['id', 'external_id', 'name'];
}
