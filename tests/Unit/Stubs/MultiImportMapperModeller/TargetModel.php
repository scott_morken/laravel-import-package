<?php

namespace Tests\Smorken\Import\Unit\Stubs\MultiImportMapperModeller;

use Smorken\Import\Models\Target\Eloquent;

class TargetModel extends Eloquent
{
    protected array $sourceIdentifierAttributes = ['source_id'];

    protected array $targetIdentifierAttributes = ['id'];

    protected $fillable = ['name', 'other_id'];
}
