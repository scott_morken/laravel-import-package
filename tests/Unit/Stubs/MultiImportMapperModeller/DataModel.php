<?php

namespace Tests\Smorken\Import\Unit\Stubs\MultiImportMapperModeller;

use Smorken\Import\Models\Data;

class DataModel extends Data
{
    protected array $sourceIdentifierAttributes = ['source_id'];

    protected array $targetIdentifierAttributes = ['target_id'];

    public function conversions(): array
    {
        return [
            'default' => [
                'source_id' => 'id',
                'other_id' => 'some_other_id',
                'name' => 'descr',
            ],
        ];
    }
}
