<?php

namespace Tests\Smorken\Import\Unit\Stubs\MultiImportMapperModeller;

use Smorken\Import\Contracts\Enums\IdentifierTypes;
use Smorken\Import\Modellers\ModellerWithImportMapper;

class Modeller extends ModellerWithImportMapper
{
    protected ?string $importMapperKey = MultiImporter::class;

    protected array $importMapperLookups = [
        OtherImporter::class => [
            IdentifierTypes::SOURCE => 'some_other_id',
            IdentifierTypes::TARGET => 'other_id',
        ],
    ];
}
