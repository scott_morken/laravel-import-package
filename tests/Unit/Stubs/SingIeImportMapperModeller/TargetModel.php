<?php

namespace Tests\Smorken\Import\Unit\Stubs\SingIeImportMapperModeller;

use Smorken\Import\Models\Target\Eloquent;

class TargetModel extends Eloquent
{
    protected array $sourceIdentifierAttributes = ['source_id'];

    protected array $targetIdentifierAttributes = ['id'];

    protected $fillable = ['name'];
}
