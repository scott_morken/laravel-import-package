<?php

namespace Tests\Smorken\Import\Unit\Stubs\SingIeImportMapperModeller;

use Smorken\Import\Contracts\Storage\HasTarget;
use Smorken\Import\Storage\Target\Eloquent;

class TargetStorage extends Eloquent implements HasTarget
{
}
