<?php

namespace Tests\Smorken\Import\Unit\Stubs\SingIeImportMapperModeller;

use Smorken\Import\Models\Data;

class DataModel extends Data
{
    protected array $sourceIdentifierAttributes = ['source_id'];

    protected array $targetIdentifierAttributes = ['target_id'];

    public function conversions(): array
    {
        return [
            'default' => [
                'source_id' => 'id',
                'name' => 'descr',
            ],
        ];
    }
}
