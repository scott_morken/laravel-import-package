<?php

namespace Tests\Smorken\Import\Unit\Stubs\SingIeImportMapperModeller;

use Smorken\Import\Contracts\Storage\HasSource;
use Smorken\Import\Storage\Source\Eloquent;

class SourceStorage extends Eloquent implements HasSource
{
}
