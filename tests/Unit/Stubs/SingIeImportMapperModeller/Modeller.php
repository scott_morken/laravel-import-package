<?php

namespace Tests\Smorken\Import\Unit\Stubs\SingIeImportMapperModeller;

use Smorken\Import\Modellers\ModellerWithImportMapper;

class Modeller extends ModellerWithImportMapper
{
    protected ?string $importMapperKey = SingleImporter::class;
}
