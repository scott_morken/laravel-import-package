<?php

namespace Tests\Smorken\Import\Unit\Stubs;

use Smorken\Import\Models\Data;

class SingleColDataModel extends Data
{
    protected array $sourceIdentifierAttributes = ['external_id'];

    public function conversions(): array
    {
        return [
            'default' => [
                'external_id' => 'SC_ID',
                'name' => 'SC_NAME',
            ],
        ];
    }
}
