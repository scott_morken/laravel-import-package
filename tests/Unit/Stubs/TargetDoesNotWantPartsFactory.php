<?php

namespace Tests\Smorken\Import\Unit\Stubs;

use Smorken\Import\Storage\Target\Eloquent;

class TargetDoesNotWantPartsFactory extends Eloquent
{
    protected string $factory = '';

    public function setFactory(string $foo): void
    {
        $this->factory = $foo;
    }

    public function getFactory(): string
    {
        return $this->factory;
    }

    public function wantsPartsFactory(): bool
    {
        return false;
    }
}
