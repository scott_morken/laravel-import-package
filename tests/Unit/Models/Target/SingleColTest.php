<?php

namespace Tests\Smorken\Import\Unit\Models\Target;

use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use Smorken\Import\Contracts\Enums\IdentifierTypes;
use Smorken\Import\Contracts\Enums\TargetUpdateTypes;
use Tests\Smorken\Import\Unit\Stubs\SingleColDataModel;
use Tests\Smorken\Import\Unit\Stubs\SingleColTargetModel;

class SingleColTest extends TestCase
{
    public function testFromDataModelGetSourceIdentifierIsSingleColumn(): void
    {
        $sut = new SingleColTargetModel();
        $dataModel = (new SingleColDataModel())->fromModel(['SC_ID' => 'foo', 'SC_NAME' => 'Bar']);
        $model = $sut->fromDataModel($dataModel);
        $this->assertEquals('foo', $model->getIdentifiers()->getSingleValue(IdentifierTypes::SOURCE));
    }

    public function testFromDataModelOnNew(): void
    {
        $sut = new SingleColTargetModel();
        $dataModel = (new SingleColDataModel())->fromModel(['SC_ID' => 'foo', 'SC_NAME' => 'Bar']);
        $model = $sut->fromDataModel($dataModel);
        $this->assertNotSame($model, $sut);
        $this->assertEquals(TargetUpdateTypes::UNDEFINED, $model->getUpdateType());
        $this->assertEquals('Bar', $model->name);
        $this->assertEquals([], $model->getUpdatedAttributes());
    }

    public function testFromDataModelOnNewWithAttributes(): void
    {
        $sut = new SingleColTargetModel();
        $dataModel = (new SingleColDataModel())->fromModel(['SC_ID' => 'foo', 'biz' => 'Bar']);
        $model = $sut->fromDataModel($dataModel, ['name' => 'biz']);
        $this->assertNotSame($model, $sut);
        $this->assertEquals(TargetUpdateTypes::UNDEFINED, $model->getUpdateType());
        $this->assertEquals('Bar', $model->name);
        $this->assertEquals([], $model->getUpdatedAttributes());
    }

    public function testFromDataModelOnUpdateIsFull(): void
    {
        $sut = new SingleColTargetModel();
        $sut->forceFill(['id' => 1, 'external_id' => 'foo', 'name' => 'Bar']);
        $dataModel = (new SingleColDataModel())->fromModel(['SC_ID' => 'foo', 'SC_NAME' => 'Bizz']);
        $model = $sut->fromDataModel($dataModel);
        $this->assertSame($model, $sut);
        $this->assertEquals(TargetUpdateTypes::FULL, $model->getUpdateType());
        $this->assertEquals('Bizz', $model->name);
        $this->assertEquals(['name' => 'Bizz'], $model->getUpdatedAttributes());
    }

    public function testFromDataModelOnUpdateIsFullWithAttributes(): void
    {
        $sut = new SingleColTargetModel();
        $sut->forceFill(['id' => 1, 'external_id' => 'foo', 'name' => 'Bar']);
        $dataModel = (new SingleColDataModel())->fromModel(['SC_ID' => 'foo', 'biz' => 'Bizz']);
        $model = $sut->fromDataModel($dataModel, ['name' => 'biz']);
        $this->assertSame($model, $sut);
        $this->assertEquals(TargetUpdateTypes::FULL, $model->getUpdateType());
        $this->assertEquals('Bizz', $model->name);
        $this->assertEquals(['name' => 'Bizz'], $model->getUpdatedAttributes());
    }

    public function testFromDataModelOnUpdateIsFullWithCarbonAsDateAttributes(): void
    {
        $sut = new SingleColTargetModel();
        $dateOne = Carbon::now()->subDay()->startOfDay();
        $date = Carbon::now()->startOfDay();
        $sut->forceFill(['id' => 1, 'external_id' => 'foo', 'name' => $dateOne->toDateString()]);
        $dataModel = (new SingleColDataModel())->fromModel(['SC_ID' => 'foo', 'biz' => $date]);
        $model = $sut->fromDataModel($dataModel, ['name' => 'biz']);
        $this->assertSame($model, $sut);
        $this->assertEquals(TargetUpdateTypes::FULL, $model->getUpdateType());
        $this->assertEquals($date, $model->name);
        $this->assertEquals(['name' => $date], $model->getUpdatedAttributes());
    }

    public function testFromDataModelOnUpdateIsFullWithCarbonAttributes(): void
    {
        $sut = new SingleColTargetModel();
        $dateOne = Carbon::now()->subSeconds(5);
        $date = Carbon::now();
        $sut->forceFill(['id' => 1, 'external_id' => 'foo', 'name' => $dateOne->toDateTimeString()]);
        $dataModel = (new SingleColDataModel())->fromModel(['SC_ID' => 'foo', 'biz' => $date]);
        $model = $sut->fromDataModel($dataModel, ['name' => 'biz']);
        $this->assertSame($model, $sut);
        $this->assertEquals(TargetUpdateTypes::FULL, $model->getUpdateType());
        $this->assertEquals($date->toDateTimeString(), $model->name);
        $this->assertEquals(['name' => $date->toDateTimeString()], $model->getUpdatedAttributes());
    }

    public function testFromDataModelOnUpdateIsTouch(): void
    {
        $sut = new SingleColTargetModel();
        $sut->forceFill(['id' => 1, 'external_id' => 'foo', 'name' => 'Bar']);
        $dataModel = (new SingleColDataModel())->fromModel(['SC_ID' => 'foo', 'SC_NAME' => 'Bar']);
        $model = $sut->fromDataModel($dataModel);
        $this->assertSame($model, $sut);
        $this->assertEquals(TargetUpdateTypes::TOUCH, $model->getUpdateType());
        $this->assertEquals('Bar', $model->name);
        $this->assertEquals([], $model->getUpdatedAttributes());
    }

    public function testFromDataModelOnUpdateIsTouchWithAttributes(): void
    {
        $sut = new SingleColTargetModel();
        $sut->forceFill(['id' => 1, 'external_id' => 'foo', 'name' => 'Bar']);
        $dataModel = (new SingleColDataModel())->fromModel(['SC_ID' => 'foo', 'biz' => 'Bar']);
        $model = $sut->fromDataModel($dataModel, ['name' => 'biz']);
        $this->assertSame($model, $sut);
        $this->assertEquals(TargetUpdateTypes::TOUCH, $model->getUpdateType());
        $this->assertEquals('Bar', $model->name);
        $this->assertEquals([], $model->getUpdatedAttributes());
    }

    public function testFromDataModelOnUpdateIsTouchWithCarbonAsDateAttributes(): void
    {
        $sut = new SingleColTargetModel();
        $date = Carbon::now()->startOfDay();
        $sut->forceFill(['id' => 1, 'external_id' => 'foo', 'name' => $date->toDateString()]);
        $dataModel = (new SingleColDataModel())->fromModel(['SC_ID' => 'foo', 'biz' => $date]);
        $model = $sut->fromDataModel($dataModel, ['name' => 'biz']);
        $this->assertSame($model, $sut);
        $this->assertEquals(TargetUpdateTypes::TOUCH, $model->getUpdateType());
        $this->assertEquals($date->toDateString(), $model->name);
        $this->assertEquals([], $model->getUpdatedAttributes());
    }

    public function testFromDataModelOnUpdateIsTouchWithCarbonAttributes(): void
    {
        $sut = new SingleColTargetModel();
        $date = Carbon::now();
        $sut->forceFill(['id' => 1, 'external_id' => 'foo', 'name' => $date->toDateTimeString()]);
        $dataModel = (new SingleColDataModel())->fromModel(['SC_ID' => 'foo', 'biz' => $date]);
        $model = $sut->fromDataModel($dataModel, ['name' => 'biz']);
        $this->assertSame($model, $sut);
        $this->assertEquals(TargetUpdateTypes::TOUCH, $model->getUpdateType());
        $this->assertEquals($date->toDateTimeString(), $model->name);
        $this->assertEquals([], $model->getUpdatedAttributes());
    }

    public function testFromDataModelOnUpdateWithNewInstanceIsUndefined(): void
    {
        $sut = new SingleColTargetModel();
        $sut->forceFill(['id' => 1, 'external_id' => 'foo', 'name' => 'Bar']);
        $this->assertEquals(TargetUpdateTypes::UNDEFINED, $sut->getUpdateType());
    }
}
