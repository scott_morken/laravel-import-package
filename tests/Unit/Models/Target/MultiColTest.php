<?php

namespace Tests\Smorken\Import\Unit\Models\Target;

use PHPUnit\Framework\TestCase;
use Smorken\Import\Contracts\Enums\TargetUpdateTypes;
use Tests\Smorken\Import\Unit\Stubs\MultiColDataModel;
use Tests\Smorken\Import\Unit\Stubs\MultiColTargetModel;
use Tests\Smorken\Import\Unit\Stubs\MultiColVirtualSourceTargetModel;

class MultiColTest extends TestCase
{
    public function testFromDataModelGetSourceIdentifierIsMultiColumn(): void
    {
        $sut = new MultiColTargetModel();
        $dataModel = (new MultiColDataModel())->fromModel(['SC_ID' => 'foo', 'SC_ANOTHER' => 'baz', 'SC_NAME' => 'Bar']);
        $model = $sut->fromDataModel($dataModel);
        $this->assertEquals([
            'external_id' => 'foo',
            'another_id' => 'baz',
        ], $model->getIdentifiers()->getSourceIdentifiers());
    }

    public function testFromDataModelOnNew(): void
    {
        $sut = new MultiColTargetModel();
        $dataModel = (new MultiColDataModel())->fromModel(['SC_ID' => 'foo', 'SC_ANOTHER' => 'baz', 'SC_NAME' => 'Bar']);
        $model = $sut->fromDataModel($dataModel);
        $this->assertNotSame($model, $sut);
        $this->assertEquals(TargetUpdateTypes::UNDEFINED, $model->getUpdateType());
        $this->assertEquals('Bar', $model->name);
        $this->assertEquals([], $model->getUpdatedAttributes());
        $this->assertEquals(['external_id' => 'foo', 'another_id' => 'baz'],
            $model->getIdentifiers()->getSourceIdentifiers());
    }

    public function testFromDataModelOnNewWithAttributes(): void
    {
        $sut = new MultiColTargetModel();
        $dataModel = (new MultiColDataModel())->fromModel(['SC_ID' => 'foo', 'SC_ANOTHER' => 'baz', 'biz' => 'Bar']);
        $model = $sut->fromDataModel($dataModel, ['name' => 'biz']);
        $this->assertNotSame($model, $sut);
        $this->assertEquals(TargetUpdateTypes::UNDEFINED, $model->getUpdateType());
        $this->assertEquals('Bar', $model->name);
        $this->assertEquals([], $model->getUpdatedAttributes());
    }

    public function testFromDataModelOnNewWithVirtualSource(): void
    {
        $sut = new MultiColVirtualSourceTargetModel();
        $dataModel = (new MultiColDataModel())->fromModel(['SC_ID' => 'foo', 'SC_ANOTHER' => 'baz', 'SC_NAME' => 'Bar']);
        $model = $sut->fromDataModel($dataModel);
        $this->assertNotSame($model, $sut);
        $this->assertEquals(TargetUpdateTypes::UNDEFINED, $model->getUpdateType());
        $this->assertEquals('Bar', $model->name);
        $this->assertEquals([], $model->getUpdatedAttributes());
        $this->assertEquals(['external_id' => 'foo', 'another_id' => 'baz'],
            $model->getIdentifiers()->getSourceIdentifiers());
    }

    public function testFromDataModelOnUpdateIsFull(): void
    {
        $sut = new MultiColTargetModel();
        $sut->forceFill(['id' => 1, 'external_id' => 'foo', 'another_id' => 'baz', 'name' => 'Bar']);
        $dataModel = (new MultiColDataModel())->fromModel(['SC_ID' => 'foo', 'SC_ANOTHER' => 'baz', 'SC_NAME' => 'Bizz']);
        $model = $sut->fromDataModel($dataModel);
        $this->assertSame($model, $sut);
        $this->assertEquals(TargetUpdateTypes::FULL, $model->getUpdateType());
        $this->assertEquals('Bizz', $model->name);
        $this->assertEquals(['name' => 'Bizz'], $model->getUpdatedAttributes());
    }

    public function testFromDataModelOnUpdateIsFullVirtualSource(): void
    {
        $sut = new MultiColVirtualSourceTargetModel();
        $sut->forceFill(['id' => 1, 'name' => 'Bar']);
        $dataModel = (new MultiColDataModel())->fromModel(['SC_ID' => 'foo', 'SC_ANOTHER' => 'baz', 'SC_NAME' => 'Bizz']);
        $model = $sut->fromDataModel($dataModel);
        $this->assertSame($model, $sut);
        $this->assertEquals(TargetUpdateTypes::FULL, $model->getUpdateType());
        $this->assertEquals('Bizz', $model->name);
        $this->assertEquals(['name' => 'Bizz'], $model->getUpdatedAttributes());
        $this->assertEquals(['external_id' => 'foo', 'another_id' => 'baz'],
            $model->getIdentifiers()->getSourceIdentifiers());
    }

    public function testFromDataModelOnUpdateIsFullWithAttributes(): void
    {
        $sut = new MultiColTargetModel();
        $sut->forceFill(['id' => 1, 'external_id' => 'foo', 'another_id' => 'baz', 'name' => 'Bar']);
        $dataModel = (new MultiColDataModel())->fromModel(['SC_ID' => 'foo', 'SC_ANOTHER' => 'baz', 'biz' => 'Bizz']);
        $model = $sut->fromDataModel($dataModel, ['name' => 'biz']);
        $this->assertSame($model, $sut);
        $this->assertEquals(TargetUpdateTypes::FULL, $model->getUpdateType());
        $this->assertEquals('Bizz', $model->name);
        $this->assertEquals(['name' => 'Bizz'], $model->getUpdatedAttributes());
    }

    public function testFromDataModelOnUpdateIsTouch(): void
    {
        $sut = new MultiColTargetModel();
        $sut->forceFill(['id' => 1, 'external_id' => 'foo', 'another_id' => 'baz', 'name' => 'Bar']);
        $dataModel = (new MultiColDataModel())->fromModel(['SC_ID' => 'foo', 'SC_ANOTHER' => 'baz', 'SC_NAME' => 'Bar']);
        $model = $sut->fromDataModel($dataModel);
        $this->assertSame($model, $sut);
        $this->assertEquals(TargetUpdateTypes::TOUCH, $model->getUpdateType());
        $this->assertEquals('Bar', $model->name);
        $this->assertEquals([], $model->getUpdatedAttributes());
    }

    public function testFromDataModelOnUpdateIsTouchVirtualSource(): void
    {
        $sut = new MultiColVirtualSourceTargetModel();
        $sut->forceFill(['id' => 1, 'name' => 'Bar']);
        $dataModel = (new MultiColDataModel())->fromModel(['SC_ID' => 'foo', 'SC_ANOTHER' => 'baz', 'SC_NAME' => 'Bar']);
        $model = $sut->fromDataModel($dataModel);
        $this->assertSame($model, $sut);
        $this->assertEquals(TargetUpdateTypes::TOUCH, $model->getUpdateType());
        $this->assertEquals('Bar', $model->name);
        $this->assertEquals([], $model->getUpdatedAttributes());
        $this->assertEquals(['external_id' => 'foo', 'another_id' => 'baz'],
            $model->getIdentifiers()->getSourceIdentifiers());
    }

    public function testFromDataModelOnUpdateIsTouchWithAttributes(): void
    {
        $sut = new MultiColTargetModel();
        $sut->forceFill(['id' => 1, 'external_id' => 'foo', 'another_id' => 'baz', 'name' => 'Bar']);
        $dataModel = (new MultiColDataModel())->fromModel(['SC_ID' => 'foo', 'SC_ANOTHER' => 'baz', 'biz' => 'Bar']);
        $model = $sut->fromDataModel($dataModel, ['name' => 'biz']);
        $this->assertSame($model, $sut);
        $this->assertEquals(TargetUpdateTypes::TOUCH, $model->getUpdateType());
        $this->assertEquals('Bar', $model->name);
        $this->assertEquals([], $model->getUpdatedAttributes());
    }

    public function testFromDataModelOnUpdateWithNewInstanceIsUndefined(): void
    {
        $sut = new MultiColTargetModel();
        $sut->forceFill(['id' => 1, 'external_id' => 'foo', 'another_id' => 'baz', 'name' => 'Bar']);
        $this->assertEquals(TargetUpdateTypes::UNDEFINED, $sut->getUpdateType());
    }
}
