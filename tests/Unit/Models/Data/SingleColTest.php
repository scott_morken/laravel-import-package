<?php

namespace Tests\Smorken\Import\Unit\Models\Data;

use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;
use Smorken\Import\Contracts\Enums\IdentifierTypes;
use Tests\Smorken\Import\Unit\Stubs\SingleColDataModel;
use Tests\Smorken\Import\Unit\Stubs\SingleColSourceModel;

class SingleColTest extends TestCase
{
    public function testFromCollection(): void
    {
        $sources = new Collection([
            new SingleColSourceModel(['SC_ID' => 'ID1', 'SC_NAME' => 'Foo 1']),
            new SingleColSourceModel(['SC_ID' => 'ID2', 'SC_NAME' => 'Foo 2']),
        ]);
        $suts = (new SingleColDataModel())->fromCollection($sources);
        $this->assertCount(2, $suts);
        $this->assertEquals([
            'external_id' => 'ID1',
            'name' => 'Foo 1',
        ], $suts[0]->getAttributes());
        $this->assertEquals([
            'external_id' => 'ID2',
            'name' => 'Foo 2',
        ], $suts[1]->getAttributes());
    }

    public function testFromModel(): void
    {
        $source = new SingleColSourceModel(['SC_ID' => 'ID1', 'SC_NAME' => 'Foo 1']);
        $sut = (new SingleColDataModel())->fromModel($source);
        $this->assertEquals([
            'external_id' => 'ID1',
            'name' => 'Foo 1',
        ], $sut->getAttributes());
    }

    public function testGetSourceIdentifierUsesConvertedAttribute(): void
    {
        $sut = (new SingleColDataModel())->fromModel([
            'SC_ID' => '1234',
            'SC_NAME' => 'Foo',
        ]);
        $this->assertEquals('1234', $sut->getIdentifiers()->getSingleValue(IdentifierTypes::SOURCE));
    }

    public function testGetTargetIdentifierWhenEmpty(): void
    {
        $sut = (new SingleColDataModel())->fromModel([
            'SC_ID' => '1234',
            'SC_NAME' => 'Foo',
        ]);
        $this->assertEquals(['target_id' => null], $sut->getIdentifiers()->get(IdentifierTypes::TARGET));
    }

    public function testModelWithName(): void
    {
        $sut = (new SingleColDataModel())->fromModel([
            'SC_ID' => '1234',
            'SC_NAME' => 'Foo',
        ]);
        $this->assertEquals('Foo', $sut->name);
    }
}
