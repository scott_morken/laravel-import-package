<?php

namespace Tests\Smorken\Import\Unit\Models\Data;

use PHPUnit\Framework\TestCase;
use Tests\Smorken\Import\Unit\Stubs\MultiColDataModel;

class MultiColTest extends TestCase
{
    public function testGetSourceIdentifiersUsesConvertedAttribute(): void
    {
        $sut = (new MultiColDataModel())->fromModel([
            'SC_ID' => '1234',
            'SC_ANOTHER' => 'AAAA',
            'SC_NAME' => 'Foo',
        ]);
        $this->assertEquals([
            'external_id' => '1234',
            'another_id' => 'AAAA',
        ], $sut->getIdentifiers()->getSourceIdentifiers());
    }

    public function testGetTargetIdentifierWhenEmpty(): void
    {
        $sut = (new MultiColDataModel())->fromModel([
            'SC_ID' => '1234',
            'SC_ANOTHER' => 'AAAA',
            'SC_NAME' => 'Foo',
        ]);
        $this->assertEquals(['target_id' => null], $sut->getIdentifiers()->getTargetIdentifiers());
    }
}
