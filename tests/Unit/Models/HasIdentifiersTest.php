<?php

namespace Tests\Smorken\Import\Unit\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Import\Identifiers;
use Smorken\Import\Models\Traits\HasIdentifiers;
use Tests\Smorken\Import\Unit\Stubs\MultiColTargetModel;
use Tests\Smorken\Import\Unit\Stubs\MultiColVirtualSourceTargetModel;
use Tests\Smorken\Import\Unit\Stubs\SingleColTargetModel;
use Tests\Smorken\Import\Unit\Stubs\SingleColVirtualSourceTargetModel;

class HasIdentifiersTest extends TestCase
{
    use HasIdentifiers;

    protected bool $sourceIdentifierIsVirtual = false;

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testScopeSourceIdentifierInMultiColumn(): void
    {
        $externalIds = [
            new Identifiers(new MultiColTargetModel(['external_id' => 'foo-1', 'another_id' => 'bar-2']),
                ['external_id', 'another_id'], []),
            new Identifiers(new MultiColTargetModel(['external_id' => 'foo-2', 'another_id' => 'bar-3']),
                ['external_id', 'another_id'], []),
        ];
        $query = $this->getMockQueryBuilder();
        $nestedQuery = m::mock(Builder::class);
        $nestedQuery->shouldReceive('getQuery')->once()->andReturn($query);
        $subNestedQuery = m::mock(Builder::class);
        $model = m::mock(Model::class);
        $model->shouldReceive('getTable')->andReturn('foo_table');
        $model->shouldReceive('newQueryWithoutRelationships')->once()->andReturn($nestedQuery);
        $builder = new Builder($query);
        $builder->setModel($model);
        $key = 0;
        $nestedQuery->shouldReceive('orWhere')->twice()->with(m::on(function ($callback) use (
            $subNestedQuery,
            $externalIds,
            &$key
        ) {
            $callback($subNestedQuery, $externalIds[$key]);
            $key++;

            return true;
        }));
        $subNestedQuery->shouldReceive('where')->once()->with('external_id', 'foo-1');
        $subNestedQuery->shouldReceive('where')->once()->with('another_id', 'bar-2');
        $subNestedQuery->shouldReceive('where')->once()->with('external_id', 'foo-2');
        $subNestedQuery->shouldReceive('where')->once()->with('another_id', 'bar-3');
        $builder->getQuery()->shouldReceive('addNestedWhereQuery')->once()->with($query, 'and');
        $sut = new MultiColTargetModel();
        $b = $sut->scopeSourceIdentifierIn($builder, $externalIds);
        $this->assertSame($b, $builder);
    }

    public function testScopeSourceIdentifierInSingleColumn(): void
    {
        $identifier = new Identifiers(new SingleColTargetModel(['external_id' => 'foo-1']), ['external_id'], []);
        $builder = new Builder($this->getMockQueryBuilder());
        $builder->getQuery()->shouldReceive('whereIn')->once()->with('external_id', ['foo-1']);
        $sut = new SingleColTargetModel();
        $b = $sut->scopeSourceIdentifierIn($builder, [$identifier]);
        $this->assertSame($b, $builder);
    }

    public function testScopeSourceIdentifierIsMultiColumn(): void
    {
        $identifier = new Identifiers(new MultiColTargetModel(['external_id' => 'foo-1', 'another_id' => 'bar-2']),
            ['external_id', 'another_id'], []);
        $query = $this->getMockQueryBuilder();
        $nestedQuery = m::mock(Builder::class);
        $nestedQuery->shouldReceive('getQuery')->once()->andReturn($query);
        $model = m::mock(Model::class);
        $model->shouldReceive('getTable')->andReturn('foo_table');
        $model->shouldReceive('newQueryWithoutRelationships')->once()->andReturn($nestedQuery);
        $builder = new Builder($query);
        $builder->setModel($model);
        $nestedQuery->shouldReceive('where')->once()->with('external_id', 'foo-1');
        $nestedQuery->shouldReceive('where')->once()->with('another_id', 'bar-2');
        $builder->getQuery()->shouldReceive('addNestedWhereQuery')->once()->with($query, 'and');
        $sut = new MultiColTargetModel();
        $b = $sut->scopeSourceIdentifierIs($builder, $identifier);
        $this->assertSame($b, $builder);
    }

    public function testScopeSourceIdentifierIsSingleColumn(): void
    {
        $identifier = new Identifiers(new SingleColTargetModel(['external_id' => 'foo-1']), ['external_id'], []);
        $builder = new Builder($this->getMockQueryBuilder());
        $builder->getQuery()->shouldReceive('where')->once()->with('external_id', '=', 'foo-1');
        $sut = new SingleColTargetModel();
        $b = $sut->scopeSourceIdentifierIs($builder, $identifier);
        $this->assertSame($b, $builder);
    }

    public function testScopeVirtualSourceIdentifierInMultiColumn(): void
    {
        $this->sourceIdentifierIsVirtual = true;
        $externalIds = [
            new Identifiers(new MultiColVirtualSourceTargetModel(['external_id' => 'foo-1', 'another_id' => 'bar-2']),
                ['external_id', 'another_id'], []),
            new Identifiers(new MultiColVirtualSourceTargetModel(['external_id' => 'foo-2', 'another_id' => 'bar-3']),
                ['external_id', 'another_id'], []),
        ];
        DB::shouldReceive('raw')->once()->with('1=0')->andReturn('1=0');
        $builder = new Builder($this->getMockQueryBuilder());
        $builder->getQuery()->shouldReceive('where')->once()->with('1=0');
        $b = $this->scopeSourceIdentifierIn($builder, $externalIds);
        $this->assertSame($b, $builder);
    }

    public function testScopeVirtualSourceIdentifierInSingleColumn(): void
    {
        $this->sourceIdentifierIsVirtual = true;
        $identifier = new Identifiers(new SingleColVirtualSourceTargetModel(['external_id' => 'foo-1']),
            ['external_id'], []);
        DB::shouldReceive('raw')->once()->with('1=0')->andReturn('1=0');
        $builder = new Builder($this->getMockQueryBuilder());
        $builder->getQuery()->shouldReceive('where')->once()->with('1=0');
        $b = $this->scopeSourceIdentifierIn($builder, [$identifier]);
        $this->assertSame($b, $builder);
    }

    protected function getMockQueryBuilder(): \Illuminate\Database\Query\Builder
    {
        $query = m::mock(\Illuminate\Database\Query\Builder::class);
        $query->shouldReceive('from')->with('foo_table');

        return $query;
    }
}
