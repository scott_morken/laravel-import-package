<?php

namespace Tests\Smorken\Import\Unit;

use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Import\Contracts\Enums\PartsFactoryParts;
use Smorken\Import\Contracts\Importer;
use Smorken\Import\Contracts\Models\Data;
use Smorken\Import\Contracts\Models\Source;
use Smorken\Import\Contracts\Models\Target;
use Smorken\Import\Contracts\Storage\HasSource;
use Smorken\Import\Contracts\Storage\HasTarget;
use Smorken\Import\Lookups;
use Smorken\Import\MapId;
use Smorken\Import\PartsFactory;
use Smorken\Import\Results;
use Smorken\Support\Filter;
use Tests\Smorken\Import\Unit\Stubs\MultiColDataModel;
use Tests\Smorken\Import\Unit\Stubs\MultiColSourceModel;
use Tests\Smorken\Import\Unit\Stubs\MultiColTargetModel;
use Tests\Smorken\Import\Unit\Stubs\StubModeller;

class ImporterTest extends TestCase
{
    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testHandle(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sourceModels = new Collection([
            $this->getSourceModel(['SC_ID' => 'S1', 'SC_ANOTHER' => 'SA1', 'SC_NAME' => 'S 1']),
            $this->getSourceModel(['SC_ID' => 'S2', 'SC_ANOTHER' => 'SA2', 'SC_NAME' => 'S 2']),
            $this->getSourceModel(['SC_ID' => 'S3', 'SC_ANOTHER' => 'SA3', 'SC_NAME' => 'S 3']),
        ]);
        $targetModelsExisting = new Collection([
            $this->getTargetModel([
                'id_1' => '1-1', 'id_2' => '2-1', 'external_id' => 'S1', 'another_id' => 'SA1', 'name' => 'foo',
            ]),
        ]);
        $targetModelsCreate = new Collection([
            $this->getTargetModel([
                'id_1' => '1-2', 'id_2' => '2-2', 'external_id' => 'S2', 'another_id' => 'SA2', 'name' => 'S 2',
            ]),
            $this->getTargetModel([
                'id_1' => '1-3', 'id_2' => '2-3', 'external_id' => 'S3', 'another_id' => 'SA3', 'name' => 'S 3',
            ]),
        ]);
        $sut->getFactory()
            ->getTargetProvider()
            ->shouldReceive('getBySourceIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['S1:SA1', 'S2:SA2', 'S3:SA3'];
                $this->assertEquals($expected, array_keys($identifiers));

                return count($identifiers) === 3;
            }))
            ->andReturn($targetModelsExisting);
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('createMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 2;
            }))
            ->andReturn($targetModelsCreate);
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('updateMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 1;
            }))
            ->andReturn(1);
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('touchMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 0;
            }))
            ->andReturn(0);
        $sut->handle($sourceModels);
        $expected = [
            'Test::total' => 3,
            'Test::validated' => 3,
            'Test::existing' => 1,
            'Test::created' => 2,
            'Test::touched' => 0,
            'Test::updated' => 1,
        ];
        $this->assertEquals($expected, $sut->getFactory()->getResults()->getCounters());
    }

    public function testSample(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sourceModels = new Collection([
            $this->getSourceModel(['SC_ID' => 'S1', 'SC_ANOTHER' => 'SA1', 'SC_NAME' => 'S 1']),
            $this->getSourceModel(['SC_ID' => 'S2', 'SC_ANOTHER' => 'SA2', 'SC_NAME' => 'S 2']),
            $this->getSourceModel(['SC_ID' => 'S3', 'SC_ANOTHER' => 'SA3', 'SC_NAME' => 'S 3']),
        ]);
        $targetModelsExisting = new Collection([
            $this->getTargetModel([
                'id_1' => '1-1', 'id_2' => '2-1', 'external_id' => 'S1', 'another_id' => 'SA1', 'name' => 'foo',
            ]),
        ]);
        $sut->getFactory()
            ->getSourceProvider()
            ->shouldReceive('forImport')
            ->once()
            ->andReturnUsing(function ($filter, $callback, $perPage) use ($sourceModels) {
                $callback($sourceModels);

            });
        $sut->getFactory()
            ->getTargetProvider()
            ->shouldReceive('getBySourceIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['S1:SA1', 'S2:SA2', 'S3:SA3'];
                $this->assertEquals($expected, array_keys($identifiers));

                return count($identifiers) === 3;
            }))
            ->andReturn($targetModelsExisting);
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('createMany')
            ->never();
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('updateMany')
            ->never();
        $sut->getFactory()->getTargetProvider()
            ->shouldReceive('touchMany')
            ->never();
        $sut->sample(new Filter());
        $expected = [
            'Test::total' => 3,
            'Test::validated' => 3,
            'Test::existing' => 1,
        ];
        $this->assertEquals($expected, $sut->getFactory()->getResults()->getCounters());
    }

    protected function getDataModel(array $attributes = []): Data
    {
        return new MultiColDataModel($attributes);
    }

    protected function getPartsFactory(): \Smorken\Import\Contracts\PartsFactory
    {
        $parts = new PartsFactory([
            PartsFactoryParts::RESULTS => new Results('Test'),
            PartsFactoryParts::TARGET_PROVIDER => m::mock(HasTarget::class),
            PartsFactoryParts::SOURCE_PROVIDER => m::mock(HasSource::class),
            PartsFactoryParts::DATA_MODEL => $this->getDataModel(),
            PartsFactoryParts::LOOKUPS => new Lookups(['default' => new MapId()]),
            PartsFactoryParts::DATA_TARGET_MODELLER => new StubModeller(),
        ]);
        $parts->getTargetProvider()
            ->shouldReceive('getTarget')
            ->andReturn($this->getTargetModel());

        return $parts;
    }

    protected function getSourceModel(array $attributes = []): Source
    {
        return (new MultiColSourceModel())->forceFill($attributes);
    }

    protected function getSut(\Smorken\Import\Contracts\PartsFactory $partsFactory
    ): Importer {
        return new \Smorken\Import\Importer($partsFactory);
    }

    protected function getTargetModel(array $attributes = [], bool $exists = true): Target
    {
        $m = (new MultiColTargetModel())->forceFill($attributes);
        $m->exists = $exists;

        return $m;
    }
}
