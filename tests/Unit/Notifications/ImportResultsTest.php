<?php

namespace Tests\Smorken\Import\Unit\Notifications;

use PHPUnit\Framework\TestCase;
use Smorken\Import\Models\VO\Notifiable;
use Smorken\Import\Notifications\ImportResults;
use Smorken\Import\Results;

class ImportResultsTest extends TestCase
{
    public function testMailableCreatesLinesFromResults(): void
    {
        $results = new Results('Foo');
        $results->init();
        $results->increment('created', 25);
        $results->increment('updated', 5);
        $results->addMessage('error', 'Some error');
        $results->addMessage('error', 'Another error');
        $results->addMessage('blah', 'Blah blah blah');
        $results->stop();
        $sut = new ImportResults($results);
        $mailable = $sut->toMail(new Notifiable(['email' => 'foo@example.com']));
        $expected = [
            'level' => 'info',
            'subject' => 'Import results for Foo',
            'greeting' => 'Import results for Foo',
            'salutation' => null,
            'outroLines' => [],
            'actionText' => null,
            'actionUrl' => null,
            'displayableActionUrl' => '',
        ];
        $fromMailable = $mailable->toArray();
        foreach ($expected as $k => $v) {
            $this->assertEquals($v, $fromMailable[$k]);
        }
        $lines = [
            'Foo::created: 25',
            'Foo::updated: 5',
            'Messages.0.Foo::error: Some error',
            'Messages.1.Foo::error: Another error',
            'Messages.2.Foo::blah: Blah blah blah',
        ];
        foreach ($lines as $line) {
            $this->assertContains($line, $fromMailable['introLines']);
        }
    }
}
