<?php

namespace Tests\Smorken\Import\Unit;

use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Import\Contracts\Enums\PartsFactoryParts;
use Smorken\Import\Contracts\Importer;
use Smorken\Import\Contracts\Importers;
use Smorken\Import\Contracts\Models\Data;
use Smorken\Import\Contracts\Models\Source;
use Smorken\Import\Contracts\Models\Target;
use Smorken\Import\Contracts\Multi;
use Smorken\Import\Contracts\Storage\HasSource;
use Smorken\Import\Contracts\Storage\HasTarget;
use Smorken\Import\Lookups;
use Smorken\Import\MapId;
use Smorken\Import\PartsFactory;
use Smorken\Import\Results;
use Smorken\Support\Filter;
use Tests\Smorken\Import\Unit\Stubs\MultiColDataModel;
use Tests\Smorken\Import\Unit\Stubs\MultiColSourceModel;
use Tests\Smorken\Import\Unit\Stubs\MultiColTargetModel;
use Tests\Smorken\Import\Unit\Stubs\SingleColDataModel;
use Tests\Smorken\Import\Unit\Stubs\SingleColTargetModel;
use Tests\Smorken\Import\Unit\Stubs\StubModeller;

class MultiTest extends TestCase
{
    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testHandle(): void
    {
        $partsFactory = $this->getPartsFactory($this->getDataModel(), $this->getTargetModel());
        $importers = $this->getImporters([
            'MultiOne' => $this->getImporter($partsFactory),
        ]);
        $sut = $this->getSut($importers, $partsFactory->getSourceProvider());
        $sourceModels = new Collection([
            $this->getSourceModel(['SC_ID' => 'S1', 'SC_ANOTHER' => 'SA1', 'SC_NAME' => 'S 1']),
            $this->getSourceModel(['SC_ID' => 'S2', 'SC_ANOTHER' => 'SA2', 'SC_NAME' => 'S 2']),
            $this->getSourceModel(['SC_ID' => 'S3', 'SC_ANOTHER' => 'SA3', 'SC_NAME' => 'S 3']),
        ]);
        $targetModelsExisting = new Collection([
            $this->getTargetModel([
                'id_1' => '1-1', 'id_2' => '2-1', 'external_id' => 'S1', 'another_id' => 'SA1', 'name' => 'foo',
            ]),
        ]);
        $targetModelsCreate = new Collection([
            $this->getTargetModel([
                'id_1' => '1-2', 'id_2' => '2-2', 'external_id' => 'S2', 'another_id' => 'SA2', 'name' => 'S 2',
            ]),
            $this->getTargetModel([
                'id_1' => '1-3', 'id_2' => '2-3', 'external_id' => 'S3', 'another_id' => 'SA3', 'name' => 'S 3',
            ]),
        ]);
        $sut->getSource()
            ->shouldReceive('forImport')
            ->once()
            ->andReturnUsing(function ($filter, $callback, $perPage) use ($sourceModels) {
                $callback($sourceModels);

            });
        $partsFactory->getTargetProvider()
            ->shouldReceive('getBySourceIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['S1:SA1', 'S2:SA2', 'S3:SA3'];
                $this->assertEquals($expected, array_keys($identifiers));

                return count($identifiers) === 3;
            }))
            ->andReturn($targetModelsExisting);
        $partsFactory->getTargetProvider()
            ->shouldReceive('createMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 2;
            }))
            ->andReturn($targetModelsCreate);
        $partsFactory->getTargetProvider()
            ->shouldReceive('updateMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 1;
            }))
            ->andReturn(1);
        $partsFactory->getTargetProvider()
            ->shouldReceive('touchMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 0;
            }))
            ->andReturn(0);
        $partsFactory->getTargetProvider()
            ->shouldReceive('cleanup')
            ->once();
        $results = $sut->handle(new Filter());
        $expected = [
            'Test::total' => 3,
            'Test::validated' => 3,
            'Test::existing' => 1,
            'Test::created' => 2,
            'Test::touched' => 0,
            'Test::updated' => 1,
            'Test::deleted' => 0,
        ];
        $this->assertEquals($expected, $results->getCounters());
    }

    public function testHandleWithTwo(): void
    {
        $importers = $this->getImporters([
            'One' => $this->getImporter($this->getPartsFactory($this->getDataModel(), $this->getTargetModel(), 'One')),
            'Two' => $this->getImporter($this->getPartsFactory($this->getSingleColDataModel(),
                $this->getSingleColTargetModel(), 'Two')),
        ]);
        $sut = $this->getSut($importers, m::mock(HasSource::class));
        $sourceModels = new Collection([
            $this->getSourceModel(['SC_ID' => 'S1', 'SC_ANOTHER' => 'SA1', 'SC_NAME' => 'S 1']),
            $this->getSourceModel(['SC_ID' => 'S2', 'SC_ANOTHER' => 'SA2', 'SC_NAME' => 'S 2']),
            $this->getSourceModel(['SC_ID' => 'S3', 'SC_ANOTHER' => 'SA3', 'SC_NAME' => 'S 3']),
        ]);
        $oneTargetModelsExisting = new Collection([
            $this->getTargetModel([
                'id_1' => '1-1', 'id_2' => '2-1', 'external_id' => 'S1', 'another_id' => 'SA1', 'name' => 'foo',
            ]),
        ]);
        $oneTargetModelsCreate = new Collection([
            $this->getTargetModel([
                'id_1' => '1-2', 'id_2' => '2-2', 'external_id' => 'S2', 'another_id' => 'SA2', 'name' => 'S 2',
            ]),
            $this->getTargetModel([
                'id_1' => '1-3', 'id_2' => '2-3', 'external_id' => 'S3', 'another_id' => 'SA3', 'name' => 'S 3',
            ]),
        ]);
        $twoTargetModelsCreate = new Collection([
            $this->getSingleColTargetModel([
                'id' => '1', 'external_id' => 'S1', 'name' => 'S 1',
            ]),
            $this->getSingleColTargetModel([
                'id' => '2', 'external_id' => 'S2', 'name' => 'S 2',
            ]),
            $this->getSingleColTargetModel([
                'id' => '3', 'external_id' => 'S3', 'name' => 'S 3',
            ]),
        ]);
        $sut->getSource()
            ->shouldReceive('forImport')
            ->once()
            ->andReturnUsing(function ($filter, $callback, $perPage) use ($sourceModels) {
                $callback($sourceModels);

            });
        $importers->get('One')->getFactory()->getTargetProvider()
            ->shouldReceive('getBySourceIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['S1:SA1', 'S2:SA2', 'S3:SA3'];
                $this->assertEquals($expected, array_keys($identifiers));

                return count($identifiers) === 3;
            }))
            ->andReturn($oneTargetModelsExisting);
        $importers->get('Two')->getFactory()->getTargetProvider()
            ->shouldReceive('getBySourceIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['S1', 'S2', 'S3'];
                $this->assertEquals($expected, array_keys($identifiers));

                return count($identifiers) === 3;
            }))
            ->andReturn(new Collection());
        $importers->get('One')->getFactory()->getTargetProvider()
            ->shouldReceive('createMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 2;
            }))
            ->andReturn($oneTargetModelsCreate);
        $importers->get('Two')->getFactory()->getTargetProvider()
            ->shouldReceive('createMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 3;
            }))
            ->andReturn($twoTargetModelsCreate);
        $importers->get('One')->getFactory()->getTargetProvider()
            ->shouldReceive('updateMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 1;
            }))
            ->andReturn(1);
        $importers->get('Two')->getFactory()->getTargetProvider()
            ->shouldReceive('updateMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 0;
            }))
            ->andReturn(0);
        $importers->get('One')->getFactory()->getTargetProvider()
            ->shouldReceive('touchMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 0;
            }))
            ->andReturn(0);
        $importers->get('Two')->getFactory()->getTargetProvider()
            ->shouldReceive('touchMany')
            ->once()
            ->with(m::on(function (Collection $models) {
                return $models->count() === 0;
            }))
            ->andReturn(0);
        $importers->get('One')->getFactory()->getTargetProvider()
            ->shouldReceive('cleanup')
            ->once();
        $importers->get('Two')->getFactory()->getTargetProvider()
            ->shouldReceive('cleanup')
            ->once();
        $results = $sut->handle(new Filter());
        $expected = [
            'One::total' => 3,
            'One::validated' => 3,
            'One::existing' => 1,
            'One::created' => 2,
            'One::touched' => 0,
            'One::updated' => 1,
            'One::deleted' => 0,
            'Two::total' => 3,
            'Two::validated' => 3,
            'Two::existing' => 0,
            'Two::created' => 3,
            'Two::touched' => 0,
            'Two::updated' => 0,
            'Two::deleted' => 0,
        ];
        $this->assertEquals($expected, $results->getCounters());
    }

    protected function getDataModel(array $attributes = []): Data
    {
        return new MultiColDataModel($attributes);
    }

    protected function getImporter(PartsFactory $partsFactory): Importer
    {
        return new \Smorken\Import\Importer($partsFactory);
    }

    protected function getImporters(array $importers): Importers
    {
        return new \Smorken\Import\Importers($importers);
    }

    protected function getPartsFactory(
        Data $dataModel,
        Target $targetModel,
        string $name = 'Test'
    ): \Smorken\Import\Contracts\PartsFactory {
        $parts = new PartsFactory([
            PartsFactoryParts::RESULTS => new Results($name),
            PartsFactoryParts::TARGET_PROVIDER => m::mock(HasTarget::class),
            PartsFactoryParts::SOURCE_PROVIDER => m::mock(HasSource::class),
            PartsFactoryParts::DATA_MODEL => $dataModel,
            PartsFactoryParts::LOOKUPS => new Lookups(['default' => new MapId()]),
            PartsFactoryParts::DATA_TARGET_MODELLER => new StubModeller(),
        ]);
        $parts->getTargetProvider()
            ->shouldReceive('getTarget')
            ->andReturn($targetModel);

        return $parts;
    }

    protected function getSingleColDataModel(array $attributes = []): Data
    {
        return new SingleColDataModel($attributes);
    }

    protected function getSingleColTargetModel(array $attributes = [], bool $exists = true): Target
    {
        $m = (new SingleColTargetModel())->forceFill($attributes);
        $m->exists = $exists;

        return $m;
    }

    protected function getSourceModel(array $attributes = []): Source
    {
        return (new MultiColSourceModel())->forceFill($attributes);
    }

    protected function getSut(Importers $importers, HasSource $source): Multi
    {
        return new \Smorken\Import\Multi($importers, $source);
    }

    protected function getTargetModel(array $attributes = [], bool $exists = true): Target
    {
        $m = (new MultiColTargetModel())->forceFill($attributes);
        $m->exists = $exists;

        return $m;
    }
}
