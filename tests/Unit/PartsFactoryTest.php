<?php

namespace Tests\Smorken\Import\Unit;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Import\Contracts\MapId;
use Smorken\Import\Contracts\Results;
use Smorken\Import\Contracts\Storage\HasSource;
use Smorken\Import\Contracts\Storage\HasTarget;
use Smorken\Import\DataAndTargetModeller;
use Smorken\Import\Lookups;
use Smorken\Import\PartsFactory;
use Tests\Smorken\Import\Unit\Stubs\MultiColDataModel;
use Tests\Smorken\Import\Unit\Stubs\SingleColSourceModel;
use Tests\Smorken\Import\Unit\Stubs\TargetDoesNotWantPartsFactory;

class PartsFactoryTest extends TestCase
{
    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testCanCreateDefaultLookupsAndMapIdWhenNotSet(): void
    {
        $sut = new PartsFactory();
        $lookups = $sut->getLookups();
        $this->assertInstanceOf(MapId::class, $lookups->get());
    }

    public function testCanCreateResultsWhenNotSet(): void
    {
        $sut = new PartsFactory();
        $r = $sut->getResults();
        $this->assertInstanceOf(Results::class, $r);
    }

    public function testCanGetLookupWhenAdded(): void
    {
        $sut = new PartsFactory();
        $m = new \Smorken\Import\MapId();
        $sut->getLookups()->setMapId('foo', $m);
        $this->assertSame($m, $sut->getLookups()->get('foo'));
    }

    public function testChainableSets(): void
    {
        $sut = new PartsFactory();
        $lookups = new Lookups([]);
        $modeller = new DataAndTargetModeller();
        $dataModel = new MultiColDataModel();
        $results = new \Smorken\Import\Results();
        $sourceProvider = m::mock(HasSource::class);
        $targetProvider = m::mock(HasTarget::class);
        $sut->setLookups($lookups)
            ->setDataAndTargetModeller($modeller)
            ->setDataModel($dataModel)
            ->setResults($results)
            ->setSourceProvider($sourceProvider)
            ->setTargetProvider($targetProvider);
        $this->assertSame($lookups, $sut->getLookups());
        $this->assertSame($modeller, $sut->getDataAndTargetModeller());
        $this->assertSame($dataModel, $sut->getDataModel());
        $this->assertSame($results, $sut->getResults());
        $this->assertSame($sourceProvider, $sut->getSourceProvider());
        $this->assertSame($targetProvider, $sut->getTargetProvider());
    }

    public function testDoesNotSetFactoryOnPartWhenDoesNotWant(): void
    {
        $sut = new PartsFactory();
        $t = new TargetDoesNotWantPartsFactory(new SingleColSourceModel());
        $sut->setTargetProvider($t);
        $this->assertEquals('', $sut->getTargetProvider()->getFactory());
    }
}
