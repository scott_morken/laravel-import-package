<?php

namespace Tests\Smorken\Import\Unit;

use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Import\Contracts\Enums\PartsFactoryParts;
use Smorken\Import\Contracts\Models\Data;
use Smorken\Import\Contracts\Models\Source;
use Smorken\Import\Contracts\Models\Target;
use Smorken\Import\Contracts\Storage\HasSource;
use Smorken\Import\Contracts\Storage\HasTarget;
use Smorken\Import\Lookups;
use Smorken\Import\MapId;
use Smorken\Import\Models\Source\VO;
use Smorken\Import\PartsFactory;
use Smorken\Import\Results;
use Tests\Smorken\Import\Unit\Stubs\MultiColDataModel;
use Tests\Smorken\Import\Unit\Stubs\MultiColSourceModel;
use Tests\Smorken\Import\Unit\Stubs\MultiColTargetModel;
use Tests\Smorken\Import\Unit\Stubs\SingleColCanCreateDataModel;
use Tests\Smorken\Import\Unit\Stubs\StubModeller;

class DataAndTargetModellerTest extends TestCase
{
    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testCreateModelledDataModelFailsValidation(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sourceModels = new Collection([
            $this->getSourceModel(['SC_ID' => 'S1', 'SC_ANOTHER' => 'SA1', 'SC_NAME' => 'S 1']),
            $this->getSourceModel(['SC_ID' => 'S2', 'SC_ANOTHER' => 'SA2', 'SC_NAME' => 'S 2']),
            $this->getSourceModel(['SC_ID' => 'S3', 'SC_ANOTHER' => 'SA3', 'SC_NAME' => 'fail']),
        ]);
        $sut->getFactory()
            ->getTargetProvider()
            ->shouldReceive('getBySourceIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['S1:SA1', 'S2:SA2'];
                $this->assertEquals($expected, array_keys($identifiers));

                return count($identifiers) === 2;
            }))
            ->andReturn(new Collection());
        $modelled = $sut->fromSourceModels($sourceModels);
        $this->assertCount(2, $modelled->getDataModels());
        $this->assertCount(0, $modelled->getTargetModels());
        $firstData = $modelled->getDataModels()->first();
        $this->assertEquals('before', $firstData->getSourceModel()->SC_NAME);
        $this->assertEquals('before after', $firstData->name);
        foreach ($modelled->getDataModels()->slice(1) as $dataModel) {
            $this->assertEquals($dataModel->getSourceModel()->SC_NAME, $dataModel->name);
        }
        $this->assertCount(1, $modelled->getValidationFailures());
        $this->assertInstanceOf(MultiColDataModel::class, $modelled->getValidationFailures()[0]);
        $this->assertEquals('fail', $modelled->getValidationFailures()[0]->name);
    }

    public function testCreateModelledDataModelWithShouldCreateFailure(): void
    {
        $parts = $this->getPartsFactory();
        $parts->setDataModel(new SingleColCanCreateDataModel());
        $sut = $this->getSut($parts);
        $sourceModels = new Collection([
            $this->getSourceModel(['SC_ID' => 'S1', 'SC_NAME' => 'S 1']),
            $this->getSourceModel(['SC_ID' => 'S2', 'SC_NAME' => 'S 2']),
            $this->getSourceModel(['SC_ID' => 'S3', 'SC_NAME' => 'fail']),
        ]);
        $sut->getFactory()
            ->getTargetProvider()
            ->shouldReceive('getBySourceIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['S1', 'S2'];
                $this->assertEquals($expected, array_keys($identifiers));

                return count($identifiers) === 2;
            }))
            ->andReturn(new Collection());
        $modelled = $sut->fromSourceModels($sourceModels);
        $this->assertCount(2, $modelled->getDataModels());
        $this->assertCount(0, $modelled->getTargetModels());
        $firstData = $modelled->getDataModels()->first();
        $this->assertEquals('before', $firstData->getSourceModel()->SC_NAME);
        $this->assertEquals('before after', $firstData->name);
        foreach ($modelled->getDataModels()->slice(1) as $dataModel) {
            $this->assertEquals($dataModel->getSourceModel()->SC_NAME, $dataModel->name);
        }
        $this->assertCount(0, $modelled->getValidationFailures());
    }

    public function testCreateModelledNoTargetModels(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sourceModels = new Collection([
            $this->getSourceModel(['SC_ID' => 'S1', 'SC_ANOTHER' => 'SA1', 'SC_NAME' => 'S 1']),
            $this->getSourceModel(['SC_ID' => 'S2', 'SC_ANOTHER' => 'SA2', 'SC_NAME' => 'S 2']),
            $this->getSourceModel(['SC_ID' => 'S3', 'SC_ANOTHER' => 'SA3', 'SC_NAME' => 'S 3']),
        ]);
        $sut->getFactory()
            ->getTargetProvider()
            ->shouldReceive('getBySourceIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['S1:SA1', 'S2:SA2', 'S3:SA3'];
                $this->assertEquals($expected, array_keys($identifiers));

                return count($identifiers) === 3;
            }))
            ->andReturn(new Collection());
        $modelled = $sut->fromSourceModels($sourceModels);
        $this->assertCount(3, $modelled->getDataModels());
        $this->assertCount(0, $modelled->getTargetModels());
        $firstData = $modelled->getDataModels()->first();
        $this->assertEquals('before', $firstData->getSourceModel()->SC_NAME);
        $this->assertEquals('before after', $firstData->name);
        foreach ($modelled->getDataModels()->slice(1) as $dataModel) {
            $this->assertEquals($dataModel->getSourceModel()->SC_NAME, $dataModel->name);
        }
        $this->assertCount(0, $modelled->getValidationFailures());
    }

    public function testCreateModelledNoTargetModelsMixedSourceModels(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sourceModels = new Collection([
            $this->getSourceModel(['SC_ID' => 'S1', 'SC_ANOTHER' => 'SA1', 'SC_NAME' => 'S 1']),
            new VO(['SC_ID' => 'S2', 'SC_ANOTHER' => 'SA2', 'SC_NAME' => 'S 2']),
            $this->getSourceModel(['SC_ID' => 'S3', 'SC_ANOTHER' => 'SA3', 'SC_NAME' => 'S 3']),
        ]);
        $sut->getFactory()
            ->getTargetProvider()
            ->shouldReceive('getBySourceIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['S1:SA1', 'S2:SA2', 'S3:SA3'];
                $this->assertEquals($expected, array_keys($identifiers));

                return count($identifiers) === 3;
            }))
            ->andReturn(new Collection());
        $modelled = $sut->fromSourceModels($sourceModels);
        $this->assertCount(3, $modelled->getDataModels());
        $this->assertCount(0, $modelled->getTargetModels());
        $firstData = $modelled->getDataModels()->first();
        $this->assertEquals('before', $firstData->getSourceModel()->SC_NAME);
        $this->assertEquals('before after', $firstData->name);
        foreach ($modelled->getDataModels()->slice(1) as $dataModel) {
            $this->assertEquals($dataModel->getSourceModel()->SC_NAME, $dataModel->name);
        }
        $this->assertCount(0, $modelled->getValidationFailures());
    }

    public function testCreateModelledWithExistingLookups(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sourceModels = new Collection([
            $this->getSourceModel(['SC_ID' => 'S1', 'SC_ANOTHER' => 'SA1', 'SC_NAME' => 'S 1']),
            $this->getSourceModel(['SC_ID' => 'S2', 'SC_ANOTHER' => 'SA2', 'SC_NAME' => 'S 2']),
            $this->getSourceModel(['SC_ID' => 'S3', 'SC_ANOTHER' => 'SA3', 'SC_NAME' => 'S 3']),
        ]);
        $sut->getFactory()->getLookups()->get()->set('S2:SA2', '1-2:2-2');
        $sut->getFactory()->getLookups()->get()->set('S3:SA3', '1-3:2-3');
        $targetModels = new Collection([
            $this->getTargetModel([
                'id_1' => '1-1', 'id_2' => '2-1', 'external_id' => 'S1', 'another_id' => 'SA1', 'name' => 'foo',
            ]),
        ]);
        $sut->getFactory()
            ->getTargetProvider()
            ->shouldReceive('getBySourceIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['S1:SA1'];
                $this->assertEquals($expected, array_keys($identifiers));

                return count($identifiers) === 1;
            }))
            ->andReturn($targetModels);
        $modelled = $sut->fromSourceModels($sourceModels);
        $this->assertCount(1, $modelled->getDataModels());
        $this->assertCount(1, $modelled->getTargetModels());
        $firstData = $modelled->getDataModels()->first();
        $this->assertEquals('before', $firstData->getSourceModel()->SC_NAME);
        $this->assertEquals('before after', $firstData->name);
        foreach ($modelled->getDataModels()->slice(1) as $dataModel) {
            $this->assertEquals($dataModel->getSourceModel()->SC_NAME, $dataModel->name);
        }
        $this->assertEquals('foo AFTER', $targetModels->first()->name);
        $this->assertEquals('1-1:2-1', $sut->getFactory()->getLookups()->get()->get('S1:SA1'));
        $this->assertCount(0, $modelled->getValidationFailures());
    }

    public function testCreateModelledWithTargetModel(): void
    {
        $sut = $this->getSut($this->getPartsFactory());
        $sourceModels = new Collection([
            $this->getSourceModel(['SC_ID' => 'S1', 'SC_ANOTHER' => 'SA1', 'SC_NAME' => 'S 1']),
            $this->getSourceModel(['SC_ID' => 'S2', 'SC_ANOTHER' => 'SA2', 'SC_NAME' => 'S 2']),
            $this->getSourceModel(['SC_ID' => 'S3', 'SC_ANOTHER' => 'SA3', 'SC_NAME' => 'S 3']),
        ]);
        $targetModels = new Collection([
            $this->getTargetModel([
                'id_1' => '1-1', 'id_2' => '2-1', 'external_id' => 'S1', 'another_id' => 'SA1', 'name' => 'foo',
            ]),
        ]);
        $sut->getFactory()
            ->getTargetProvider()
            ->shouldReceive('getBySourceIdentifiers')
            ->once()
            ->with(m::on(function (array $identifiers) {
                $expected = ['S1:SA1', 'S2:SA2', 'S3:SA3'];
                $this->assertEquals($expected, array_keys($identifiers));

                return count($identifiers) === 3;
            }))
            ->andReturn($targetModels);
        $modelled = $sut->fromSourceModels($sourceModels);
        $this->assertCount(3, $modelled->getDataModels());
        $this->assertCount(1, $modelled->getTargetModels());
        $firstData = $modelled->getDataModels()->first();
        $this->assertEquals('before', $firstData->getSourceModel()->SC_NAME);
        $this->assertEquals('before after', $firstData->name);
        foreach ($modelled->getDataModels()->slice(1) as $dataModel) {
            $this->assertEquals($dataModel->getSourceModel()->SC_NAME, $dataModel->name);
        }
        $this->assertEquals('foo AFTER', $targetModels->first()->name);
        $this->assertEquals('1-1:2-1', $sut->getFactory()->getLookups()->get()->get('S1:SA1'));
        $this->assertCount(0, $modelled->getValidationFailures());
    }

    protected function getDataModel(array $attributes = []): Data
    {
        return new MultiColDataModel($attributes);
    }

    protected function getPartsFactory(): \Smorken\Import\Contracts\PartsFactory
    {
        $parts = new PartsFactory([
            PartsFactoryParts::RESULTS => new Results('Test'),
            PartsFactoryParts::TARGET_PROVIDER => m::mock(HasTarget::class),
            PartsFactoryParts::SOURCE_PROVIDER => m::mock(HasSource::class),
            PartsFactoryParts::DATA_MODEL => $this->getDataModel(),
            PartsFactoryParts::LOOKUPS => new Lookups(['default' => new MapId()]),
            PartsFactoryParts::DATA_TARGET_MODELLER => new StubModeller(),
        ]);
        $parts->getTargetProvider()
            ->shouldReceive('getTarget')
            ->andReturn($this->getTargetModel());

        return $parts;
    }

    protected function getSourceModel(array $attributes = []): Source
    {
        return (new MultiColSourceModel())->forceFill($attributes);
    }

    protected function getSut(\Smorken\Import\Contracts\PartsFactory $partsFactory
    ): \Smorken\Import\Contracts\DataAndTargetModeller {
        $m = new StubModeller();
        $m->setFactory($partsFactory);

        return $m;
    }

    protected function getTargetModel(array $attributes = []): Target
    {
        return (new MultiColTargetModel())->forceFill($attributes);
    }
}
