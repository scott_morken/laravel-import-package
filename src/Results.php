<?php

namespace Smorken\Import;

use Carbon\Carbon;

class Results implements \Smorken\Import\Contracts\Results
{
    protected string $base = '';

    protected array $counters = [];

    protected Carbon $end;

    protected float $mem = 0.0;

    protected array $messages = [];

    protected Carbon $start;

    protected bool $started = false;

    protected bool $stopped = false;

    public function __construct(?string $base = null)
    {
        if ($base) {
            $this->setBase($base);
        }
    }

    public function addMessage(string $key, string $message): void
    {
        $this->messages[] = [$this->getKey($key) => $message];
    }

    public function decrement(string $key, int $count = 1): void
    {
        $this->changeCount($key, $count * -1);
    }

    public function getCounter(string $key): int
    {
        return $this->counters[$this->getKey($key)] ?? 0;
    }

    public function getCounters(): array
    {
        return $this->counters;
    }

    public function setCounters(array $counters): void
    {
        foreach ($counters as $k => $v) {
            $this->setCounter($k, $v);
        }
    }

    public function getMessages(): array
    {
        return $this->messages;
    }

    public function getName(): string
    {
        return $this->base ?: 'Results';
    }

    public function increment(string $key, int $count = 1): void
    {
        $this->changeCount($key, $count);
    }

    public function init(array $counters = []): void
    {
        $this->initCounters($counters);
        $this->initMessages();
        $this->start();
    }

    public function initCounters(array $counters = []): void
    {
        $this->counters = [];
        foreach ($counters as $counter) {
            $this->reset($this->getKey($counter));
        }
    }

    public function initMessages(): void
    {
        $this->messages = [];
    }

    public function newInstance(?string $base = null): static
    {
        return new static($base);
    }

    public function reset(string $key): void
    {
        $this->setCounter($this->getKey($key), 0);
    }

    public function resetAll(): void
    {
        $this->started = false;
        $this->stopped = false;
        $this->init(array_keys($this->getCounters()));
    }

    public function setBase(string $base, bool $force = false): void
    {
        if (! $this->base || $force) {
            $this->base = $base;
        }
    }

    public function setCounter(string $key, int $value): void
    {
        $this->counters[$this->getKey($key)] = $value;
    }

    public function start(): void
    {
        if (! $this->started) {
            $this->start = Carbon::now();
            $this->started = true;
        }
    }

    public function stop(): void
    {
        if (! $this->stopped) {
            $this->getMemoryUse();
            $this->end = Carbon::now();
            $this->stopped = true;
        }
    }

    public function toArray(): array
    {
        $this->start();
        $this->stop();

        return array_merge($this->getCounters(), [
            'start' => $this->start,
            'end' => $this->end,
            'memory' => $this->getMemoryUse(),
            'messages' => $this->getMessages(),
        ]);
    }

    protected function changeCount(string $key, int $count): void
    {
        $key = $this->getKey($key);
        $current = $this->getCounter($key);
        $this->counters[$key] = $current + $count;
    }

    protected function getKey(string $key): string
    {
        if ($this->base && ! str_contains($key, '::')) {
            return $this->base.'::'.$key;
        }

        return $key;
    }

    protected function getMemoryUse(): float
    {
        if (! $this->mem) {
            $this->mem = memory_get_peak_usage(true) / 2 ** 20;
        }

        return $this->mem;
    }
}
