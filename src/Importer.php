<?php

namespace Smorken\Import;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Event;
use JetBrains\PhpStorm\ArrayShape;
use Smorken\Import\Contracts\Enums\IdentifierTypes;
use Smorken\Import\Contracts\Enums\TargetUpdateTypes;
use Smorken\Import\Contracts\HasImportMapper;
use Smorken\Import\Contracts\Models\Data;
use Smorken\Import\Contracts\Models\Source;
use Smorken\Import\Contracts\Models\Target;
use Smorken\Import\Contracts\PartsFactory;
use Smorken\Import\Contracts\Results;
use Smorken\Import\Events\ImportResults;
use Smorken\Import\Traits\HasPartsFactory;
use Smorken\Support\Contracts\Filter;

class Importer implements \Smorken\Import\Contracts\Importer
{
    use HasPartsFactory;

    protected bool $failureMessages = true;

    protected bool $sampling = false;

    /**
     * @var array<string, string>
     */
    protected array $updateAttributes = [];

    public function __construct(
        PartsFactory $partsFactory
    ) {
        $this->setFactory($partsFactory);
        $this->getFactory()->getResults()->setBase(class_basename($this));
    }

    public function cleanup(Filter $filter, string $key = 'deleted'): Results
    {
        $filter = $this->modifyFilterForCleanup($filter);
        if (! $this->sampling) {
            $this->increment($key, $this->getFactory()->getTargetProvider()->cleanup($filter));
        }

        return $this->getFactory()->getResults();
    }

    public function dispatchResults(): void
    {
        Event::dispatch(new ImportResults($this->getFactory()->getResults()));
    }

    /**
     * @param  \Illuminate\Support\Collection<Source>  $models
     */
    public function handle(Collection $models): void
    {
        $this->preHandle($models);
        $modelled = $this->doHandle($models);
        $this->postHandle($modelled);
        $modelled = null;
    }

    public function import(Filter $filter, int $perChunk = 100): Results
    {
        $this->sampling = false;
        $filter = $this->modifyFilterForImport($filter);
        $this->preImport();
        $this->getFactory()->getSourceProvider()->forImport($filter, $this->handle(...), $perChunk);
        $this->cleanup($filter);
        $this->postImport();

        return $this->getFactory()->getResults();
    }

    public function sample(Filter $filter, int $count = 10): \Smorken\Import\Contracts\Modelled
    {
        $this->sampling = true;
        $modelled = $this->getFactory()->getDataAndTargetModeller()->getModelled();
        $this->getFactory()->getResults()->init();
        $this->getFactory()->getSourceProvider()->forImport($filter, function (Collection $models) use (&$modelled) {
            $modelled = $this->doHandle($models);

            return false;
        }, $count);
        $this->getFactory()->getResults()->stop();

        return $modelled;
    }

    protected function collectInitialSourceModels(Collection $models): Collection
    {
        return $models;
    }

    /**
     * @return \Illuminate\Support\Collection<\Smorken\Import\Contracts\Models\Target>
     */
    protected function create(): Collection
    {
        $needed = $this->getFactory()->getDataAndTargetModeller()->getTargetsToCreate();

        return $this->getFactory()->getTargetProvider()->createMany($needed);
    }

    /**
     * @param  \Illuminate\Support\Collection<Source>  $models
     */
    protected function doHandle(Collection $models): \Smorken\Import\Contracts\Modelled
    {
        $models = $this->collectInitialSourceModels($models);
        $this->incrementFromCountable('total', $models);
        $this->getFactory()->getDataAndTargetModeller()->init();
        $modelled = $this->getFactory()->getDataAndTargetModeller()->fromSourceModels($models);
        $this->incrementFromCountable('validated', $modelled->getDataModels());
        $this->incrementFromCountable('existing', $modelled->getTargetModels());
        $this->handleFailureMessages($modelled);
        if (! $this->sampling) {
            $created = $this->create();
            $this->incrementFromCountable('created', $created);
            $this->getFactory()->getDataAndTargetModeller()->addTargetModelsToMapId($created);
            $results = $this->update($modelled->getDataModels(), $modelled->getTargetModels());
            foreach ($results as $key => $count) {
                $this->increment($key, $count);
            }
            $this->handleImportMapper();
        }

        return $modelled;
    }

    /**
     * @return array<string, string>
     */
    protected function getUpdateAttributes(): array
    {
        return $this->updateAttributes;
    }

    protected function handleFailureMessages(\Smorken\Import\Contracts\Modelled $modelled): void
    {
        if ($this->failureMessages) {
            foreach ($modelled->getFailures() as $type => $models) {
                foreach ($models as $model) {
                    $this->getFactory()->getResults()->addMessage(ucfirst($type).'Error', sprintf(
                        '[%s] %s',
                        $model->getIdentifiers()->asString(IdentifierTypes::SOURCE),
                        (string) $model
                    ));
                }
            }
        }
    }

    protected function handleImportMapper(): void
    {
        if ($this->getFactory()->getDataAndTargetModeller() instanceof HasImportMapper) {
            $this->getFactory()->getDataAndTargetModeller()->storeToImportMapper();
        }
    }

    protected function handleMissingTargets(): void
    {
        foreach ($this->getFactory()->getLookups()->get() as $sourceId => $targetId) {
            if (! $targetId) {
                $this->getFactory()->getResults()->addMessage('MissingTarget', 'Source ID: '.$sourceId);
            }
        }
    }

    protected function increment(string $key, int $count): void
    {
        $this->getFactory()->getResults()->increment($key, $count);
    }

    protected function incrementFromCountable(string $key, \Countable $items): void
    {
        $this->increment($key, count($items));
    }

    protected function modifyFilterForCleanup(Filter $filter): Filter
    {
        if (! $filter->getAttribute('cleanupBefore')) {
            $filter->setAttribute('cleanupBefore', Carbon::now()->subDays(14));
        }

        return $filter;
    }

    protected function modifyFilterForImport(Filter $filter): Filter
    {
        return $filter;
    }

    protected function postHandle(\Smorken\Import\Contracts\Modelled $modelled): void
    {
        $this->getFactory()->getDataAndTargetModeller()->reset();
    }

    protected function postImport(): void
    {
        $this->handleMissingTargets();
        $this->getFactory()->getResults()->stop();
        $this->dispatchResults();
    }

    protected function preHandle(Collection $models): void
    {
        // override if needed
    }

    protected function preImport(): void
    {
        $this->getFactory()->getResults()->init();
    }

    /**
     * @param  \Illuminate\Support\Collection<string, Data>  $dataModels
     * @param  \Illuminate\Support\Collection<\Smorken\Import\Contracts\Models\Target>  $existingModels
     * @return \Illuminate\Support\Collection<string, \Smorken\Import\Contracts\Models\Target>
     */
    protected function setUpdateTypesOnExistingModels(Collection $dataModels, Collection $existingModels): Collection
    {
        $coll = new Collection([
            TargetUpdateTypes::TOUCH => new Collection(), TargetUpdateTypes::FULL => new Collection(),
        ]);
        foreach ($existingModels as $sourceIdentifier => $targetModel) {
            $dataModel = $dataModels->get($sourceIdentifier);
            if ($dataModel) {
                $targetModel->fromDataModel($dataModel, $this->getUpdateAttributes());
                $coll[$targetModel->getUpdateType()]->push($targetModel);
            }
        }

        return $coll;
    }

    /**
     * @param  \Illuminate\Support\Collection<string, Data>  $dataModels
     * @param  \Illuminate\Support\Collection<Target>  $existingModels
     * @return array<string, int>
     */
    #[ArrayShape(['touched' => 'int', 'updated' => 'int'])]
    protected function update(Collection $dataModels, Collection $existingModels): array
    {
        $results = [];
        $needsUpdate = $this->setUpdateTypesOnExistingModels($dataModels, $existingModels);
        $results['updated'] = $this->getFactory()
            ->getTargetProvider()
            ->updateMany($needsUpdate->get(TargetUpdateTypes::FULL,
                new Collection()));
        $results['touched'] = $this->getFactory()
            ->getTargetProvider()
            ->touchMany($needsUpdate->get(TargetUpdateTypes::TOUCH,
                new Collection()));

        return $results;
    }
}
