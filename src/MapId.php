<?php

namespace Smorken\Import;

use Illuminate\Support\Collection;
use JetBrains\PhpStorm\ExpectedValues;
use JetBrains\PhpStorm\Pure;
use Smorken\Import\Contracts\Enums\IdentifierTypes;
use Smorken\Import\Contracts\Enums\MapIdTypes;
use Smorken\Import\Contracts\Models\Data;
use Smorken\Import\Contracts\Models\Target;

class MapId implements \Smorken\Import\Contracts\MapId
{
    public function __construct(protected array $items = [])
    {
    }

    public function fromDataCollection(Collection $models): void
    {
        foreach ($models as $model) {
            $this->fromDataModel($model);
        }
    }

    public function fromTargetCollection(Collection $models): void
    {
        foreach ($models as $model) {
            $this->fromTargetModel($model);
        }
    }

    public function get(string $sourceIdentifier): string|int|null
    {
        return $this->offsetGet($sourceIdentifier);
    }

    public function getIdArray(#[ExpectedValues(valuesFromClass: MapIdTypes::class)] string $type): array
    {
        if ($type === MapIdTypes::SOURCE) {
            return array_keys($this->items);
        }

        return array_filter(array_values($this->items));
    }

    /**
     * {@inheritDoc}
     */
    public function getIterator(): \Traversable
    {
        return yield from $this->items;
    }

    public function getSourceIdentifier(string $targetIdentifier): string|int|null
    {
        foreach ($this->items as $source => $target) {
            if (! is_null($target) && (string) $target === $targetIdentifier) {
                return $source;
            }
        }

        return null;
    }

    public function has(string $sourceIdentifier, bool $ignoreNull = true): bool
    {
        if ($ignoreNull) {
            return $this->offsetExists($sourceIdentifier);
        }

        return array_key_exists($sourceIdentifier, $this->items);
    }

    public function isEmpty(): bool
    {
        return empty($this->items);
    }

    #[Pure]
    public function newInstance(): static
    {
        return new static();
    }

    /**
     * {@inheritDoc}
     */
    public function offsetExists(mixed $offset): bool
    {
        return isset($this->items[$offset]);
    }

    /**
     * {@inheritDoc}
     */
    public function offsetGet(mixed $offset): mixed
    {
        return $this->items[$offset] ?? null;
    }

    /**
     * {@inheritDoc}
     */
    public function offsetSet(mixed $offset, mixed $value): void
    {
        $this->items[$offset] = $value;
    }

    /**
     * {@inheritDoc}
     */
    public function offsetUnset(mixed $offset): void
    {
        unset($this->items[$offset]);
    }

    public function set(string $sourceIdentifier, string|int|null $targetIdentifier): void
    {
        if ($targetIdentifier || (is_null($targetIdentifier) && ! $this->has($sourceIdentifier))) {
            $this->offsetSet($sourceIdentifier, $targetIdentifier);
        }
    }

    protected function fromDataModel(Data $model): void
    {
        $this->set($model->getIdentifiers()->asString(IdentifierTypes::SOURCE), null);
    }

    protected function fromTargetModel(Target $model): void
    {
        $this->set($model->getIdentifiers()->asString(IdentifierTypes::SOURCE),
            $model->getIdentifiers()->asString(IdentifierTypes::TARGET));
    }
}
