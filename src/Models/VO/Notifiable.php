<?php

namespace Smorken\Import\Models\VO;

use Smorken\Model\VO;

class Notifiable extends VO implements \Smorken\Import\Contracts\Models\Notifiable
{
    use \Illuminate\Notifications\Notifiable;
}
