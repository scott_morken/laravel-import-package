<?php

namespace Smorken\Import\Models\Target;

use Smorken\Import\Contracts\Models\Target;
use Smorken\Import\Models\Traits\HasIdentifiers;
use Smorken\Import\Models\Traits\HasTarget;

abstract class Eloquent extends \Smorken\Model\Eloquent implements Target
{
    use HasIdentifiers, HasTarget;

    protected array $sourceIdentifierAttributes = ['external_id'];

    protected array $targetIdentifierAttributes = ['id'];
}
