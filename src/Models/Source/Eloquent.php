<?php

namespace Smorken\Import\Models\Source;

use Smorken\Import\Contracts\Models\Source;

abstract class Eloquent extends \Smorken\Model\Eloquent implements Source
{
}
