<?php

namespace Smorken\Import\Models\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Smorken\Import\Contracts\Enums\IdentifierTypes;
use Smorken\Import\Identifiers;

trait HasIdentifiers
{
    protected static string $identifiersClass = Identifiers::class;

    protected array $fillableIdentifiers = ['source_identifier', 'target_identifier'];

    protected ?\Smorken\Import\Contracts\Identifiers $identifiers = null;

    protected bool $mergedFillable = false;

    public static function setIdentifiersClass(string $identifierClass): void
    {
        self::$identifiersClass = $identifierClass;
    }

    /**
     * Get the fillable attributes for the model.
     *
     * @return array
     */
    public function getFillable()
    {
        if (! $this->mergedFillable) {
            $this->fillable(array_unique(array_merge($this->fillable, $this->fillableIdentifiers)));
            $this->mergedFillable = true;
        }

        return parent::getFillable();
    }

    public function getIdentifiers(): \Smorken\Import\Contracts\Identifiers
    {
        if (! $this->identifiers) {
            $cls = self::$identifiersClass;
            $this->identifiers = new $cls($this, $this->getSourceIdentifierAttributes(),
                $this->getTargetIdentifierAttributes());
        }

        return $this->identifiers;
    }

    public function getSourceIdentifierAttribute(): array
    {
        return $this->getIdentifiers()->get(IdentifierTypes::SOURCE);
    }

    /**
     * @return string[]
     */
    public function getSourceIdentifierAttributes(): array
    {
        return property_exists($this,
            'sourceIdentifierAttributes') ? $this->sourceIdentifierAttributes : ['external_id'];
    }

    public function getTargetIdentifierAttribute(): array
    {
        return $this->getIdentifiers()->get(IdentifierTypes::TARGET);
    }

    /**
     * @return string[]
     */
    public function getTargetIdentifierAttributes(): array
    {
        return property_exists($this, 'targetIdentifierAttributes') ? $this->targetIdentifierAttributes : ['id'];
    }

    public function scopeQueryEmpty(Builder $query): Builder
    {
        return $query->where(DB::raw('1=0'));
    }

    /**
     * @param  \Smorken\Import\Contracts\Identifiers[]  $identifiers
     */
    public function scopeSourceIdentifierIn(Builder $query, array $identifiers): Builder
    {
        if ($this->sourceIdentifierIsVirtual()) {
            return $this->scopeQueryEmpty($query);
        }
        if ($this->getIdentifiers()->isSingleAttribute(IdentifierTypes::SOURCE)) {
            return $query->whereIn($this->getIdentifiers()->getSingleAttribute(IdentifierTypes::SOURCE),
                array_map(fn (\Smorken\Import\Contracts\Identifiers $identifier) => $identifier->getSingleValue(IdentifierTypes::SOURCE), $identifiers));
        }

        return $query->where(function ($sq) use ($identifiers) {
            foreach ($identifiers as $identifier) {
                $sq->orWhere(function ($ssq) use ($identifier) {
                    $this->addWheresForMultiColumnIdentifier($ssq, IdentifierTypes::SOURCE, $identifier);
                });
            }
        });
    }

    public function scopeSourceIdentifierIs(Builder $query, \Smorken\Import\Contracts\Identifiers $identifier): Builder
    {
        if ($this->sourceIdentifierIsVirtual()) {
            return $this->scopeQueryEmpty($query);
        }
        if ($this->getIdentifiers()->isSingleAttribute(IdentifierTypes::SOURCE)) {
            return $query->where($this->getIdentifiers()->getSingleAttribute(IdentifierTypes::SOURCE), '=',
                $identifier->getSingleValue(IdentifierTypes::SOURCE));
        }

        return $query->where(function ($sq) use ($identifier) {
            $this->addWheresForMultiColumnIdentifier($sq, IdentifierTypes::SOURCE, $identifier);
        });
    }

    /**
     * @param  \Smorken\Import\Contracts\Identifiers[]  $identifiers
     */
    public function scopeTargetIdentifierIn(Builder $query, array $identifiers): Builder
    {
        if ($this->getIdentifiers()->isSingleAttribute(IdentifierTypes::TARGET)) {
            return $query->whereIn($this->getIdentifiers()->getSingleAttribute(IdentifierTypes::TARGET),
                array_map(fn (\Smorken\Import\Contracts\Identifiers $identifier) => $identifier->getSingleValue(IdentifierTypes::TARGET), $identifiers));
        }

        return $query->where(function ($sq) use ($identifiers) {
            foreach ($identifiers as $identifier) {
                $sq->orWhere(function ($ssq) use ($identifier) {
                    $this->addWheresForMultiColumnIdentifier($ssq, IdentifierTypes::TARGET, $identifier);
                });
            }
        });
    }

    public function scopeTargetIdentifierIs(Builder $query, \Smorken\Import\Contracts\Identifiers $identifier): Builder
    {
        if ($this->getIdentifiers()->isSingleAttribute(IdentifierTypes::TARGET)) {
            return $query->where($this->getIdentifiers()->getSingleAttribute(IdentifierTypes::TARGET), '=',
                $identifier->getSingleValue(IdentifierTypes::TARGET));
        }

        return $query->where(function ($sq) use ($identifier) {
            $this->addWheresForMultiColumnIdentifier($sq, IdentifierTypes::TARGET, $identifier);
        });
    }

    public function setSourceIdentifierAttribute(array|string $value): void
    {
        $this->getIdentifiers()->set(IdentifierTypes::SOURCE, $value, $this->sourceIdentifierIsVirtual());
    }

    public function setTargetIdentifierAttribute(array|string $value): void
    {
        $this->getIdentifiers()->set(IdentifierTypes::TARGET, $value, false);
    }

    protected function addWheresForArray(Builder $query, array $attributes): void
    {
        foreach ($attributes as $col => $val) {
            $query->where($col, $val);
        }
    }

    protected function addWheresForMultiColumnIdentifier(
        Builder $query,
        string $type,
        \Smorken\Import\Contracts\Identifiers $identifiers
    ): void {
        $attributes = [];
        $mapped = $this->mapIdentifierAttributes($type, $identifiers);
        $values = $identifiers->get($type);
        foreach ($values as $otherColumn => $value) {
            $selfColumn = $mapped[$otherColumn];
            if ($selfColumn) {
                $attributes[$selfColumn] = $value;
            }
        }
        $this->addWheresForArray($query, $attributes);
    }

    protected function mapIdentifierAttributes(string $type, \Smorken\Import\Contracts\Identifiers $identifiers): array
    {
        $map = [];
        $other = $identifiers->getAttributes($type);
        $self = $identifiers->getAttributes($type);
        foreach ($other as $i => $k) {
            $map[$k] = $self[$i];
        }

        return $map;
    }

    protected function sourceIdentifierIsVirtual(): bool
    {
        return property_exists($this, 'sourceIdentifierIsVirtual') ? $this->sourceIdentifierIsVirtual : false;
    }
}
