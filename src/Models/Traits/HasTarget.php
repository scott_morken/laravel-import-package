<?php

namespace Smorken\Import\Models\Traits;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use JetBrains\PhpStorm\ExpectedValues;
use Smorken\Import\Contracts\Enums\TargetUpdateTypes;
use Smorken\Import\Contracts\Models\Data;

trait HasTarget
{
    #[ExpectedValues(valuesFromClass: TargetUpdateTypes::class)]
    protected string $updateType = TargetUpdateTypes::UNDEFINED;

    protected array $updatedAttributes = [];

    public function fromDataModel(Data $dataModel, array $attributes = []): static
    {
        if (! $this->isUpdating()) {
            return $this->newFromDataModel($dataModel, $attributes);
        }
        $this->updateFromDataModel($dataModel, $attributes);

        return $this;
    }

    #[ExpectedValues(valuesFromClass: TargetUpdateTypes::class)]
    public function getUpdateType(): string
    {
        return $this->updateType;
    }

    protected function setUpdateType(#[ExpectedValues(valuesFromClass: TargetUpdateTypes::class)] string $type): void
    {
        $this->updateType = $type;
    }

    /**
     * @return array<string, mixed>
     */
    public function getUpdatedAttributes(): array
    {
        return $this->updatedAttributes;
    }

    public function scopeIdIn(Builder $query, array $ids): Builder
    {
        return $query->whereIn($this->getKeyName(), $ids);
    }

    protected function compareAttributes(string $attribute, mixed $dataModelValue): bool
    {
        $val = $this->getAttribute($attribute);
        if (is_int($val)) {
            return $val === (int) $dataModelValue;
        }
        if (is_bool($val)) {
            return $val === (bool) $dataModelValue;
        }
        if (is_int($dataModelValue)) {
            return (int) $val === $dataModelValue;
        }
        if (is_bool($dataModelValue)) {
            return (bool) $val === $dataModelValue;
        }
        if ($val instanceof Carbon || $dataModelValue instanceof Carbon) {
            return $this->compareCarbonDate($val, $dataModelValue);
        }

        return $val === $dataModelValue;
    }

    protected function compareCarbonDate(string|Carbon $targetModelValue, string|Carbon $dataModelValue): bool
    {
        if (is_string($targetModelValue)) {
            $targetModelValue = $this->tryConvertToCarbon($targetModelValue);
        }
        if (is_string($dataModelValue)) {
            $dataModelValue = $this->tryConvertToCarbon($dataModelValue);
        }
        if ($targetModelValue instanceof Carbon && $dataModelValue instanceof Carbon) {
            return $targetModelValue->setMilliseconds(0)->equalTo($dataModelValue->setMilliseconds(0));
        }

        return $targetModelValue === $dataModelValue;
    }

    protected function getAttributeFromDataModel(string $dataModelAttribute, Data $dataModel): mixed
    {
        return $dataModel->getAttribute($dataModelAttribute) ?? $dataModel->getSourceModel()->getAttribute($dataModelAttribute);
    }

    protected function getAttributesToUse(array $dataModelAttributes, array $attributes): array
    {
        if (count($attributes) > 0) {
            return $attributes;
        }
        $attrs = [];
        foreach ($dataModelAttributes as $col) {
            if (in_array($col, $this->fillable) || $this->shouldForceFillNewModel()) {
                $attrs[$col] = $col;
            }
        }

        return $attrs;
    }

    protected function isUpdating(): bool
    {
        return $this->exists || $this->getKey();
    }

    protected function newFromDataModel(Data $dataModel, array $attributes = []): static
    {
        $sourceIdentifiers = $dataModel->getIdentifiers()->getSourceIdentifiers();
        $data = [];
        $data['source_identifier'] = $sourceIdentifiers;
        $attributes = $this->getAttributesToUse($dataModel->getCreateAttributes(), $attributes);
        foreach ($attributes as $targetAttribute => $dataModelAttribute) {
            if (! isset($data[$targetAttribute])) {
                $data[$targetAttribute] = $dataModel->getAttribute($dataModelAttribute) ?? $dataModel->getSourceModel()
                    ->getAttribute($dataModelAttribute);
            }
        }
        if ($this->shouldForceFillNewModel()) {
            return (new static())->forceFill($data);
        }

        return new static($data);
    }

    protected function shouldForceFillNewModel(): bool
    {
        return property_exists($this, 'forceFillNewModel') ? $this->forceFillNewModel : false;
    }

    protected function tryConvertToCarbon(string $dateValue): Carbon|string
    {
        try {
            return Carbon::parse($dateValue);
        } catch (\Throwable) {
            return $dateValue;
        }
    }

    protected function updateAttribute(
        string $targetModelAttribute,
        mixed $dataModelValue
    ): void {
        $this->setUpdateType(TargetUpdateTypes::FULL);
        $this->updatedAttributes[$targetModelAttribute] = $dataModelValue;
        $this->setAttribute($targetModelAttribute, $dataModelValue);
    }

    /**
     * @param  array<string, string>  $attributes
     */
    protected function updateAttributes(array $attributes, Data $dataModel): void
    {
        foreach ($attributes as $targetModelAttribute => $dataModelAttribute) {
            $dataModelValue = $this->getAttributeFromDataModel($dataModelAttribute, $dataModel);
            if (! $this->compareAttributes($targetModelAttribute, $dataModelValue)) {
                $this->updateAttribute($targetModelAttribute, $dataModelValue);
            }
        }
    }

    /**
     * @param  array<string, string>  $attributes
     */
    protected function updateFromDataModel(Data $dataModel, array $attributes = []): void
    {
        $this->setUpdateType(TargetUpdateTypes::TOUCH);
        $attributes = $this->getAttributesToUse($dataModel->getUpdateAttributes(), $attributes);
        $this->updateAttributes($attributes, $dataModel);
        $this->source_identifier = $dataModel->getIdentifiers()->getSourceIdentifiers();
    }
}
