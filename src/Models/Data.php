<?php

namespace Smorken\Import\Models;

use Illuminate\Support\Collection;
use Smorken\Import\Models\Traits\HasIdentifiers;
use Smorken\Model\VO;

abstract class Data extends VO implements \Smorken\Import\Contracts\Models\Data
{
    use HasIdentifiers;

    protected array $requiredAttributes = [];

    protected array $sourceIdentifierAttributes = ['source_id'];

    protected mixed $sourceModel = null;

    protected array $targetIdentifierAttributes = ['target_id'];

    protected bool $wantsDTO = false;

    abstract public function conversions(): array;

    public function fromCollection(Collection $models): Collection
    {
        $returns = new Collection();
        foreach ($models as $model) {
            if ($this->shouldCreateFrom($model)) {
                $returns->push($this->fromModel($model));
            }
        }

        return $returns;
    }

    public function fromModel(mixed $model): \Smorken\Import\Contracts\Models\Data
    {
        $m = $this->newInstance();
        $m->setSourceModel($model);

        return $m;
    }

    public function getAttribute($key)
    {
        $v = parent::getAttribute($key);
        if (! is_null($v)) {
            return $v;
        }

        return $this->getSourceAttributeByConvertedKey($key);
    }

    public function getCreateAttributes(): array
    {
        return array_keys($this->getConversion());
    }

    public function getSourceModel(): mixed
    {
        return $this->sourceModel;
    }

    public function setSourceModel(mixed $model): void
    {
        if (is_array($model)) {
            $model = new VO($model);
        }
        $this->sourceModel = $model;
        $this->addAttributesFrom($model);
    }

    public function getUpdateAttributes(): array
    {
        return array_keys($this->getConversion());
    }

    public function isValid(): bool
    {
        if (count($this->requiredAttributes)) {
            return $this->hasAttributesWithValues($this->requiredAttributes);
        }

        return $this->hasAttributesWithValues($this->getSourceIdentifierAttributes());
    }

    public function shouldCreateFrom(mixed $model): bool
    {
        return true;
    }

    protected function addAttributesFrom(mixed $model): void
    {
        $sourceAttributes = $this->getAttributesFromSourceModel($model);
        foreach ($this->getConversion() as $convertedAttribute => $sourceAttribute) {
            if ($sourceAttribute instanceof \Closure) {
                $this->setAttribute($convertedAttribute, $sourceAttribute());
            } else {
                $this->setAttribute($convertedAttribute, $sourceAttributes[$sourceAttribute] ?? null);
            }
        }
    }

    protected function getAttributesFromSourceModel(mixed $model): array
    {
        if (is_object($model)) {
            // DataTransferObject
            if ($this->wantsDTO && method_exists($model, 'getData')) {
                return $model->getData()->toArray();
            }
            if (method_exists($model, 'attributesToArray')) {
                return $model->attributesToArray();
            }
            if (method_exists($model, 'toArray')) {
                return $model->toArray();
            }
            if (method_exists($model, 'getAttributes')) {
                return $model->getAttributes();
            }
        }

        return (array) $model;
    }

    protected function getConversion(): array
    {
        return $this->conversions()['default'] ?? $this->conversions();
    }

    protected function getSourceAttributeByConvertedKey(string $key): ?string
    {
        foreach ($this->getConversion() as $convertedAttribute => $sourceAttribute) {
            if ($convertedAttribute === $key) {
                if ($sourceAttribute instanceof \Closure) {
                    return $sourceAttribute();
                }

                return $this->attributes[$sourceAttribute] ?? null;
            }
        }

        return null;
    }

    protected function hasAttributesWithValues(array $attributes): bool
    {
        foreach ($attributes as $attr) {
            if (strlen((string) $this->getAttribute($attr)) === 0) {
                return false;
            }
        }

        return true;
    }
}
