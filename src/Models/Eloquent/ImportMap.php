<?php

namespace Smorken\Import\Models\Eloquent;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Smorken\Import\Contracts\Enums\IdentifierTypes;
use Smorken\Import\Contracts\Identifiers;
use Smorken\Import\Models\Traits\HasIdentifiers;
use Smorken\Model\Eloquent;

class ImportMap extends Eloquent implements \Smorken\Import\Contracts\Models\ImportMap
{
    use HasFactory, HasIdentifiers;

    protected $fillable = ['importer', 'source_id', 'target_id'];

    protected array $rules = [
        'importer' => 'required|string',
        'source_id' => 'required',
        'target_id' => 'required',
    ];

    protected array $sourceIdentifierAttributes = ['source_id'];

    protected array $targetIdentifierAttributes = ['target_id'];

    public function fromIdentifiers(string $importer, Identifiers $identifiers): static
    {
        return $this->newInstance([
            'importer' => $importer,
            'source_id' => $identifiers->asString(IdentifierTypes::SOURCE),
            'target_id' => $identifiers->asString(IdentifierTypes::TARGET),
        ]);
    }

    public function scopeImporterIs(Builder $query, string $importer): Builder
    {
        return $query->where('importer', '=', $importer);
    }

    public function scopeOrderByImporter(Builder $query): Builder
    {
        return $query->orderBy('importer');
    }

    public function scopeOrderBySourceId(Builder $query): Builder
    {
        return $query->orderBy('source_id');
    }

    public function scopeOrderByTargetId(Builder $query): Builder
    {
        return $query->orderBy('target_id');
    }

    public function scopeSourceIdIn(Builder $query, array $sourceIds): Builder
    {
        return $query->whereIn('source_id', $sourceIds);
    }

    public function scopeSourceIdIs(Builder $query, string $sourceId): Builder
    {
        return $query->where('source_id', '=', $sourceId);
    }

    public function scopeTargetIdIn(Builder $query, array $targetIds): Builder
    {
        return $query->whereIn('target_id', $targetIds);
    }

    public function scopeTargetIdIs(Builder $query, string $targetId): Builder
    {
        return $query->where('target_id', '=', $targetId);
    }
}
