<?php

namespace Smorken\Import\Models\Eloquent;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Model\Eloquent;

class ImportResult extends Eloquent implements \Smorken\Import\Contracts\Models\ImportResult
{
    protected $casts = [
        'data' => 'array',
    ];

    protected $fillable = ['importer', 'data'];

    public function scopeDefaultOrder(Builder $query): Builder
    {
        return $query->orderBy('created_at', 'desc');
    }

    public function scopeDefaultWiths(Builder $query): Builder
    {
        return $query;
    }

    public function scopeImporterIs(Builder $query, string $importer): Builder
    {
        return $query->where('importer', '=', $importer);
    }

    public function scopeCreatedBefore(Builder $query, Carbon $carbon): Builder
    {
        return $query->whereDate('created_at', '<=', $carbon);
    }

    public function scopeCreatedAfter(Builder $query, Carbon $carbon): Builder
    {
        return $query->whereDate('created_at', '>=', $carbon);
    }
}
