<?php

namespace Smorken\Import;

use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\ExpectedValues;
use Smorken\Import\Contracts\DataAndTargetModeller;
use Smorken\Import\Contracts\Enums\PartsFactoryParts;
use Smorken\Import\Contracts\Models\Data;
use Smorken\Import\Contracts\Storage\HasSource;
use Smorken\Import\Contracts\Storage\HasTarget;

class PartsFactory implements \Smorken\Import\Contracts\PartsFactory
{
    public const PARTS_SHAPE = [
        PartsFactoryParts::DATA_MODEL => Data::class,
        PartsFactoryParts::SOURCE_PROVIDER => HasSource::class,
        PartsFactoryParts::TARGET_PROVIDER => HasTarget::class,
        PartsFactoryParts::RESULTS => \Smorken\Import\Contracts\Results::class,
        PartsFactoryParts::LOOKUPS => \Smorken\Import\Contracts\Lookups::class,
        PartsFactoryParts::DATA_TARGET_MODELLER => DataAndTargetModeller::class,
    ];

    #[ArrayShape(self::PARTS_SHAPE)]
    protected array $parts = [];

    public function __construct(#[ArrayShape(self::PARTS_SHAPE)] array $parts = [])
    {
        foreach ($parts as $type => $obj) {
            $this->set($type, $obj);
        }
    }

    public function get(#[ExpectedValues(valuesFromClass: PartsFactoryParts::class)] string $type): mixed
    {
        return $this->parts[$type] ?? null;
    }

    public function getDataAndTargetModeller(): DataAndTargetModeller
    {
        return $this->get(PartsFactoryParts::DATA_TARGET_MODELLER);
    }

    public function getDataModel(): Data
    {
        return $this->get(PartsFactoryParts::DATA_MODEL);
    }

    public function getLookups(): \Smorken\Import\Contracts\Lookups
    {
        $lookups = $this->get(PartsFactoryParts::LOOKUPS);
        if (! $lookups) {
            $lookups = $this->set(PartsFactoryParts::LOOKUPS, new \Smorken\Import\Lookups([
                'default' => new \Smorken\Import\MapId(),
            ]));
        }

        return $lookups;
    }

    public function getResults(): Results
    {
        $results = $this->get(PartsFactoryParts::RESULTS);
        if (! $results) {
            $results = $this->set(PartsFactoryParts::RESULTS, new Results());
        }

        return $results;
    }

    public function getSourceProvider(): HasSource
    {
        return $this->get(PartsFactoryParts::SOURCE_PROVIDER);
    }

    public function getTargetProvider(): HasTarget
    {
        return $this->get(PartsFactoryParts::TARGET_PROVIDER);
    }

    public function set(#[ExpectedValues(valuesFromClass: PartsFactoryParts::class)] string $type, mixed $part): mixed
    {
        $this->parts[$type] = $part;
        if ($this->partWantsPartsFactory($part)) {
            $part->setFactory($this);
        }

        return $part;
    }

    public function setDataAndTargetModeller(DataAndTargetModeller $dataAndTargetModeller): static
    {
        $this->set(PartsFactoryParts::DATA_TARGET_MODELLER, $dataAndTargetModeller);

        return $this;
    }

    public function setDataModel(Data $dataModel): static
    {
        $this->set(PartsFactoryParts::DATA_MODEL, $dataModel);

        return $this;
    }

    public function setLookups(\Smorken\Import\Contracts\Lookups $lookups): static
    {
        $this->set(PartsFactoryParts::LOOKUPS, $lookups);

        return $this;
    }

    public function setResults(\Smorken\Import\Contracts\Results $results): static
    {
        $this->set(PartsFactoryParts::RESULTS, $results);

        return $this;
    }

    public function setSourceProvider(HasSource $sourceProvider): static
    {
        $this->set(PartsFactoryParts::SOURCE_PROVIDER, $sourceProvider);

        return $this;
    }

    public function setTargetProvider(HasTarget $target): static
    {
        $this->set(PartsFactoryParts::TARGET_PROVIDER, $target);

        return $this;
    }

    protected function partWantsPartsFactory(mixed $part): bool
    {
        if (method_exists($part, 'setFactory')) {
            if (method_exists($part, 'wantsPartsFactory')) {
                return $part->wantsPartsFactory();
            }

            return true;
        }

        return false;
    }
}
