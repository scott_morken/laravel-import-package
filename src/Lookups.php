<?php

namespace Smorken\Import;

use Smorken\Import\Contracts\MapId;

class Lookups implements \Smorken\Import\Contracts\Lookups
{
    protected array $mapIds = [];

    protected array $providers = [];

    public function __construct(array $mapIds, array $providers = [])
    {
        $this->setMapIds($mapIds);
        $this->setProviders($providers);
    }

    public function get(string $key = 'default'): MapId
    {
        if (! isset($this->mapIds[$key])) {
            $this->setMapId($key, new \Smorken\Import\MapId());
        }

        return $this->mapIds[$key];
    }

    public function getProvider(string $key): mixed
    {
        return $this->providers[$key] ?? null;
    }

    public function reset(): void
    {
        foreach ($this->mapIds as $key => $mapId) {
            $this->resetMapId($key);
        }
    }

    public function resetMapId(string $key = 'default'): void
    {
        $this->setMapId($key, new \Smorken\Import\MapId());
    }

    public function setMapId(string $key, MapId $mapId): void
    {
        $this->mapIds[$key] = $mapId;
    }

    public function setMapIds(array $mapIds): void
    {
        foreach ($mapIds as $k => $mapId) {
            $this->setMapId($k, $mapId);
        }
    }

    public function setProvider(string $key, mixed $provider): void
    {
        $this->providers[$key] = $provider;
    }

    public function setProviders(array $providers): void
    {
        foreach ($this->providers as $k => $p) {
            $this->setProvider($k, $p);
        }
    }
}
