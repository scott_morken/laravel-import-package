<?php

namespace Smorken\Import\Modellers;

use Illuminate\Support\Collection;
use Smorken\Import\Contracts\Enums\IdentifierTypes;
use Smorken\Import\Contracts\HasImportMapper;
use Smorken\Import\Contracts\ImportMapper;
use Smorken\Import\Contracts\Models\Data;
use Smorken\Import\DataAndTargetModeller;

class ModellerWithImportMapper extends DataAndTargetModeller implements HasImportMapper
{
    protected ?ImportMapper $importMapper = null;

    /**
     * importer class name to use as key in the import map
     */
    protected ?string $importMapperKey = null;

    protected array $importMapperLookups = [];

    protected bool $shouldChunkPreload = false;

    public function addTargetModelsToMapId(Collection $targetModels): void
    {
        parent::addTargetModelsToMapId($targetModels);
        $this->getImportMapper()->setMapIdsForCreatedModels($this->importMapperKey, $targetModels);
    }

    public function getImportMapper(): ImportMapper
    {
        if (is_null($this->importMapper)) {
            $this->setImportMapper(new \Smorken\Import\ImportMapper($this->getFactory()));
        }

        return $this->importMapper;
    }

    public function setImportMapper(ImportMapper $importMapper): void
    {
        $this->importMapper = $importMapper;
    }

    public function storeToImportMapper(): int
    {
        if ($this->importMapperKey) {
            return $this->getImportMapper()->store($this->importMapperKey);
        }

        return 0;
    }

    protected function addTargetIdentifierToDataModel(Data $model): void
    {
        if (! $model->getIdentifiers()->isComplete(IdentifierTypes::TARGET)) {
            $targetId = $this->getImportMapper()
                ->getFactory()
                ->getLookups()
                ->get($this->importMapperKey)
                ->get($model->getIdentifiers()->asString(IdentifierTypes::SOURCE));
            if ($targetId) {
                $model->getIdentifiers()->set(IdentifierTypes::TARGET, $targetId);
            }
        }
    }

    protected function addTargetIdentifierToDataModels(Collection $dataModels): void
    {
        foreach ($dataModels as $dataModel) {
            $this->addTargetIdentifierToDataModel($dataModel);
        }
    }

    protected function addTargetIdentifiersFromImporterMap(Collection $dataModels, Collection $identifiers): void
    {
        if ($this->importMapperKey) {
            $ids = $this->getIdentifiersFromDataModels($dataModels);
            $mapped = $this->getImportMapper()
                ->getImportMap($this->importMapperKey, IdentifierTypes::SOURCE, $ids->keys()->toArray());
            foreach ($mapped as $map) {
                if ($map->target_id) {
                    $identifiers->put($map->target_id, $map->getIdentifiers());
                }
            }
        }
    }

    protected function afterCreateDataModels(Collection $dataModels): void
    {
        if ($this->importMapperKey) {
            $this->setMapIdsFor($this->importMapperKey, $dataModels);
            $this->addTargetIdentifierToDataModels($dataModels);
        }
        $this->handleImportMapperLookups($dataModels);
    }

    protected function applyLookupColumnToDataModel(
        string $importer,
        Data $model,
        string $sourceColumn,
        string $targetColumn
    ): bool {
        $sourceId = $model->getAttribute($sourceColumn) ?? $model->getSourceModel()->getAttribute($sourceColumn);
        if (is_null($sourceId)) {
            return false;
        }
        $targetId = $this->getImportMapper()
            ->getFactory()
            ->getLookups()
            ->get($importer)
            ->get($sourceId);
        if ($targetId) {
            $model->setAttribute($targetColumn, $targetId);

            return true;
        }

        return false;
    }

    protected function applyLookupColumnToDataModels(
        string $importer,
        Collection $dataModels,
        string $sourceColumn,
        string $targetColumn
    ): void {
        foreach ($dataModels as $key => $model) {
            if (! $this->applyLookupColumnToDataModel($importer, $model, $sourceColumn, $targetColumn)) {
                $this->getModelled()->addLookupFailure($model);
                unset($dataModels[$key]);
            }
        }
    }

    protected function applySourceIdentifiersToTargets(Collection $targets, Collection $missing): void
    {
        foreach ($targets as $target) {
            $targetId = $target->getIdentifiers()->asString(IdentifierTypes::TARGET);
            $identifier = $missing->get($targetId);
            $target->getIdentifiers()->set(IdentifierTypes::SOURCE, $identifier->get(IdentifierTypes::SOURCE));
        }
    }

    protected function doInit(): void
    {
        if ($this->importMapperKey) {
            $this->getImportMapper()->preload($this->importMapperKey);
            $this->getImportMapper()->setShouldChunkPreload($this->shouldChunkPreload);
        }
        foreach ($this->importMapperLookups as $key => $data) {
            $this->getImportMapper()->preload($key);
        }
    }

    protected function getTargetIdentifiersFromDataModels(Collection $dataModels): Collection
    {
        $identifiers = new Collection();
        foreach ($dataModels as $dataModel) {
            $identifier = $dataModel->getIdentifiers();
            if ($identifier && $identifier->isComplete(IdentifierTypes::TARGET)) {
                $identifiers->put($identifier->asString(IdentifierTypes::TARGET), $identifier);
            }
        }

        return $identifiers;
    }

    protected function getTargetModelsByDataModels(Collection $dataModels): Collection
    {
        if (! $this->importMapperKey) {
            return parent::getTargetModelsByDataModels($dataModels);
        }

        return $this->getTargetModelsByDataModelsByTarget($dataModels);
    }

    protected function getTargetModelsByDataModelsByTarget(Collection $dataModels): Collection
    {
        $identifiers = $this->getTargetIdentifiersFromDataModels($dataModels);
        $this->addTargetIdentifiersFromImporterMap($dataModels, $identifiers);
        $missing = $this->removeIdentifiersWithTarget($identifiers);
        $targets = $this->getFactory()
            ->getTargetProvider()
            ->getByTargetIdentifiers($missing->toArray());
        $this->applySourceIdentifiersToTargets($targets, $missing);

        return $this->keyTargetModelsBySourceIdentifier($targets);
    }

    protected function handleImportMapperLookups(Collection $dataModels): void
    {
        foreach ($this->importMapperLookups as $importerClass => $lookups) {
            $sourceColumn = $lookups[IdentifierTypes::SOURCE];
            $this->getImportMapper()
                ->setMapIdsForModels($importerClass, IdentifierTypes::SOURCE, $dataModels, $sourceColumn);
            $this->applyLookupColumnToDataModels($importerClass, $dataModels, $sourceColumn,
                $lookups[IdentifierTypes::TARGET]);
        }
    }

    protected function setMapIdsFor(string $importer, Collection $dataModels): void
    {
        $this->getImportMapper()->setMapIdsForModels($importer, IdentifierTypes::SOURCE, $dataModels);
    }
}
