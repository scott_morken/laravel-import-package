<?php

namespace Smorken\Import;

class Importers implements \Smorken\Import\Contracts\Importers
{
    protected array $importers = [];

    public function __construct(array $importers)
    {
        $this->setFromArray($importers);
    }

    public function all(): array
    {
        return $this->importers;
    }

    public function get(string $interface): \Smorken\Import\Contracts\Importer
    {
        return $this->importers[$interface];
    }

    public function set(
        string $interface,
        \Smorken\Import\Contracts\Importer $importer
    ): void {
        $this->importers[$interface] = $importer;
    }

    public function setFromArray(array $importers): void
    {
        foreach ($importers as $interface => $importer) {
            $this->set($interface, $importer);
        }
    }
}
