<?php

namespace Smorken\Import\Traits;

use Smorken\Import\Contracts\PartsFactory;

trait HasPartsFactory
{
    protected ?PartsFactory $partsFactory = null;

    public function getFactory(): PartsFactory
    {
        return $this->partsFactory;
    }

    public function setFactory(PartsFactory $partsFactory): void
    {
        $this->partsFactory = $partsFactory;
    }
}
