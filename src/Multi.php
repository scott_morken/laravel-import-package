<?php

namespace Smorken\Import;

use Illuminate\Support\Collection;
use Smorken\Import\Contracts\Importers;
use Smorken\Import\Contracts\Storage\HasSource;
use Smorken\Support\Contracts\Filter;

class Multi extends AbstractMulti implements \Smorken\Import\Contracts\Multi
{
    public function __construct(Importers $importers, protected HasSource $source, ?string $memoryLimit = null)
    {
        parent::__construct($importers, $memoryLimit);
    }

    public function cleanup(Filter $filter): void
    {
        foreach ($this->getImporters()->all() as $interface => $importer) {
            $importer->cleanup($filter);
        }
    }

    public function getSource(): HasSource
    {
        return $this->source;
    }

    public function setSource(HasSource $source): void
    {
        $this->source = $source;
    }

    protected function collectInitialSourceModels(Collection $models): Collection
    {
        return $models;
    }

    /**
     * This is primarily to handle test cases where the
     * same import runs from DI multiple times
     */
    protected function resetImporters(Collection $importers): void
    {
        /** @var \Smorken\Import\Contracts\Importer $importer */
        foreach ($importers as $importer) {
            $importer->getFactory()->getResults()->resetAll();
            $importer->getFactory()->getLookups()->reset();
        }
    }

    protected function runImport(string $collectionMethod, Filter $filter, array $lookups, int $perChunk): array
    {
        $importers = $this->$collectionMethod();
        if ($importers->count() > 0) {
            $this->resetImporters($importers);
            $this->getSource()->forImport($filter, function (Collection $sourceModels) use (&$lookups, $importers) {
                $sourceModels = $this->collectInitialSourceModels($sourceModels);
                foreach ($importers as $interface => $importer) {
                    $this->commandOutput(sprintf('[%s] chunk starting', $interface));
                    $this->addLookupsToImporter($importer, $lookups);
                    $importer->handle($sourceModels);
                    if (! isset($lookups[$interface])) {
                        $lookups[$interface] = $importer->getFactory()->getLookups()->get();
                    }
                    $this->commandOutput(sprintf('[%s] chunk complete', $interface));
                }
            }, $perChunk);
        }

        return $lookups;
    }
}
