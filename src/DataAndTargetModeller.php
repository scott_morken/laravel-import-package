<?php

namespace Smorken\Import;

use Illuminate\Support\Collection;
use Smorken\Import\Contracts\Enums\IdentifierTypes;
use Smorken\Import\Contracts\Modelled;
use Smorken\Import\Contracts\Models\Data;
use Smorken\Import\Contracts\Models\Source;
use Smorken\Import\Contracts\Models\Target;
use Smorken\Import\Traits\HasPartsFactory;

class DataAndTargetModeller implements \Smorken\Import\Contracts\DataAndTargetModeller
{
    use HasPartsFactory;

    protected static string $modelledClass = \Smorken\Import\Modelled::class;

    protected bool $initialized = false;

    protected ?Modelled $modelled = null;

    public static function setModelledClass(string $modelledClass): void
    {
        self::$modelledClass = $modelledClass;
    }

    public function addTargetModelsToMapId(Collection $targetModels): void
    {
        foreach ($targetModels as $targetModel) {
            $this->addTargetModelToMapId($targetModel);
        }
    }

    public function fromSourceModels(Collection $sourceModels): Modelled
    {
        $this->reset();
        $sourceModels = $this->collectInitialSourceModels($sourceModels);
        if (method_exists($this, 'beforeCreateDataModels')) {
            $this->beforeCreateDataModels($sourceModels);
        }
        $dataModels = $this->createUniqueDataModelCollection($sourceModels);
        if (method_exists($this, 'afterCreateDataModels')) {
            $this->afterCreateDataModels($dataModels);
        }
        $this->validateDataModels($dataModels);
        $this->getModelled()->setDataModels($dataModels);
        $targetModels = $this->getTargetModelsByDataModels($dataModels);
        if (method_exists($this, 'afterCreateTargetModels')) {
            $this->afterCreateTargetModels($targetModels);
        }
        $this->getModelled()->setTargetModels($targetModels);
        $this->addTargetModelsToMapId($targetModels);

        return $this->getModelled();
    }

    public function getModelled(): Modelled
    {
        $cls = self::$modelledClass;
        if (! $this->modelled) {
            $this->modelled = new $cls();
        }

        return $this->modelled;
    }

    public function getTargetsToCreate(): Collection
    {
        $needed = $this->collectMissingDataModels($this->getModelled()->getDataModels());

        return $this->createTargetsFromDataModels($needed);
    }

    public function init(): void
    {
        if (! $this->initialized) {
            $this->doInit();
            $this->initialized = true;
        }
    }

    public function reset(): void
    {
        $this->modelled = null;
    }

    protected function addTargetModelToMapId(Target $model): void
    {
        $this->getFactory()->getLookups()->get()->set($model->getIdentifiers()->asString(IdentifierTypes::SOURCE),
            $model->getIdentifiers()->asString(IdentifierTypes::TARGET));
    }

    protected function collectInitialSourceModels(Collection $sourceModels): Collection
    {
        return $sourceModels;
    }

    protected function collectMissingDataModels(Collection $dataModels): Collection
    {
        return $dataModels->filter(fn ($model, $key) => $this->getFactory()->getLookups()->get()->has($key) === false);
    }

    protected function convertToDataModel(Source $model): Data
    {
        return $this->getFactory()->getDataModel()->fromModel($model);
    }

    protected function createTargetsFromDataModels(Collection $dataModels): Collection
    {
        $mapped = new Collection();
        foreach ($dataModels as $key => $dataModel) {
            $mapped->put($key, $this->getFactory()
                ->getTargetProvider()
                ->getTarget()
                ->newInstance()
                ->fromDataModel($dataModel));
        }
        return $mapped;
    }

    /**
     * @param  \Illuminate\Support\Collection<\Smorken\Model\Contracts\Model>  $models
     * @return \Illuminate\Support\Collection<Data>
     */
    protected function createUniqueDataModelCollection(Collection $models): Collection
    {
        $collected = new Collection();
        foreach ($models as $sourceModel) {
            if (! $this->getFactory()->getDataModel()->shouldCreateFrom($sourceModel)) {
                continue;
            }
            $model = $this->convertToDataModel($sourceModel);
            $model = $this->modifyDataModel($model, $sourceModel);
            $sourceId = $model->getIdentifiers()->asString(IdentifierTypes::SOURCE);
            $isComplete = $model->getIdentifiers()->isComplete(IdentifierTypes::SOURCE);
            if (! $isComplete) {
                $this->getModelled()->addValidationFailure($model);
            }
            if ($isComplete && ! $collected->has($sourceId) && ! $this->getFactory()
                ->getLookups()
                ->get()
                ->has($sourceId, false)) {
                $collected->put($sourceId, $model);
                $this->getFactory()->getLookups()->get()->set($sourceId, null);
            }
        }

        return $collected;
    }

    protected function dataModelIsValid(Data $model): bool
    {
        return $model->isValid();
    }

    protected function doInit(): void
    {
        // override if needed
    }

    protected function getIdentifiersFromDataModels(Collection $dataModels): Collection
    {
        $identifiers = new Collection();
        foreach ($dataModels as $dataModel) {
            $id = $dataModel->getIdentifiers();
            $identifiers->put($id->asString(IdentifierTypes::SOURCE), $id);
        }

        return $identifiers;
    }

    protected function getTargetModelsByDataModels(Collection $dataModels): Collection
    {
        return $this->getTargetModelsByDataModelsBySource($dataModels);
    }

    protected function getTargetModelsByDataModelsBySource(Collection $dataModels): Collection
    {
        $identifiers = $this->getIdentifiersFromDataModels($dataModels);
        $nonExisting = $this->removeIdentifiersWithTarget($identifiers);
        $targets = $this->getFactory()
            ->getTargetProvider()
            ->getBySourceIdentifiers($nonExisting->toArray());

        return $this->keyTargetModelsBySourceIdentifier($targets);
    }

    protected function keyTargetModelsBySourceIdentifier(Collection $targetModels): Collection
    {
        return $targetModels->keyBy(fn (Target $model) => $model->getIdentifiers()
            ->asString(IdentifierTypes::SOURCE) ?: $model->getKey());
    }

    protected function modifyDataModel(Data $model, Source $sourceModel): Data
    {
        return $model;
    }

    protected function removeIdentifiersWithTarget(Collection $identifiers): Collection
    {
        return $identifiers->filter(fn (\Smorken\Import\Contracts\Identifiers $item) => $this->getFactory()
            ->getLookups()
            ->get()
            ->has($item->asString(IdentifierTypes::SOURCE)) === false);
    }

    protected function validateDataModels(Collection $dataModels): void
    {
        foreach ($dataModels as $key => $model) {
            if (! $this->dataModelIsValid($model)) {
                $this->getModelled()->addValidationFailure($model);
                $sourceId = $model->getIdentifiers()->asString(IdentifierTypes::SOURCE);
                unset($dataModels[$key]);
                if ($this->getFactory()->getLookups()->get()->get($sourceId) === null) {
                    $this->getFactory()->getLookups()->get()->offsetUnset($sourceId);
                }
            }
        }
    }
}
