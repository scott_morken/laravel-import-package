<?php

namespace Smorken\Import\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Smorken\Import\Contracts\Notifications\Notify;
use Smorken\Import\Events\ImportResults;

class SendNotifications implements ShouldQueue
{
    public function __construct(protected Notify $notify)
    {
    }

    public function handle(ImportResults $importResults): void
    {
        $this->notify->notify($importResults->results);
    }
}
