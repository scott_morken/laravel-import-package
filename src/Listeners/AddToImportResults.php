<?php

namespace Smorken\Import\Listeners;

use Smorken\Import\Contracts\Storage\ImportResult;
use Smorken\Import\Events\ImportResults;

class AddToImportResults
{
    public function __construct(protected ImportResult $provider)
    {
    }

    public function handle(ImportResults $importResults): void
    {
        $this->provider->create([
            'importer' => $importResults->results->getName(),
            'data' => $importResults->results->toArray(),
        ]);
    }
}
