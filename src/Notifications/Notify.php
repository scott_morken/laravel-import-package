<?php

namespace Smorken\Import\Notifications;

use Smorken\Import\Contracts\Models\Notifiable;
use Smorken\Import\Contracts\Results;

class Notify implements \Smorken\Import\Contracts\Notifications\Notify
{
    public function __construct(protected Notifiable $notifiable)
    {
    }

    public function getNotifiableModel(): Notifiable
    {
        return $this->notifiable;
    }

    public function notify(Results $results): void
    {
        $this->getNotifiableModel()->notify(new ImportResults($results));
    }
}
