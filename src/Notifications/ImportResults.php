<?php

namespace Smorken\Import\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Smorken\Import\Contracts\Results;

class ImportResults extends Notification
{
    public function __construct(protected Results $results)
    {
    }

    public function toMail(mixed $notifiable): MailMessage
    {
        $message = new MailMessage();
        $lines = $this->flatten($this->results->toArray());
        $message->subject($this->getSubject())
            ->greeting($this->getSubject());
        foreach ($lines as $line) {
            $message->line($line);
        }

        return $message;
    }

    public function via(mixed $notifiable): array
    {
        return ['mail'];
    }

    protected function flatten(array $arr, ?string $parentKey = null): array
    {
        if (! is_array($arr)) {
            return [];
        }
        $result = [];
        foreach ($arr as $key => $value) {
            if ($parentKey) {
                $key = sprintf('%s.%s', $parentKey, $key);
            }
            if (is_array($value)) {
                $result = array_merge($result, $this->flatten($value, $key));
            } else {
                $result[] = sprintf('%s: %s', ucfirst($key), $value);
            }
        }

        return $result;
    }

    protected function getSubject(): string
    {
        return sprintf('Import results for %s', $this->results->getName());
    }
}
