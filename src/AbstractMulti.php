<?php

namespace Smorken\Import;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Event;
use Smorken\Import\Contracts\Importer;
use Smorken\Import\Contracts\Importers;
use Smorken\Import\Contracts\Results;
use Smorken\Import\Events\ImportResults;
use Smorken\Support\Contracts\Filter;

abstract class AbstractMulti
{
    protected ?Command $command = null;

    protected Results $results;

    protected array $runAfter = [];

    public function __construct(protected Importers $importers, ?string $memoryLimit = null)
    {
        $this->results = new \Smorken\Import\Results('Multi');
        if ($memoryLimit) {
            ini_set('memory_limit', $memoryLimit);
        }
    }

    abstract public function cleanup(Filter $filter): void;

    abstract protected function runImport(
        string $collectionMethod,
        Filter $filter,
        array $lookups,
        int $perChunk
    ): array;

    public function commandOutput(string $message): void
    {
        $this->getCommand()?->line($message);
    }

    public function dispatchResults(): void
    {
        Event::dispatch(new ImportResults($this->getResults()));
    }

    public function getCommand(): ?Command
    {
        return $this->command;
    }

    public function setCommand(Command $command): void
    {
        $this->command = $command;
    }

    public function getImporter(string $interface): Importer
    {
        return $this->getImporters()->get($interface);
    }

    public function getImporters(): Importers
    {
        return $this->importers;
    }

    public function getResults(): Results
    {
        return $this->results;
    }

    public function handle(Filter $filter, int $perChunk = 100): Results
    {
        $this->getResults()->init();
        $this->import($filter, $perChunk);
        $this->cleanup($filter);
        $this->mixinResults($this->getResults());
        $this->getResults()->stop();

        return $this->getResults();
    }

    public function import(Filter $filter, int $perChunk = 100): void
    {
        $filter = $this->modifyFilterForImport($filter);
        $lookups = $this->runImport('collectFirstRunImporters', $filter, [], $perChunk);
        $this->runImport('collectPostRunImporters', $filter, $lookups, $perChunk);
    }

    public function setImporters(Importers $importers): void
    {
        $this->importers = $importers;
    }

    protected function addLookupsToImporter(Importer $importer, array $lookups): void
    {
        foreach ($lookups as $interface => $mapId) {
            if (! is_subclass_of($importer, $interface)) {
                $importer->getFactory()->getLookups()->setMapId($interface, $mapId);
            }
        }
    }

    protected function collectFirstRunImporters(): Collection
    {
        $importers = new Collection();
        foreach ($this->getImporters()->all() as $interface => $importer) {
            if (! in_array($interface, $this->runAfter)) {
                $importers->put($interface, $importer);
            }
        }

        return $importers;
    }

    protected function collectPostRunImporters(): Collection
    {
        $importers = new Collection();
        foreach ($this->getImporters()->all() as $interface => $importer) {
            if (in_array($interface, $this->runAfter)) {
                $importers->put($interface, $importer);
            }
        }

        return $importers;
    }

    protected function mixinImporterCounterResults(Results $results, Importer $importer): void
    {
        $results->setCounters($importer->getFactory()->getResults()->getCounters());
    }

    protected function mixinMessageResults(Results $results, Importer $importer): void
    {
        foreach ($importer->getFactory()->getResults()->getMessages() as $key => $messages) {
            foreach ($messages as $message) {
                $results->addMessage($key, $message);
            }
        }
    }

    protected function mixinResults(Results $results): void
    {
        foreach ($this->getImporters()->all() as $importer) {
            $this->mixinImporterCounterResults($results, $importer);
            $this->mixinMessageResults($results, $importer);
        }
    }

    protected function modifyFilterForImport(Filter $filter): Filter
    {
        return $filter;
    }
}
