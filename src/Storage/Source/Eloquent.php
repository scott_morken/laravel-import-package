<?php

namespace Smorken\Import\Storage\Source;

use JetBrains\PhpStorm\Pure;
use Smorken\Import\Contracts\Models\Source;
use Smorken\Import\Contracts\Storage\HasSource;
use Smorken\Support\Contracts\Filter;

abstract class Eloquent extends \Smorken\Storage\Eloquent implements HasSource
{
    public function forImport(Filter $filter, callable $callback, int $count = 100): void
    {
        $q = $this->queryFromFilter($filter);
        $q->chunk($count, $callback);
    }

    #[Pure]
    public function getSource(): Source
    {
        return $this->getModel();
    }
}
