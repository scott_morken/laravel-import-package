<?php

namespace Smorken\Import\Storage\Target;

use Smorken\Import\Contracts\Storage\HasTarget;

class Eloquent extends \Smorken\Storage\Eloquent implements HasTarget
{
    use \Smorken\Import\Storage\Traits\HasTarget;
}
