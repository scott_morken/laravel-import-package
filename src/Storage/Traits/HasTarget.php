<?php

namespace Smorken\Import\Storage\Traits;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use JetBrains\PhpStorm\Pure;
use Smorken\Import\Contracts\Models\Target;
use Smorken\Support\Contracts\Filter;

trait HasTarget
{
    public function cleanup(Filter $filter): int
    {
        if (! $this->canCleanup($filter)) {
            return 0;
        }

        return $this->doCleanup($filter);
    }

    public function createMany(Collection $targetModels): Collection
    {
        $this->saveModels($targetModels);

        return $targetModels;
    }

    public function getBySourceIdentifiers(array $identifiers): Collection
    {
        if (count($identifiers)) {
            return $this->getModel()->newQuery()->sourceIdentifierIn($identifiers)->get();
        }

        return new Collection();
    }

    public function getByTargetIdentifiers(array $identifiers): Collection
    {
        if (count($identifiers)) {
            return $this->getModel()->newQuery()->targetIdentifierIn($identifiers)->get();
        }

        return new Collection();
    }

    #[Pure]
    public function getTarget(): Target
    {
        return $this->getModel();
    }

    public function touchMany(Collection $targetModels): int
    {
        if ($targetModels->count()) {
            return $this->getModel()
                ->newQuery()
                ->idIn($this->getIdsFromTargetModels($targetModels))
                ->update(['updated_at' => Carbon::now()]);
        }

        return 0;
    }

    public function updateMany(Collection $targetModels): int
    {
        return $this->saveModels($targetModels);
    }

    protected function canCleanup(Filter $filter): bool
    {
        return (bool) $filter->getAttribute('cleanupBefore');
    }

    protected function doCleanup(Filter $filter): int
    {
        $q = $this->queryFromFilter($filter);

        return $q->delete();
    }

    protected function filterBefore(Builder $query, $v): Builder
    {
        return $query->whereDate('updated_at', '<', $v);
    }

    protected function getFilterMethods(): array
    {
        return [
            'cleanupBefore' => 'filterBefore',
        ];
    }

    /**
     * @return int[]
     */
    protected function getIdsFromTargetModels(Collection $targetModels): array
    {
        return $targetModels->map(fn ($model) => $model->getKey())->toArray();
    }

    protected function saveModels(Collection $targetModels): int
    {
        $count = 0;
        if ($targetModels->count()) {
            DB::transaction(function () use ($targetModels, &$count) {
                foreach ($targetModels as $targetModel) {
                    if ($targetModel->save()) {
                        $count++;
                    }
                }
            });
        }

        return $count;
    }
}
