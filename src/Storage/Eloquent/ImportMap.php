<?php

namespace Smorken\Import\Storage\Eloquent;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use JetBrains\PhpStorm\ExpectedValues;
use Smorken\Import\Contracts\Enums\IdentifierTypes;

class ImportMap implements \Smorken\Import\Contracts\Storage\ImportMap
{
    public function __construct(protected \Smorken\Import\Contracts\Models\ImportMap $model)
    {
    }

    public function getByImporter(string $importer): Collection
    {
        return $this->getModel()
            ->newQuery()
            ->importerIs($importer)
            ->orderBySourceId()
            ->get();
    }

    public function chunkByImporter(string $importer, callable $callback, int $perPage = 500): void
    {
        $query = $this->getModel()
            ->newQuery()
            ->importerIs($importer)
            ->orderBySourceId();
        $query->chunk($perPage, $callback);
    }

    public function getByImporterAndIdentifiers(
        string $importer,
        #[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $type,
        array $identifiers
    ): Collection {
        if (empty($identifiers)) {
            return new Collection();
        }
        $query = $this->getModel()
            ->newQuery()
            ->importerIs($importer)
            ->orderByImporter()
            ->orderBySourceId();
        if ($type === IdentifierTypes::SOURCE) {
            $query->sourceIdIn($identifiers);
        }
        if ($type === IdentifierTypes::TARGET) {
            $query->targetIdIn($identifiers);
        }

        return $query->get();
    }

    public function getModel(): \Smorken\Import\Contracts\Models\ImportMap
    {
        return $this->model;
    }

    public function saveManyImporterAndArray(string $importer, array $data): int
    {
        return $this->getModel()->upsert(
            $this->createUpsertData($importer, $data),
            ['importer', 'source_id'],
            ['target_id']
        );
    }

    public function saveManyModels(Collection $models): int
    {
        $count = 0;
        if ($models->count()) {
            DB::transaction(function () use ($models, &$count) {
                foreach ($models as $model) {
                    if ($model->save()) {
                        $count++;
                    }
                }
            });
        }

        return $count;
    }

    protected function createUpsertData(string $importer, array $data): array
    {
        $upsert = [];
        foreach ($data as $identifiers) {
            $targetId = $identifiers['target_id'];
            $sourceId = $identifiers['source_id'];
            if ($targetId && $sourceId) {
                $upsert[] = ['importer' => $importer, 'target_id' => $targetId, 'source_id' => $sourceId];
            }
        }

        return $upsert;
    }
}
