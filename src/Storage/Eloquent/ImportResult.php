<?php

namespace Smorken\Import\Storage\Eloquent;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Storage\Eloquent;

class ImportResult extends Eloquent implements \Smorken\Import\Contracts\Storage\ImportResult
{
    protected function filterCreatedAfter(Builder $query, $v): Builder
    {
        if (strlen($v ?? '')) {
            $date = Carbon::parse($v);
            $query->createdAfter($date);
        }

        return $query;
    }

    protected function filterCreatedBefore(Builder $query, $v): Builder
    {
        if (strlen($v ?? '')) {
            $date = Carbon::parse($v);
            $query->createdBefore($date);
        }

        return $query;
    }

    protected function filterImporter(Builder $query, $v): Builder
    {
        if (strlen($v ?? '')) {
            $query->importerIs($v);
        }

        return $query;
    }

    protected function getFilterMethods(): array
    {
        return [
            'f_importer' => 'filterImporter',
            'f_createdAfter' => 'filterCreatedAfter',
            'f_createdBefore' => 'filterCreatedBefore',
        ];
    }
}
