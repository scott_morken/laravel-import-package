<?php

namespace Smorken\Import\Contracts;

use JetBrains\PhpStorm\ExpectedValues;
use Smorken\Import\Contracts\Enums\PartsFactoryParts;
use Smorken\Import\Contracts\Models\Data;
use Smorken\Import\Contracts\Storage\HasSource;
use Smorken\Import\Contracts\Storage\HasTarget;

interface PartsFactory
{
    public function get(#[ExpectedValues(valuesFromClass: PartsFactoryParts::class)] string $type): mixed;

    public function getDataAndTargetModeller(): DataAndTargetModeller;

    public function getDataModel(): Data;

    public function getLookups(): Lookups;

    public function getResults(): Results;

    public function getSourceProvider(): HasSource;

    public function getTargetProvider(): HasTarget;

    public function set(#[ExpectedValues(valuesFromClass: PartsFactoryParts::class)] string $type, mixed $part): mixed;

    public function setDataAndTargetModeller(DataAndTargetModeller $dataAndTargetModeller): static;

    public function setDataModel(Data $dataModel): static;

    public function setLookups(Lookups $lookups): static;

    public function setResults(Results $results): static;

    public function setSourceProvider(HasSource $sourceProvider): static;

    public function setTargetProvider(HasTarget $target): static;
}
