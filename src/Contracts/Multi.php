<?php

namespace Smorken\Import\Contracts;

use Smorken\Import\Contracts\Storage\HasSource;

interface Multi extends MultiImporters
{
    public function getSource(): HasSource;

    public function setSource(HasSource $source): void;
}
