<?php

namespace Smorken\Import\Contracts;

use ArrayAccess;
use Illuminate\Support\Collection;
use IteratorAggregate;
use JetBrains\PhpStorm\ExpectedValues;
use Smorken\Import\Contracts\Enums\MapIdTypes;

interface MapId extends ArrayAccess, IteratorAggregate
{
    /**
     * @param  \Illuminate\Support\Collection<\Smorken\Import\Contracts\Models\Data>  $models
     */
    public function fromDataCollection(Collection $models): void;

    /**
     * @param  \Illuminate\Support\Collection<\Smorken\Import\Contracts\Models\Target>  $models
     */
    public function fromTargetCollection(Collection $models): void;

    public function get(string $sourceIdentifier): string|int|null;

    public function getIdArray(#[ExpectedValues(valuesFromClass: MapIdTypes::class)] string $type): array;

    public function getSourceIdentifier(string $targetIdentifier): string|int|null;

    public function has(string $sourceIdentifier, bool $ignoreNull = true): bool;

    public function isEmpty(): bool;

    public function newInstance(): static;

    public function set(string $sourceIdentifier, string|int|null $targetIdentifier): void;
}
