<?php

namespace Smorken\Import\Contracts;

interface HasImportMapper
{
    public function getImportMapper(): ImportMapper;

    public function setImportMapper(ImportMapper $importMapper): void;

    public function storeToImportMapper(): int;
}
