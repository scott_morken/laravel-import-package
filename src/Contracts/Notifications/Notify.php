<?php

namespace Smorken\Import\Contracts\Notifications;

use Smorken\Import\Contracts\Models\Notifiable;
use Smorken\Import\Contracts\Results;

interface Notify
{
    public function getNotifiableModel(): Notifiable;

    public function notify(Results $results): void;
}
