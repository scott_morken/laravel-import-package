<?php

namespace Smorken\Import\Contracts;

use Illuminate\Support\Collection;

interface DataAndTargetModeller extends HasPartsFactory
{
    public function addTargetModelsToMapId(Collection $targetModels): void;

    public function fromSourceModels(Collection $sourceModels): Modelled;

    public function getModelled(): Modelled;

    public function getTargetsToCreate(): Collection;

    public function init(): void;

    public function reset(): void;

    public static function setModelledClass(string $modelledClass): void;
}
