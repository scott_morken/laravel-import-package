<?php

namespace Smorken\Import\Contracts\Models;

use Smorken\Model\Contracts\Model;

/**
 * @property int $id
 * @property string $importer
 * @property array $data
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
interface ImportResult extends Model
{
}
