<?php

namespace Smorken\Import\Contracts\Models;

use Smorken\Import\Contracts\Identifiers;
use Smorken\Model\Contracts\Model;

/**
 * @property string $importer
 * @property string $source_id
 * @property string $target_id
 *
 * @method $this|\Illuminate\Database\Eloquent\Builder newQuery()
 * @method $this|\Illuminate\Database\Eloquent\Builder sourceIdIs(string $sourceId)
 * @method $this|\Illuminate\Database\Eloquent\Builder targetIdIs(string $targetId)
 * @method $this|\Illuminate\Database\Eloquent\Builder sourceIdIn(array $sourceIds)
 * @method $this|\Illuminate\Database\Eloquent\Builder targetIdIn(array $targetIds)
 * @method $this|\Illuminate\Database\Eloquent\Builder importerIs(string $importer)
 * @method $this|\Illuminate\Database\Eloquent\Builder orderBySourceId()
 * @method $this|\Illuminate\Database\Eloquent\Builder orderByTargetId()
 * @method $this|\Illuminate\Database\Eloquent\Builder orderByImporter()
 */
interface ImportMap extends HasIdentifiers, Model
{
    public function fromIdentifiers(string $importer, Identifiers $identifiers): static;
}
