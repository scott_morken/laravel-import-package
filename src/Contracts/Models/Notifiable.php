<?php

namespace Smorken\Import\Contracts\Models;

use Smorken\Model\Contracts\Concerns\HasAttributes;

/**
 * @property string $email
 */
interface Notifiable extends HasAttributes
{
    /**
     * Send the given notification.
     * Implements RoutesNotifications::notify
     */
    public function notify($instance);
}
