<?php

namespace Smorken\Import\Contracts\Models;

use Illuminate\Support\Collection;

interface Data extends HasIdentifiers
{
    /**
     * @return array<string, array>
     */
    public function conversions(): array;

    /**
     * @param  \Illuminate\Support\Collection<\Smorken\Import\Contracts\Models\Source>  $models
     * @return \Illuminate\Support\Collection<\Smorken\Import\Contracts\Models\Data>
     */
    public function fromCollection(Collection $models): Collection;

    public function fromModel(mixed $model): self;

    public function getCreateAttributes(): array;

    public function getSourceModel(): mixed;

    public function getUpdateAttributes(): array;

    public function isValid(): bool;

    public function setSourceModel(mixed $model): void;

    public function shouldCreateFrom(mixed $model): bool;
}
