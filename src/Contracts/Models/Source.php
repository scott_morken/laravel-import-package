<?php

namespace Smorken\Import\Contracts\Models;

use Smorken\Model\Contracts\Concerns\HasAttributes;

interface Source extends \Smorken\Model\Contracts\Model, HasAttributes
{
}
