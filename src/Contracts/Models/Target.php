<?php

namespace Smorken\Import\Contracts\Models;

use JetBrains\PhpStorm\ExpectedValues;
use Smorken\Import\Contracts\Enums\TargetUpdateTypes;

interface Target extends HasIdentifiers
{
    public function fromDataModel(
        \Smorken\Import\Contracts\Models\Data $dataModel,
        array $attributes = []
    ): static;

    #[ExpectedValues(valuesFromClass: TargetUpdateTypes::class)]
    public function getUpdateType(): string;

    public function getUpdatedAttributes(): array;
}
