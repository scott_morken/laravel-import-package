<?php

namespace Smorken\Import\Contracts\Models;

use Smorken\Import\Contracts\Identifiers;
use Smorken\Model\Contracts\Concerns\HasAttributes;

/**
 * @property array|string $source_identifier
 * @property array|string $target_identifier
 */
interface HasIdentifiers extends HasAttributes
{
    public function getIdentifiers(): Identifiers;

    /**
     * @return array<string, mixed>
     */
    public function getSourceIdentifierAttribute(): array;

    /**
     * @return string[]
     */
    public function getSourceIdentifierAttributes(): array;

    /**
     * @return array<string, mixed>
     */
    public function getTargetIdentifierAttribute(): array;

    /**
     * @return string[]
     */
    public function getTargetIdentifierAttributes(): array;

    public function setSourceIdentifierAttribute(array|string $value): void;

    public function setTargetIdentifierAttribute(array|string $value): void;

    public static function setIdentifiersClass(string $identifierClass): void;
}
