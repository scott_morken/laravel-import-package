<?php

namespace Smorken\Import\Contracts\Storage;

use Illuminate\Support\Collection;
use JetBrains\PhpStorm\ExpectedValues;
use Smorken\Import\Contracts\Enums\IdentifierTypes;

interface ImportMap
{
    public function chunkByImporter(string $importer, callable $callback, int $perPage = 500): void;

    public function getByImporter(string $importer): Collection;

    public function getByImporterAndIdentifiers(
        string $importer,
        #[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $type,
        array $identifiers
    ): Collection;

    public function getModel(): \Smorken\Import\Contracts\Models\ImportMap;

    public function saveManyImporterAndArray(string $importer, array $data): int;

    public function saveManyModels(Collection $models): int;
}
