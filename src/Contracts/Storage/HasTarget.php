<?php

namespace Smorken\Import\Contracts\Storage;

use Illuminate\Support\Collection;
use Smorken\Import\Contracts\Models\Target;
use Smorken\Support\Contracts\Filter;

interface HasTarget
{
    public function cleanup(Filter $filter): int;

    /**
     * @param  \Illuminate\Support\Collection<\Smorken\Import\Contracts\Models\Target>  $targetModels
     * @return \Illuminate\Support\Collection<\Smorken\Import\Contracts\Models\Target>
     */
    public function createMany(Collection $targetModels): Collection;

    /**
     * @param  \Smorken\Import\Contracts\Identifiers[]  $identifiers
     * @return \Illuminate\Support\Collection<\Smorken\Import\Contracts\Models\Target>
     */
    public function getBySourceIdentifiers(array $identifiers): Collection;

    /**
     * @param  \Smorken\Import\Contracts\Identifiers[]  $identifiers
     * @return \Illuminate\Support\Collection<\Smorken\Import\Contracts\Models\Target>
     */
    public function getByTargetIdentifiers(array $identifiers): Collection;

    public function getTarget(): Target;

    /**
     * @param  \Illuminate\Support\Collection<string, \Smorken\Import\Contracts\Models\Target>  $targetModels
     */
    public function touchMany(Collection $targetModels): int;

    /**
     * @param  \Illuminate\Support\Collection<\Smorken\Import\Contracts\Models\Target>  $targetModels
     */
    public function updateMany(Collection $targetModels): int;
}
