<?php

namespace Smorken\Import\Contracts\Storage;

use Smorken\Storage\Contracts\Base;

interface ImportResult extends Base
{
}
