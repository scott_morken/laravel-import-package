<?php

namespace Smorken\Import\Contracts\Storage;

use Smorken\Import\Contracts\Models\Source;
use Smorken\Support\Contracts\Filter;

interface HasSource
{
    public function forImport(Filter $filter, callable $callback, int $count = 100): void;

    public function getSource(): Source;
}
