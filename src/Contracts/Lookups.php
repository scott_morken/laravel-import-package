<?php

namespace Smorken\Import\Contracts;

interface Lookups
{
    public function get(string $key = 'default'): MapId;

    public function getProvider(string $key): mixed;

    public function setMapId(string $key, MapId $mapId): void;

    public function setMapIds(array $mapIds): void;

    public function setProvider(string $key, mixed $provider): void;

    public function setProviders(array $providers): void;

    public function reset(): void;

    public function resetMapId(string $key = 'default'): void;
}
