<?php

namespace Smorken\Import\Contracts;

interface HasPartsFactory
{
    public function getFactory(): PartsFactory;

    public function setFactory(PartsFactory $partsFactory): void;
}
