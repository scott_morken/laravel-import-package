<?php

namespace Smorken\Import\Contracts;

use Illuminate\Support\Collection;
use Smorken\Support\Contracts\Filter;

interface Importer extends HasPartsFactory
{
    public function cleanup(Filter $filter, string $key = 'deleted'): Results;

    /**
     * Dispatch the ProcessResults jobs to the queue
     */
    public function dispatchResults(): void;

    /**
     * @param  \Illuminate\Support\Collection<\Smorken\Import\Contracts\Models\Source>  $models
     */
    public function handle(Collection $models): void;

    public function import(Filter $filter, int $perChunk = 100): Results;

    public function sample(Filter $filter, int $count = 10): Modelled;
}
