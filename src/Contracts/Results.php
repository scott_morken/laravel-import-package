<?php

namespace Smorken\Import\Contracts;

interface Results
{
    public function addMessage(string $key, string $message): void;

    public function decrement(string $key, int $count = 1): void;

    public function getCounter(string $key): int;

    public function getCounters(): array;

    public function getMessages(): array;

    public function getName(): string;

    public function increment(string $key, int $count = 1): void;

    public function init(array $counters = []): void;

    public function initCounters(array $counters = []): void;

    public function initMessages(): void;

    public function newInstance(?string $base = null): static;

    public function reset(string $key): void;

    public function resetAll(): void;

    public function setBase(string $base, bool $force = false): void;

    public function setCounter(string $key, int $value): void;

    /**
     * @param  array<string, int>  $counters
     */
    public function setCounters(array $counters): void;

    public function start(): void;

    public function stop(): void;

    public function toArray(): array;
}
