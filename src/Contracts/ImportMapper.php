<?php

namespace Smorken\Import\Contracts;

use Illuminate\Support\Collection;
use JetBrains\PhpStorm\ExpectedValues;
use Smorken\Import\Contracts\Enums\IdentifierTypes;
use Smorken\Import\Contracts\Storage\ImportMap;

/**
 * @property ImportMap|null static $importMapProvider
 */
interface ImportMapper
{
    public function getFactory(): PartsFactory;

    public function getImportMap(
        string $importer,
        #[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $identifierType,
        array $identifiers
    ): Collection;

    public function getProvider(): ImportMap;

    public function preload(string $importer): void;

    public function setMapIdsForCreatedModels(
        string $importer,
        Collection $targetModels
    ): void;

    public function setMapIdsForIdentifiers(
        string $importer,
        #[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $identifierType,
        array $identifiers
    ): void;

    public function setMapIdsForModels(
        string $importer,
        #[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $identifierType,
        Collection $models,
        ?string $columnName = null
    ): void;

    public function setShouldChunkPreload(bool $shouldChunk): void;

    public function store(string $importer): int;

    public function storeHasSourceId(string $importer, string $sourceId): bool;
}
