<?php

namespace Smorken\Import\Contracts\Enums;

interface TargetUpdateTypes
{
    public const FULL = 'full';

    public const TOUCH = 'touch';

    public const UNDEFINED = 'undefined';
}
