<?php

namespace Smorken\Import\Contracts\Enums;

interface ModelTypes
{
    public const DATA = 'data';

    public const SOURCE = 'source';

    public const TARGET = 'target';
}
