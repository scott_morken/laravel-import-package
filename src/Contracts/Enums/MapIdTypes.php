<?php

namespace Smorken\Import\Contracts\Enums;

interface MapIdTypes
{
    public const TARGET = 'target';

    public const SOURCE = 'source';
}
