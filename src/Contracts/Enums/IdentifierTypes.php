<?php

namespace Smorken\Import\Contracts\Enums;

interface IdentifierTypes
{
    public const SOURCE = 'source';

    public const TARGET = 'target';
}
