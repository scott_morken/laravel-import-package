<?php

namespace Smorken\Import\Contracts\Enums;

use Smorken\Import\Contracts\DataAndTargetModeller;
use Smorken\Import\Contracts\Lookups;
use Smorken\Import\Contracts\Models\Data;
use Smorken\Import\Contracts\Results;
use Smorken\Import\Contracts\Storage\HasSource;
use Smorken\Import\Contracts\Storage\HasTarget;

interface PartsFactoryParts
{
    /**
     * Data representation, converts from Source to Target
     */
    public const DATA_MODEL = Data::class;

    public const DATA_TARGET_MODELLER = DataAndTargetModeller::class;

    public const LOOKUPS = Lookups::class;

    public const RESULTS = Results::class;

    /**
     * Raw models from external source
     */
    public const SOURCE_PROVIDER = HasSource::class;

    /**
     * Target provider, creates targets models from Data models
     */
    public const TARGET_PROVIDER = HasTarget::class;
}
