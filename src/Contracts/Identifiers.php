<?php

namespace Smorken\Import\Contracts;

use JetBrains\PhpStorm\ExpectedValues;
use Smorken\Import\Contracts\Enums\IdentifierTypes;

interface Identifiers
{
    public function asString(#[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $type): string;

    public function get(#[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $type): array;

    public function getAttributes(#[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $type): array;

    public function getSingleAttribute(#[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $type): string;

    public function getSingleValue(#[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $type): string;

    /**
     * Represents the source identifying attributes on both the data model (via the source)
     * and the target model (from the data model)
     *
     * @return array<string, mixed>
     */
    public function getSourceIdentifiers(): array;

    /**
     * Represents the record identifier.  Empty array on the data model since it is
     * transient.  This is normally the id on the target model.
     *
     * @return array<string, mixed>
     */
    public function getTargetIdentifiers(): array;

    public function getVirtualSourceIdentifiers(): array;

    public function getVirtualTargetIdentifiers(): array;

    public function isComplete(#[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $type): bool;

    public function isSingleAttribute(#[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $type): bool;

    public function set(
        #[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $type,
        string|array $value,
        bool $virtual = true
    ): void;
}
