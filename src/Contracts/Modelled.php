<?php

namespace Smorken\Import\Contracts;

use Illuminate\Support\Collection;
use Smorken\Import\Contracts\Models\Data;

interface Modelled
{
    public const VALIDATION = 'validation';

    public const LOOKUP = 'lookup';

    public function addFailure(Data $model, string $type = Modelled::VALIDATION): void;

    public function addLookupFailure(Data $model): void;

    public function addValidationFailure(Data $model): void;

    public function getDataModels(): Collection;

    public function getTargetModels(): Collection;

    public function getValidationFailures(): array;

    public function getLookupFailures(): array;

    public function getFailures(?string $type = null): array;

    public function setDataModels(Collection $dataModels): void;

    public function setTargetModels(Collection $targetModels): void;
}
