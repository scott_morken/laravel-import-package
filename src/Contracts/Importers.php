<?php

namespace Smorken\Import\Contracts;

interface Importers
{
    /**
     * @return array<string, Importer>
     */
    public function all(): array;

    public function get(
        string $interface
    ): Importer;

    public function set(
        string $interface,
        Importer $importer
    ): void;

    /**
     * @param  array<string, Importer>  $importers
     */
    public function setFromArray(array $importers): void;
}
