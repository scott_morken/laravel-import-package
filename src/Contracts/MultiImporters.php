<?php

namespace Smorken\Import\Contracts;

use Illuminate\Console\Command;
use Smorken\Support\Contracts\Filter;

interface MultiImporters
{
    public function cleanup(Filter $filter): void;

    public function commandOutput(string $message): void;

    public function dispatchResults(): void;

    public function getCommand(): ?Command;

    public function getImporter(string $interface): Importer;

    public function getImporters(): Importers;

    public function getResults(): Results;

    public function handle(Filter $filter, int $perChunk = 100): Results;

    public function import(Filter $filter, int $perChunk = 100): void;

    public function setCommand(Command $command): void;

    public function setImporters(Importers $importers): void;
}
