<?php

namespace Smorken\Import\Events;

use Illuminate\Queue\SerializesModels;
use Smorken\Import\Contracts\Results;

class ImportResults
{
    use SerializesModels;

    public function __construct(public Results $results)
    {
    }
}
