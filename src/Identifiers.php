<?php

namespace Smorken\Import;

use JetBrains\PhpStorm\ExpectedValues;
use Smorken\Import\Contracts\Enums\IdentifierTypes;
use Smorken\Import\Contracts\Models\HasIdentifiers;

class Identifiers implements \Smorken\Import\Contracts\Identifiers
{
    protected string $separator = ':';

    protected array $virtualAttributes = [
        IdentifierTypes::SOURCE => [],
        IdentifierTypes::TARGET => [],
    ];

    public function __construct(
        protected HasIdentifiers $model,
        protected array $sourceAttributes,
        protected array $targetAttributes
    ) {
        $this->setVirtualAttributeKeys($this->sourceAttributes, IdentifierTypes::SOURCE);
        $this->setVirtualAttributeKeys($this->targetAttributes, IdentifierTypes::TARGET);
    }

    public function asString(#[ExpectedValues(valuesFromClass: Contracts\Enums\IdentifierTypes::class)] string $type
    ): string {
        $values = $this->get($type);

        return implode($this->separator, array_values($values));
    }

    public function get(#[ExpectedValues(valuesFromClass: Contracts\Enums\IdentifierTypes::class)] string $type): array
    {
        return $this->getIdentifiers($type);
    }

    public function getAttributes(#[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $type): array
    {
        return $this->getTypeAttributeKeys($type);
    }

    public function getSingleAttribute(
        #[ExpectedValues(valuesFromClass: Contracts\Enums\IdentifierTypes::class)] string $type
    ): string {
        if ($this->isSingleAttribute($type)) {
            return $this->getAttributes($type)[0];
        }
        throw new \OutOfBoundsException("$type is not a single column attribute.");
    }

    public function getSingleValue(#[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $type): string
    {
        if ($this->isSingleAttribute($type)) {
            return array_values($this->get($type))[0];
        }
        throw new \OutOfBoundsException("$type is not a single column value.");
    }

    public function getSourceIdentifiers(): array
    {
        return $this->getIdentifiers(IdentifierTypes::SOURCE);
    }

    public function getTargetIdentifiers(): array
    {
        return $this->getIdentifiers(IdentifierTypes::TARGET);
    }

    public function getVirtualSourceIdentifiers(): array
    {
        return $this->fromVirtualAttributes($this->getTypeAttributeKeys(IdentifierTypes::SOURCE),
            IdentifierTypes::SOURCE);
    }

    public function getVirtualTargetIdentifiers(): array
    {
        return $this->fromVirtualAttributes($this->getTypeAttributeKeys(IdentifierTypes::TARGET),
            IdentifierTypes::TARGET);
    }

    public function isComplete(#[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $type): bool
    {
        $value = $this->get($type);
        if ($this->isEmptyIdentifier($value)) {
            return false;
        }
        if (count($value) !== count($this->getTypeAttributeKeys($type))) {
            return false;
        }

        return true;
    }

    public function isSingleAttribute(
        #[ExpectedValues(valuesFromClass: Contracts\Enums\IdentifierTypes::class)] string $type
    ): bool {
        return count($this->getTypeAttributeKeys($type)) === 1;
    }

    public function set(
        #[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $type,
        array|string $value,
        bool $virtual = true
    ): void {
        $value = $this->ensureValueArray($type, $value);
        if ($virtual) {
            $this->setVirtualAttributeValue($type, $value);

            return;
        }
        $this->setModelAttributeValue($type, $value);
    }

    protected function ensureValueArray(string $type, string|array $value): array
    {
        $checked = [];
        if (is_string($value)) {
            $value = explode($this->separator, $value);
        }
        $keys = $this->getTypeAttributeKeys($type);
        if (count($value) !== count($keys)) {
            throw new \OutOfBoundsException('Count of attributes does not match keys in identifier.');
        }
        foreach ($value as $k => $v) {
            if (is_int($k)) {
                $k = $keys[$k];
            }
            $checked[$k] = $v;
        }

        return $checked;
    }

    protected function fromAttributes(array $attributes): array
    {
        $parts = [];
        foreach ($attributes as $attr) {
            $parts[$attr] = $this->model->getAttribute($attr);
        }

        return $parts;
    }

    protected function fromVirtualAttributes(
        array $attributes,
        #[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $type
    ): array {
        $parts = [];
        foreach ($attributes as $attribute) {
            $parts[$attribute] = $this->virtualAttributes[$type][$attribute] ?? null;
        }

        return $parts;
    }

    protected function getIdentifiers(#[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $type): array
    {
        $identifiers = $this->fromAttributes($this->getTypeAttributeKeys($type));
        if (! $this->isEmptyIdentifier($identifiers)) {
            return $identifiers;
        }

        return $this->fromVirtualAttributes($this->getTypeAttributeKeys($type), $type);
    }

    protected function getTypeAttributeKeys(#[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $type
    ): array {
        if ($type === IdentifierTypes::SOURCE) {
            return $this->sourceAttributes;
        }
        if ($type === IdentifierTypes::TARGET) {
            return $this->targetAttributes;
        }

        return [];
    }

    protected function isEmptyIdentifier(array $identifier): bool
    {
        foreach ($identifier as $k => $v) {
            if (is_null($v)) {
                return true;
            }
        }

        return false;
    }

    protected function setModelAttributeValue(
        #[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $type,
        array $value
    ): void {
        foreach ($value as $k => $v) {
            if ($this->typeHasAttributeKey($type, $k)) {
                $this->model->setAttribute($k, $v);
            }
        }
    }

    protected function setVirtualAttributeKeys(
        array $attributes,
        #[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $type
    ): void {
        foreach ($attributes as $attribute) {
            $this->virtualAttributes[$type][$attribute] = null;
        }
    }

    protected function setVirtualAttributeValue(
        #[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $type,
        array $value
    ): void {
        foreach ($value as $k => $v) {
            if ($this->typeHasAttributeKey($type, $k)) {
                $this->virtualAttributes[$type][$k] = $v;
            }
        }
    }

    protected function typeHasAttributeKey(
        #[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $type,
        string $key
    ): bool {
        return in_array($key, $this->getTypeAttributeKeys($type));
    }
}
