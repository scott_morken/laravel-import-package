<?php

namespace Smorken\Import\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Smorken\Import\Contracts\Importers;
use Smorken\Import\Contracts\Modelled;
use Smorken\Import\Contracts\Results;
use Smorken\Support\Arr;
use Smorken\Support\Filter;

class ImportByName extends Command
{
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs the active imports for the selected importer name.';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:name {importer?} {--dry-run}';

    public function __construct(protected Importers $importers)
    {
        parent::__construct();
    }

    public function handle(): int
    {
        $dryRun = $this->option('dry-run');
        $importer = $this->getSelectedImporter();
        if ($importer) {
            return $this->runImporter($importer, $dryRun);
        }
        $this->error('No importer found.');

        return 1;
    }

    protected function askForSelection(array $importers): ?\Smorken\Import\Contracts\Importer
    {
        $this->info('Select an importer');
        foreach ($importers as $key => $importer) {
            $this->warn(sprintf('[%s] %s', $key, class_basename($importer)));
        }
        $selected = $this->ask('Select the importer you want to run');

        return $importers[$selected] ?? null;
    }

    protected function getSelectedImporter(): ?\Smorken\Import\Contracts\Importer
    {
        $selected = strtolower($this->argument('importer'));
        $importers = $this->rekeyImporters($this->importers->all());
        if ($selected && isset($importers[$selected])) {
            return $importers[$selected];
        }

        return $this->askForSelection($importers);
    }

    protected function outputCollection(Collection $models): void
    {
        foreach ($models as $model) {
            if (method_exists($model, 'convert')) {
                $this->line(Arr::stringify($model->convert()));
            } else {
                $this->line(Arr::stringify($model->getAttributes()));
            }
        }
    }

    protected function outputResults(Results $results): void
    {
        $this->info('Results');
        $this->line(Arr::stringify($results->toArray()));
    }

    protected function outputSample(Modelled $samples): void
    {
        $this->info('Sample');
        $this->info('-- Data Models');
        $this->outputCollection($samples->getDataModels());
        $this->info('-- Target Models');
        $this->outputCollection($samples->getTargetModels());
    }

    protected function rekeyImporters(array $importers): array
    {
        $keyed = [];
        foreach ($importers as $importer) {
            $baseName = strtolower(class_basename($importer));
            $keyed[$baseName] = $importer;
        }

        return $keyed;
    }

    protected function runImporter(\Smorken\Import\Contracts\Importer $importer, bool $dryRun): int
    {
        if ($dryRun) {
            $samples = $importer->sample(new Filter());
            $results = $importer->getFactory()->getResults();
            $this->outputSample($samples);
        } else {
            $results = $importer->import(new Filter());
        }
        $this->outputResults($results);

        return 0;
    }
}
