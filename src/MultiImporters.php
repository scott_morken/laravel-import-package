<?php

namespace Smorken\Import;

use Smorken\Support\Contracts\Filter;

class MultiImporters extends AbstractMulti implements \Smorken\Import\Contracts\MultiImporters
{
    public function cleanup(Filter $filter): void
    {
        // do nothing here, should be handled in individual importer
    }

    protected function runImport(string $collectionMethod, Filter $filter, array $lookups, int $perChunk): array
    {
        $importers = $this->$collectionMethod();
        if ($importers->count() > 0) {
            foreach ($importers as $interface => $importer) {
                $this->commandOutput(sprintf('[%s] starting', $interface));
                $importer->import($filter, $perChunk);
                $this->addLookupsToImporter($importer, $lookups);
                if (! isset($lookups[$interface])) {
                    $lookups[$interface] = $importer->getFactory()->getLookups()->get();
                }
                $this->commandOutput(sprintf('[%s] complete', $interface));
                $this->commandOutput(sprintf('%d MB memory use', memory_get_peak_usage(true) / 2 ** 20));
            }
        }

        return $lookups;
    }
}
