<?php

namespace Smorken\Import;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use JetBrains\PhpStorm\ExpectedValues;
use Smorken\Import\Contracts\Enums\IdentifierTypes;
use Smorken\Import\Contracts\Models\Data;
use Smorken\Import\Contracts\Models\Target;
use Smorken\Import\Contracts\PartsFactory;
use Smorken\Import\Contracts\Storage\ImportMap;

class ImportMapper implements \Smorken\Import\Contracts\ImportMapper
{
    public static ?ImportMap $importMapProvider = null;

    protected bool $shouldChunkPreload = false;

    protected array $stored = [];

    public function __construct(protected PartsFactory $partsFactory)
    {
    }

    public function getFactory(): PartsFactory
    {
        return $this->partsFactory;
    }

    public function getImportMap(
        string $importer,
        #[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $identifierType,
        array $identifiers
    ): Collection {
        return $this->getImportMapFromProvider($importer, $identifierType, $identifiers, true);
    }

    public function getProvider(): ImportMap
    {
        if (is_null(self::$importMapProvider)) {
            self::$importMapProvider = App::make(ImportMap::class);
        }

        return self::$importMapProvider;
    }

    public function preload(string $importer): void
    {
        if (! isset($this->stored[$importer])) {
            $this->ensureStoreImporterArray($importer);
            if ($this->shouldChunkPreload) {
                $this->getProvider()->chunkByImporter($importer, function (Collection $models) use ($importer) {
                    $this->addToMapIdsFor($importer, $models);
                });

                return;
            }
            $models = $this->getProvider()->getByImporter($importer);
            $this->addToMapIdsFor($importer, $models);
        }
    }

    public function setMapIdsForCreatedModels(string $importer, Collection $targetModels): void
    {
        $identifiers = $this->getArrayOfIdentifiersFromModels($importer, $targetModels);
        foreach ($identifiers as $sourceId => $targetId) {
            $this->getFactory()->getLookups()->get($importer)->set($sourceId, $targetId);
        }
    }

    public function setMapIdsForIdentifiers(
        string $importer,
        #[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $identifierType,
        array $identifiers
    ): void {
        $mapped = $this->getImportMap($importer, $identifierType, $identifiers);
        $this->addToMapIdsFor($importer, $mapped);
    }

    public function setMapIdsForModels(
        string $importer,
        #[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $identifierType,
        Collection $models,
        ?string $columnName = null
    ): void {
        $identifiers = $this->getIdentifiersFromModels($importer, $identifierType, $models, $columnName);
        $this->setMapIdsForIdentifiers($importer, $identifierType, $identifiers);
    }

    public function setShouldChunkPreload(bool $shouldChunk): void
    {
        $this->shouldChunkPreload = $shouldChunk;
    }

    public function store(string $importer): int
    {
        $this->preload($importer);
        $activeIdentifiers = $this->getActiveIdentifiers($importer);
        $identifiers = $this->removeIdentifiersAlreadyStored($importer, $activeIdentifiers);
        $stored = $this->getProvider()->saveManyImporterAndArray($importer, $this->convertToStorable($identifiers));
        foreach ($identifiers as $sourceId => $targetId) {
            $this->addMapIdFor($importer, $sourceId, $targetId);
        }

        return $stored;
    }

    public function storeHasSourceId(string $importer, string $sourceId): bool
    {
        return isset($this->stored[$importer][$sourceId]);
    }

    protected function addMapIdFor(string $importer, ?string $sourceId, ?string $targetId): void
    {
        if ($sourceId && $targetId) {
            $this->getFactory()->getLookups()->get($importer)->set($sourceId, $targetId);
            $this->stored[$importer][$sourceId] = $targetId;
        }
    }

    protected function addToMapIdsFor(string $importer, Collection $mapped): void
    {
        $this->ensureStoreImporterArray($importer);
        foreach ($mapped as $map) {
            $sourceId = $map->getIdentifiers()->asString(IdentifierTypes::SOURCE);
            $targetId = $map->getIdentifiers()->asString(IdentifierTypes::TARGET);
            $this->addMapIdFor($importer, $sourceId, $targetId);
        }
    }

    protected function convertToStorable(array $identifiers): array
    {
        $storable = [];
        foreach ($identifiers as $sourceId => $targetId) {
            $storable[] = ['source_id' => $sourceId, 'target_id' => $targetId];
        }

        return $storable;
    }

    protected function ensureStoreImporterArray(string $importer): void
    {
        if (! isset($this->stored[$importer])) {
            $this->stored[$importer] = [];
        }
    }

    protected function getActiveIdentifiers(string $importer): array
    {
        $active = [];
        foreach ($this->getFactory()->getLookups()->get($importer) as $sourceId => $targetId) {
            if ($sourceId !== null && $targetId !== null) {
                $active[$sourceId] = $targetId;
            }
        }

        return $active;
    }

    protected function getArrayOfIdentifiersFromModels(string $importer, Collection $models): array
    {
        $identifiers = [];
        foreach ($models as $model) {
            $sourceId = $this->getIdentifierFromModel($importer, IdentifierTypes::SOURCE, $model, null);
            $targetId = $this->getIdentifierFromModel($importer, IdentifierTypes::TARGET, $model, null);
            if ($sourceId && $targetId) {
                $identifiers[$sourceId] = $targetId;
            }
        }

        return $identifiers;
    }

    protected function getAttributeFromModel(
        Data|Target $model,
        string $attributeName,
        #[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $identifierType
    ): string|int|null {
        if ($model instanceof Data && $identifierType === IdentifierTypes::SOURCE) {
            return $model->getAttribute($attributeName) ?? $model->getSourceModel()->getAttribute($attributeName);
        }

        return $model->getAttribute($attributeName);
    }

    protected function getIdentifierFromModel(
        string $importer,
        #[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $identifierType,
        Data|Target $model,
        ?string $columnName
    ): string|int|null {
        if (is_null($columnName)) {
            return $model->getIdentifiers()->asString($identifierType);
        }

        return $this->getAttributeFromModel($model, $columnName, $identifierType);
    }

    protected function getIdentifiersFromModels(
        string $importer,
        #[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $identifierType,
        Collection $models,
        ?string $columnName
    ): array {
        $identifiers = [];
        foreach ($models as $model) {
            $identifier = $this->getIdentifierFromModel($importer, $identifierType, $model, $columnName);
            if (strlen($identifier)) {
                $identifiers[$identifier] = $identifier;
            }
        }

        return array_values($identifiers);
    }

    protected function getImportMapFromProvider(
        string $importer,
        #[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $identifierType,
        array $identifiers,
        bool $limitToMissing
    ): Collection {
        if ($limitToMissing) {
            $identifiers = $this->limitToMissingSourceIdentifiers($importer, $identifierType, $identifiers);
        }
        if (! empty($identifiers)) {
            return $this->getProvider()
                ->getByImporterAndIdentifiers($importer, $identifierType, $identifiers);
        }

        return new Collection();
    }

    protected function limitToMissingSourceIdentifiers(
        string $importer,
        #[ExpectedValues(valuesFromClass: IdentifierTypes::class)] string $identifierType,
        array $identifiers
    ): array {
        if ($identifierType === IdentifierTypes::TARGET) {
            return $identifiers;
        }
        $missing = [];
        foreach ($identifiers as $identifier) {
            if ($this->missingSourceId($importer, $identifier)) {
                $missing[$identifier] = $identifier;
            }
        }

        return array_values($missing);
    }

    protected function missingSourceId(string $importer, ?string $sourceId): bool
    {
        return $sourceId && ! $this->getFactory()->getLookups()->get($importer)->has($sourceId);
    }

    protected function removeIdentifiersAlreadyStored(string $importer, array $identifiers): array
    {
        $missing = [];
        foreach ($identifiers as $sourceId => $targetId) {
            if (! $this->storeHasSourceId($importer, $sourceId)) {
                $missing[$sourceId] = $targetId;
            }
        }

        return $missing;
    }
}
