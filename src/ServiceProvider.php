<?php

namespace Smorken\Import;

use Smorken\Import\Console\Commands\ImportByName;
use Smorken\Import\Contracts\Models\Notifiable;
use Smorken\Import\Contracts\Notifications\Notify;
use Smorken\Import\Contracts\Storage\ImportMap;
use Smorken\Import\Contracts\Storage\ImportResult;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        $this->bootConfig();
        $this->bootMigrations();
        $this->ensureImportersIsBound();
        $this->commands([
            ImportByName::class,
        ]);
    }

    public function register(): void
    {
        $this->bindImportResultStorage();
        $this->bindNotifiable();
        $this->bindNotify();
        $this->bindImportMapStorage();
    }

    protected function bindImportMapStorage(): void
    {
        $this->app->bind(ImportMap::class, function ($app) {
            $storageClass = $app['config']->get('import.import_map.impl');
            $modelClass = $app['config']->get('import.import_map.model');
            if ($storageClass && $modelClass) {
                return new $storageClass(new $modelClass());
            }
        });
    }

    protected function bindImportResultStorage(): void
    {
        $this->app->bind(ImportResult::class, function ($app) {
            $storageCls = $app['config']->get('import.storage.'.ImportResult::class);
            $modelCls = $app['config']->get('import.models.'.\Smorken\Import\Contracts\Models\ImportResult::class);

            return new $storageCls(new $modelCls());
        });
    }

    protected function bindNotifiable(): void
    {
        $this->app->bind(Notifiable::class, function ($app) {
            $email = $app['config']->get('import.email_to');

            return new \Smorken\Import\Models\VO\Notifiable(['email' => $email]);
        });
    }

    protected function bindNotify(): void
    {
        $this->app->bind(Notify::class, function ($app) {
            $notifiable = $app[Notifiable::class];

            return new \Smorken\Import\Notifications\Notify($notifiable);
        });
    }

    protected function bootConfig()
    {
        $config = __DIR__.'/../config/config.php';
        $this->mergeConfigFrom($config, 'import');
        $this->publishes([$config => config_path('import.php')], 'config');
    }

    protected function bootMigrations(): void
    {
        if ($this->app['config']->get('import.migrations', false)) {
            $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        }
    }

    protected function ensureImportersIsBound(): void
    {
        if (! $this->app->bound(\Smorken\Import\Contracts\Importers::class)) {
            $this->app->bind(\Smorken\Import\Contracts\Importers::class, fn ($app) => new Importers([]));
        }
    }
}
