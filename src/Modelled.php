<?php

namespace Smorken\Import;

use Illuminate\Support\Collection;
use Smorken\Import\Contracts\Models\Data;

class Modelled implements \Smorken\Import\Contracts\Modelled
{
    protected ?Collection $dataModels = null;

    /**
     * @var array<string,Data[]>
     */
    protected array $failures = [
        self::VALIDATION => [],
        self::LOOKUP => [],
    ];

    protected ?Collection $targetModels = null;

    public function addFailure(Data $model, string $type = \Smorken\Import\Contracts\Modelled::VALIDATION): void
    {
        $this->failures[$type][] = $model;
    }

    public function addLookupFailure(Data $model): void
    {
        $this->addFailure($model, self::LOOKUP);
    }

    public function addValidationFailure(Data $model): void
    {
        $this->addFailure($model, self::VALIDATION);
    }

    public function getDataModels(): Collection
    {
        return $this->dataModels;
    }

    public function setDataModels(Collection $dataModels): void
    {
        $this->dataModels = $dataModels;
    }

    public function getFailures(?string $type = null): array
    {
        if (is_null($type)) {
            return $this->failures;
        }

        return $this->failures[$type] ?? [];
    }

    public function getLookupFailures(): array
    {
        return $this->getFailures(self::LOOKUP);
    }

    public function getTargetModels(): Collection
    {
        return $this->targetModels;
    }

    public function setTargetModels(Collection $targetModels): void
    {
        $this->targetModels = $targetModels;
    }

    /**
     * @return \Smorken\Import\Contracts\Models\Data[]
     */
    public function getValidationFailures(): array
    {
        return $this->getFailures(self::VALIDATION);
    }
}
