<?php

namespace Database\Factories\Smorken\Import\Models\Eloquent;

use Illuminate\Database\Eloquent\Factories\Factory;
use Smorken\Import\Models\Eloquent\ImportResult;

class ImportResultFactory extends Factory
{
    protected $model = ImportResult::class;

    public function definition(): array
    {
        return [
            'importer' => 'Importer',
            'data' => json_encode(['foo' => 'bar']),
        ];
    }
}
