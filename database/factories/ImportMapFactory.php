<?php

namespace Database\Factories\Smorken\Import\Models\Eloquent;

use Illuminate\Database\Eloquent\Factories\Factory;
use Smorken\Import\Models\Eloquent\ImportMap;

class ImportMapFactory extends Factory
{
    protected $model = ImportMap::class;

    public function definition()
    {
        return [
            'importer' => 'Foo',
            'source_id' => 1,
            'target_id' => 100,
        ];
    }
}
