<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function down(): void
    {
        Schema::dropIfExists('import_maps');
    }

    public function up(): void
    {
        Schema::create('import_maps', function (Blueprint $t) {
            $t->bigIncrements('id');
            $t->string('importer');
            $t->string('source_id');
            $t->string('target_id');
            $t->timestamps();

            $t->unique(['importer', 'source_id'], 'im_imp_src_id_ndx');
            $t->index('importer', 'im_importer_ndx');
            $t->index('source_id', 'im_source_id_ndx');
            $t->index('target_id', 'im_target_id_ndx');
        });
    }
};
